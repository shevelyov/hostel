package by.epam.shevelyov.hostel.dao;

import by.epam.shevelyov.hostel.entity.Entity;

import java.util.List;

/**
 * <p>Interface that describes all operations with database with entities</p>
 *
 * @param <T> entity parameter
 * @author Ilya Shevelyov
 */
public interface BaseDAO<T extends Entity> {
    boolean create(T entity);

    boolean update(T entity);

    boolean delete(int id);

    T findEntityById(int id);

    List<T> findAll();

    List<T> execute(QueriesEnum query, Object[] params);

    List<T> execute(QueriesEnum query, int param);
}
