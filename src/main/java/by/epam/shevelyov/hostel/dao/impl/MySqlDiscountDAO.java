package by.epam.shevelyov.hostel.dao.impl;

import by.epam.shevelyov.hostel.dao.BaseDAO;
import by.epam.shevelyov.hostel.dao.QueriesEnum;
import by.epam.shevelyov.hostel.entity.impl.Discount;
import by.epam.shevelyov.hostel.pool.ConnectionsPool;
import by.epam.shevelyov.hostel.pool.PoolConnection;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * <p>Class for working with the {@link Discount} entity from MySQL database</p>
 *
 * @author Ilya Shevelyov
 */
public class MySqlDiscountDAO implements BaseDAO<Discount> {
    private static final Logger LOGGER = Logger.getLogger(MySqlDiscountDAO.class);
    private static final String SQL_CREATE = "INSERT INTO discount(title, percentage, description) VALUES(?,?,?)";
    private static final String SQL_FIND = "SELECT * FROM discount WHERE discount_id=? LIMIT 1";
    private static final String SQL_FIND_ALL = "SELECT * FROM discount";
    private static final String SQL_UPDATE = "UPDATE discount SET title=?, percentage=?, description=? WHERE discount_id=?";
    private static final String SQL_DELETE = "DELETE FROM discount WHERE discount_id = ?";
    private static final String DISCOUNT_ID = "discount.discount_id";
    private static final String DISCOUNT_TITLE = "discount.title";
    private static final String DISCOUNT_PERCENTAGE = "discount.percentage";
    private static final String DISCOUNT_DESCRIPTION = "discount.description";
    private Lock lock = new ReentrantLock();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    private static class InstanceHolder {
        private static final MySqlDiscountDAO INSTANCE = new MySqlDiscountDAO();
    }

    private MySqlDiscountDAO() {
    }

    public static MySqlDiscountDAO getInstance() {
        return MySqlDiscountDAO.InstanceHolder.INSTANCE;
    }

    @Override
    public boolean create(Discount discount) {
        try (PoolConnection connection = pool.getConnection();
             PreparedStatement statement = connection.getConnection().prepareStatement(SQL_CREATE)) {
            lock.lock();
            statement.setString(1, discount.getTitle());
            statement.setInt(2, discount.getPercentage());
            statement.setString(3, discount.getDescription());
            statement.execute();
            return true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return false;
    }

    @Override
    public Discount findEntityById(int key) {
        Discount result = null;
        ResultSet resultSet = null;
        try (PoolConnection poolConnection = pool.getConnection();
             PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_FIND)) {
            statement.setInt(1, key);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = createDiscount(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }

    @Override
    public List<Discount> findAll() {
        List<Discount> result = new ArrayList<>();
        try (PoolConnection connection = pool.getConnection();
             PreparedStatement statement = connection.getConnection().prepareStatement(SQL_FIND_ALL);
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Discount discount = createDiscount(resultSet);
                result.add(discount);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return result;
    }

    @Override
    public boolean update(Discount discount) {
        try (PoolConnection connection = pool.getConnection();
             PreparedStatement statement = connection.getConnection().prepareStatement(SQL_UPDATE)) {
            lock.lock();
            statement.setString(1, discount.getTitle());
            statement.setInt(2, discount.getPercentage());
            statement.setString(3, discount.getDescription());
            statement.setInt(4, discount.getId());
            statement.execute();
            return true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return false;
    }

    @Override
    public boolean delete(int key) {
        boolean result = false;
        try (PoolConnection poolConnection = pool.getConnection();
             PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_DELETE)) {
            lock.lock();
            statement.setInt(1, key);
            statement.executeUpdate();
            result = true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return result;
    }

    @Override
    public List<Discount> execute(QueriesEnum query, Object[] params) {
        return null;
    }

    @Override
    public List<Discount> execute(QueriesEnum query, int param) {
        return null;
    }

    private Discount createDiscount(ResultSet resultSet) {
        Discount result = null;
        try {
            int id = resultSet.getInt(DISCOUNT_ID);
            String title = resultSet.getString(DISCOUNT_TITLE);
            int percentage = resultSet.getInt(DISCOUNT_PERCENTAGE);
            String description = resultSet.getString(DISCOUNT_DESCRIPTION);
            result = new Discount(id, title, percentage, description);
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return result;
    }
}
