package by.epam.shevelyov.hostel.dao;

import by.epam.shevelyov.hostel.dao.factory.DAOFactory;
import by.epam.shevelyov.hostel.dao.factory.impl.MySqlDAOFactory;

/**
 * <p>Enum for choosing which database will be used in the project</p>
 *
 * @author Ilya Shevelyov
 */
public enum DAOEnum {
    MYSQL(new MySqlDAOFactory());

    DAOFactory factory;

    DAOEnum(DAOFactory factory) {
        this.factory = factory;
    }

    public DAOFactory getFactory() {
        return this.factory;
    }
}
