package by.epam.shevelyov.hostel.dao;

/**
 * <p>Enum for not simple CRUD operations</p>
 *
 * @author Ilya Shevelyov
 */
public enum QueriesEnum {
    USER_BY_LOGIN,
    USER_BY_EMAIL,
    USER_BY_ROLE,
    BAN_BY_USER_ID,
    BAN_BY_REASON,
    BAN_BY_END_DATE,
    ROOM_TYPE_BY_CAPACITY,
    ROOM_TYPE_BY_DESCRIPTION,
    ROOM_BY_ROOM_TYPE_ID,
    ORDER_BY_USER_ID,
    ORDER_BY_ROOM_ID,
    ORDER_BY_DATE_CHECK_IN,
    ORDER_BY_DATE_CHECK_OUT,
    ORDER_BY_DATES_AND_ROOM_ID,
    ORDER_BY_ORDER_STATUS,
    ORDER_NOT_CONFIRMED,
    ORDER_CANCELLED,
    ORDER_CONFIRMED,
    ORDER_RESERVATION,
    ORDER_PAYMENT,
    ORDER_STATUS_BY_STATUS;

}

