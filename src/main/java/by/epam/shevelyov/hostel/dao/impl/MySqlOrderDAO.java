package by.epam.shevelyov.hostel.dao.impl;

import by.epam.shevelyov.hostel.dao.BaseDAO;
import by.epam.shevelyov.hostel.dao.QueriesEnum;
import by.epam.shevelyov.hostel.entity.impl.*;
import by.epam.shevelyov.hostel.pool.ConnectionsPool;
import by.epam.shevelyov.hostel.pool.PoolConnection;
import by.epam.shevelyov.hostel.service.*;
import org.apache.log4j.Logger;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * <p>Class for working with the {@link Order} entity from MySQL database</p>
 *
 * @author Ilya Shevelyov
 */
public class MySqlOrderDAO implements BaseDAO<Order> {
    private static final Logger LOGGER = Logger.getLogger(MySqlOrderDAO.class);
    private static final String SQL_CREATE = "INSERT INTO orders (user_id, places, registration_date, date_check_in, " +
            "date_check_out, room_id, order_status_id, order_type_id, discount_id, total_amount) VALUES(?,?,?,?,?,?,?,?,?,?)";
    private static final String SQL_FIND = "SELECT * FROM orders WHERE order_id=? LIMIT 1";
    private static final String SQL_FIND_ALL = "SELECT * FROM orders";
    private static final String SQL_UPDATE = "UPDATE orders SET user_id=?, places=?, registration_date=?, " +
            "date_check_in=?, date_check_out=?, room_id=?, order_status_id=?, order_type_id=?, discount_id=?, total_amount=?  WHERE order_id=?";
    private static final String SQL_DELETE = "DELETE FROM orders WHERE order_id = ?";
    private static final String SQL_FIND_BY_USER_ID = "SELECT * FROM orders WHERE user_id = ?";
    private static final String SQL_FIND_BY_ROOM_ID = "SELECT * FROM orders WHERE room_id = ?";
    private static final String SQL_FIND_BY_DATE_CHECK_IN = "SELECT * FROM orders WHERE date_check_in = ?";
    private static final String SQL_FIND_BY_DATE_CHECK_OUT = "SELECT * FROM orders WHERE date_check_out = ?";
    private static final String SQL_FIND_BY_ORDER_STATUS_ID = "SELECT * FROM orders WHERE order_status_id = ?";
    private static final String SQL_FIND_BY_STATUS = "SELECT * FROM orders WHERE user_id = ? AND order_status_id = (SELECT order_status_id FROM order_status WHERE status = ?)";
    private static final String SQL_FIND_BY_TYPE = "SELECT * FROM orders WHERE user_id = ? AND order_type_id = (SELECT order_type_id FROM order_type WHERE type = ?)";
    private static final String SQL_FIND_BY_DATES_AND_ROOM_ID = "SELECT * FROM orders WHERE room_id=? AND (DATE(?) <= date_check_in AND DATE(?) >= date_check_in) OR (DATE(?) >= date_check_in AND DATE(?) <= date_check_out)";
    private static final String STATUS_NOT_CONFIRMED = "Notconfirmed";
    private static final String STATUS_CONFIRMED = "Confirmed";
    private static final String STATUS_CANCELLED = "Cancelled";
    private static final String TYPE_RESERVATION = "Reservation";
    private static final String TYPE_PAYMENT = "Payment";
    private static final String ORDER_ID = "orders.order_id";
    private static final String ORDER_USER_ID = "orders.user_id";
    private static final String ORDER_PLACES = "orders.places";
    private static final String ORDER_REGISTRATION_DATE = "orders.registration_date";
    private static final String ORDER_DATE_CHECK_IN = "orders.date_check_in";
    private static final String ORDER_DATE_CHECK_OUT = "orders.date_check_out";
    private static final String ORDER_ROOM_ID = "orders.room_id";
    private static final String ORDER_ORDER_STATUS_ID = "orders.order_status_id";
    private static final String ORDER_ORDER_TYPE_ID = "orders.order_type_id";
    private static final String ORDER_DISCOUNT_ID = "orders.discount_id";
    private static final String ORDER_TOTAL_AMOUNT = "orders.total_amount";
    private Lock lock = new ReentrantLock();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    private static class InstanceHolder {
        private static final MySqlOrderDAO INSTANCE = new MySqlOrderDAO();
    }

    private MySqlOrderDAO() {
    }

    public static MySqlOrderDAO getInstance() {
        return MySqlOrderDAO.InstanceHolder.INSTANCE;
    }

    @Override
    public boolean create(Order order) {
        try (PoolConnection connection = pool.getConnection();
             PreparedStatement statement = connection.getConnection().prepareStatement(SQL_CREATE)) {
            lock.lock();
            statement.setInt(1, order.getUser().getId());
            statement.setInt(2, order.getPlaces());
            statement.setDate(3, order.getRegistrationDate());
            statement.setDate(4, order.getCheckIn());
            statement.setDate(5, order.getCheckOut());
            statement.setInt(6, order.getRoom().getId());
            statement.setInt(7, order.getOrderStatus().getId());
            statement.setInt(8, order.getOrderType().getId());
            statement.setInt(9, order.getDiscount().getId());
            statement.setDouble(10, order.getTotalAmount());
            statement.execute();
            return true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return false;
    }

    @Override
    public Order findEntityById(int key) {
        Order result = null;
        ResultSet resultSet = null;
        try (PoolConnection poolConnection = pool.getConnection();
             PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_FIND)) {
            statement.setInt(1, key);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = createOrder(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }

    @Override
    public List<Order> findAll() {
        List<Order> result = new ArrayList<>();
        try (PoolConnection connection = pool.getConnection();
             PreparedStatement statement = connection.getConnection().prepareStatement(SQL_FIND_ALL);
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Order order = createOrder(resultSet);
                result.add(order);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return result;
    }

    @Override
    public boolean update(Order order) {
        try (PoolConnection connection = pool.getConnection();
             PreparedStatement statement = connection.getConnection().prepareStatement(SQL_UPDATE)) {
            lock.lock();
            statement.setInt(1, order.getUser().getId());
            statement.setInt(2, order.getPlaces());
            statement.setDate(3, order.getRegistrationDate());
            statement.setDate(4, order.getCheckIn());
            statement.setDate(5, order.getCheckOut());
            statement.setInt(6, order.getRoom().getId());
            statement.setInt(7, order.getOrderStatus().getId());
            statement.setInt(8, order.getOrderType().getId());
            statement.setInt(9, order.getDiscount().getId());
            statement.setDouble(10, order.getTotalAmount());
            statement.setInt(11, order.getId());
            statement.execute();
            return true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return false;
    }

    @Override
    public boolean delete(int key) {
        boolean result = false;
        try (PoolConnection poolConnection = pool.getConnection();
             PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_DELETE)) {
            lock.lock();
            statement.setInt(1, key);
            statement.executeUpdate();
            result = true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return result;
    }

    @Override
    public List<Order> execute(QueriesEnum query, Object[] params) {
        List<Order> result = null;
        switch (query) {
            case ORDER_BY_DATE_CHECK_IN:
                Date checkIn = (Date) params[0];
                result = findByCheckIn(checkIn);
                break;
            case ORDER_BY_DATE_CHECK_OUT:
                Date checkOut = (Date) params[0];
                result = findByCheckOut(checkOut);
                break;
            case ORDER_BY_DATES_AND_ROOM_ID:
                int roomId = (Integer)params[0];
                String start = (String) params[1];
                String end = (String) params[2];
                result = findByDatesAndRoomId(roomId, start, end);
                break;
        }
        return result;
    }

    @Override
    public List<Order> execute(QueriesEnum query, int param) {
        List<Order> result = null;
        switch (query) {
            case ORDER_BY_USER_ID:
                result = findByUserId(param);
                break;
            case ORDER_BY_ROOM_ID:
                result = findByRoomId(param);
                break;
            case ORDER_BY_ORDER_STATUS:
                result = findByOrderStatusId(param);
                break;
            case ORDER_NOT_CONFIRMED:
                result = getOrdersByStatus(param, STATUS_NOT_CONFIRMED);
                break;
            case ORDER_CONFIRMED:
                result = getOrdersByStatus(param, STATUS_CONFIRMED);
                break;
            case ORDER_CANCELLED:
                result = getOrdersByStatus(param, STATUS_CANCELLED);
                break;
            case ORDER_RESERVATION:
                result = getOrdersByType(param, TYPE_RESERVATION);
                break;
            case ORDER_PAYMENT:
                result = getOrdersByType(param, TYPE_PAYMENT);
                break;
        }
        return result;
    }

    private Order createOrder(ResultSet resultSet) {
        Order result = null;
        try {
            int id = resultSet.getInt(ORDER_ID);
            int userId = resultSet.getInt(ORDER_USER_ID);
            int places = resultSet.getInt(ORDER_PLACES);
            Date registrationDate = resultSet.getDate(ORDER_REGISTRATION_DATE);
            Date checkIn = resultSet.getDate(ORDER_DATE_CHECK_IN);
            Date checkOut = resultSet.getDate(ORDER_DATE_CHECK_OUT);
            int roomId = resultSet.getInt(ORDER_ROOM_ID);
            int orderStatusId = resultSet.getInt(ORDER_ORDER_STATUS_ID);
            int orderTypeId = resultSet.getInt(ORDER_ORDER_TYPE_ID);
            int discountId = resultSet.getInt(ORDER_DISCOUNT_ID);
            double totalAmount = resultSet.getDouble(ORDER_TOTAL_AMOUNT);
            User user = UserService.getUserById(userId);
            Room room = RoomService.getRoomById(roomId);
            OrderStatus orderStatus = OrderStatusService.getOrderStatusById(orderStatusId);
            OrderType orderType = OrderTypeService.getOrderTypeById(orderTypeId);
            Discount discount = DiscountService.getDiscountById(discountId);
            result = new Order(id, user, places, registrationDate, checkIn, checkOut, room, orderStatus, orderType, discount, totalAmount);
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return result;
    }

    private List<Order> findByUserId(int userId) {
        List<Order> result = new ArrayList<>();
        ResultSet resultSet = null;
        try (PoolConnection poolConnection = pool.getConnection();
             PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_FIND_BY_USER_ID)) {
            statement.setInt(1, userId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Order order = createOrder(resultSet);
                result.add(order);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }

    private List<Order> findByRoomId(int roomId) {
        List<Order> result = new ArrayList<>();
        ResultSet resultSet = null;
        try (PoolConnection poolConnection = pool.getConnection();
             PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_FIND_BY_ROOM_ID)) {
            statement.setInt(1, roomId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Order order = createOrder(resultSet);
                result.add(order);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }

    private List<Order> findByOrderStatusId(int orderStatusId) {
        List<Order> result = new ArrayList<>();
        ResultSet resultSet = null;
        try (PoolConnection poolConnection = pool.getConnection();
             PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_FIND_BY_ORDER_STATUS_ID)) {
            statement.setInt(1, orderStatusId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Order order = createOrder(resultSet);
                result.add(order);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }

    private List<Order> findByCheckIn(Date checkIn) {
        List<Order> result = new ArrayList<>();
        ResultSet resultSet = null;
        try (PoolConnection poolConnection = pool.getConnection();
             PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_FIND_BY_DATE_CHECK_IN)) {
            statement.setDate(1, checkIn);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Order order = createOrder(resultSet);
                result.add(order);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }


    private List<Order> findByCheckOut(Date checkOut) {
        List<Order> result = new ArrayList<>();
        ResultSet resultSet = null;
        try (PoolConnection poolConnection = pool.getConnection();
             PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_FIND_BY_DATE_CHECK_OUT)) {
            statement.setDate(1, checkOut);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Order order = createOrder(resultSet);
                result.add(order);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }

    private List<Order> findByDatesAndRoomId(int roomId, String start, String end) {
        List<Order> result = new ArrayList<>();
        ResultSet resultSet = null;
        try (PoolConnection poolConnection = pool.getConnection();
             PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_FIND_BY_DATES_AND_ROOM_ID)) {
            statement.setInt(1, roomId);
            statement.setString(2, start);
            statement.setString(3, end);
            statement.setString(4, start);
            statement.setString(5, start);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Order order = createOrder(resultSet);
                result.add(order);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }

    private List<Order> getOrdersByStatus(int userId, String status) {
        List<Order> result = new ArrayList<>();
        ResultSet resultSet = null;
        try (PoolConnection poolConnection = pool.getConnection();
             PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_FIND_BY_STATUS)) {
            statement.setInt(1, userId);
            statement.setString(2, status);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Order order = createOrder(resultSet);
                result.add(order);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }

    private List<Order> getOrdersByType(int userId, String type) {
        List<Order> result = new ArrayList<>();
        ResultSet resultSet = null;
        try (PoolConnection poolConnection = pool.getConnection();
             PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_FIND_BY_TYPE)) {
            statement.setInt(1, userId);
            statement.setString(2, type);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Order order = createOrder(resultSet);
                result.add(order);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }
}
