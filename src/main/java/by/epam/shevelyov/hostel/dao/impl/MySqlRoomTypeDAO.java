package by.epam.shevelyov.hostel.dao.impl;

import by.epam.shevelyov.hostel.dao.BaseDAO;
import by.epam.shevelyov.hostel.dao.QueriesEnum;
import by.epam.shevelyov.hostel.entity.impl.RoomType;
import by.epam.shevelyov.hostel.pool.ConnectionsPool;
import by.epam.shevelyov.hostel.pool.PoolConnection;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * <p>Class for working with the {@link RoomType} entity from MySQL database</p>
 *
 * @author Ilya Shevelyov
 */
public class MySqlRoomTypeDAO implements BaseDAO<RoomType> {
    private static final Logger LOGGER = Logger.getLogger(MySqlRoomTypeDAO.class);
    private static final String SQL_CREATE = "INSERT INTO room_type(description, capacity, amount) VALUES(?,?,?)";
    private static final String SQL_FIND = "SELECT * FROM room_type WHERE room_type_id=? LIMIT 1";
    private static final String SQL_FIND_ALL = "SELECT * FROM room_type";
    private static final String SQL_UPDATE = "UPDATE room_type SET description=?, capacity=?, amount=? WHERE room_type_id=?";
    private static final String SQL_DELETE = "DELETE FROM room_type WHERE room_type_id = ?";
    private static final String SQL_FIND_BY_CAPACITY = "SELECT * FROM room_type WHERE capacity = ?";
    private static final String SQL_FIND_BY_DESCRIPTION = "SELECT * FROM room_type WHERE description = ?";
    private static final String ROOM_TYPE_ID = "room_type.room_type_id";
    private static final String ROOM_TYPE_DESCRIPTION = "room_type.description";
    private static final String ROOM_TYPE_CAPACITY = "room_type.capacity";
    private static final String ROOM_TYPE_AMOUNT = "room_type.amount";
    private Lock lock = new ReentrantLock();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    private static class InstanceHolder {
        private static final MySqlRoomTypeDAO INSTANCE = new MySqlRoomTypeDAO();
    }

    private MySqlRoomTypeDAO() {
    }

    public static MySqlRoomTypeDAO getInstance() {
        return MySqlRoomTypeDAO.InstanceHolder.INSTANCE;
    }

    @Override
    public boolean create(RoomType roomType) {
        try (PoolConnection connection = pool.getConnection();
             PreparedStatement statement = connection.getConnection().prepareStatement(SQL_CREATE)) {
            lock.lock();
            statement.setString(1, roomType.getDescription());
            statement.setInt(2, roomType.getCapacity());
            statement.setDouble(3, roomType.getAmount());
            statement.execute();
            return true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return false;
    }

    @Override
    public RoomType findEntityById(int key) {
        RoomType result = null;
        ResultSet resultSet = null;
        try (PoolConnection poolConnection = pool.getConnection();
             PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_FIND)) {
            statement.setInt(1, key);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = createRoomType(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }

    @Override
    public List<RoomType> findAll() {
        List<RoomType> result = new ArrayList<>();
        try (PoolConnection connection = pool.getConnection();
             PreparedStatement statement = connection.getConnection().prepareStatement(SQL_FIND_ALL);
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                RoomType roomType = createRoomType(resultSet);
                result.add(roomType);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return result;
    }

    @Override
    public boolean update(RoomType roomType) {
        try (PoolConnection connection = pool.getConnection();
             PreparedStatement statement = connection.getConnection().prepareStatement(SQL_UPDATE)) {
            lock.lock();
            statement.setString(1, roomType.getDescription());
            statement.setInt(2, roomType.getCapacity());
            statement.setDouble(3, roomType.getAmount());
            statement.setInt(4, roomType.getId());
            statement.execute();
            return true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return false;
    }

    @Override
    public boolean delete(int key) {
        boolean result = false;
        try (PoolConnection poolConnection = pool.getConnection();
             PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_DELETE)) {
            lock.lock();
            statement.setInt(1, key);
            statement.executeUpdate();
            result = true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return result;
    }

    @Override
    public List<RoomType> execute(QueriesEnum query, Object[] params) {
        List<RoomType> result = null;
        switch (query) {
            case ROOM_TYPE_BY_DESCRIPTION:
                String description = (String) params[0];
                result = findByDescription(description);
                break;
        }
        return result;
    }

    @Override
    public List<RoomType> execute(QueriesEnum query, int param) {
        List<RoomType> result = null;
        switch (query) {
            case ROOM_TYPE_BY_CAPACITY:
                result = findByCapacity(param);
                break;
        }
        return result;
    }


    /**
     * <p>Method for reading RoomType entity from ResultSet</p>
     *
     * @param resultSet {@link ResultSet} that contains data from query
     * @return {@link RoomType} from resultSet
     */
    private RoomType createRoomType(ResultSet resultSet) {
        RoomType result = null;
        try {
            int id = resultSet.getInt(ROOM_TYPE_ID);
            String description = resultSet.getString(ROOM_TYPE_DESCRIPTION);
            int capacity = resultSet.getInt(ROOM_TYPE_CAPACITY);
            double amount = resultSet.getDouble(ROOM_TYPE_AMOUNT);
            result = new RoomType(id, description, capacity, amount);
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return result;
    }

    private List<RoomType> findByCapacity(int capacity) {
        List<RoomType> result = new ArrayList<>();
        ResultSet resultSet = null;
        try (PoolConnection poolConnection = pool.getConnection();
             PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_FIND_BY_CAPACITY)) {
            statement.setInt(1, capacity);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                RoomType roomType = createRoomType(resultSet);
                result.add(roomType);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }

    private List<RoomType> findByDescription(String description) {
        List<RoomType> result = new ArrayList<>();
        ResultSet resultSet = null;
        try (PoolConnection poolConnection = pool.getConnection();
             PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_FIND_BY_DESCRIPTION)) {
            statement.setString(1, description);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                RoomType roomType = createRoomType(resultSet);
                result.add(roomType);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }
}
