package by.epam.shevelyov.hostel.dao.factory;

import by.epam.shevelyov.hostel.dao.DAOEnum;

/**
 * <p>Main factory that defines right DAOFactory based on type of database</p>
 *
 * @author Ilya Shevelyov
 */
public class BaseFactory {

    /**
     * <p>Method for defining {@link DAOFactory}</p>
     *
     * @param key type of the database
     * @return {@link DAOFactory} for the used database
     */
    public static DAOFactory defineFactory(DAOEnum key) {
        return key.getFactory();
    }
}
