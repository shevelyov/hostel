package by.epam.shevelyov.hostel.dao.impl;

import by.epam.shevelyov.hostel.dao.BaseDAO;
import by.epam.shevelyov.hostel.dao.QueriesEnum;
import by.epam.shevelyov.hostel.entity.impl.OrderType;
import by.epam.shevelyov.hostel.pool.ConnectionsPool;
import by.epam.shevelyov.hostel.pool.PoolConnection;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * <p>Class for working with the {@link OrderType} entity from MySQL database</p>
 *
 * @author Ilya Shevelyov
 */
public class MySqlOrderTypeDAO implements BaseDAO<OrderType> {
    private static final Logger LOGGER = Logger.getLogger(MySqlOrderTypeDAO.class);
    private static final String SQL_CREATE = "INSERT INTO order_type(type) VALUES(?)";
    private static final String SQL_FIND = "SELECT * FROM order_type WHERE order_type_id=? LIMIT 1";
    private static final String SQL_FIND_ALL = "SELECT * FROM order_type";
    private static final String SQL_UPDATE = "UPDATE order_type SET type=? WHERE order_type_id=?";
    private static final String SQL_DELETE = "DELETE FROM order_type WHERE order_type_id = ?";
    private static final String ORDER_TYPE_ID = "order_type.order_type_id";
    private static final String ORDER_TYPE_TYPE = "order_type.type";
    private Lock lock = new ReentrantLock();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    private static class InstanceHolder {
        private static final MySqlOrderTypeDAO INSTANCE = new MySqlOrderTypeDAO();
    }

    private MySqlOrderTypeDAO() {
    }

    public static MySqlOrderTypeDAO getInstance() {
        return MySqlOrderTypeDAO.InstanceHolder.INSTANCE;
    }

    @Override
    public boolean create(OrderType orderType) {
        try (PoolConnection connection = pool.getConnection();
             PreparedStatement statement = connection.getConnection().prepareStatement(SQL_CREATE)) {
            lock.lock();
            statement.setString(1, orderType.getType());
            statement.execute();
            return true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return false;
    }

    @Override
    public OrderType findEntityById(int key) {
        OrderType result = null;
        ResultSet resultSet = null;
        try (PoolConnection poolConnection = pool.getConnection();
             PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_FIND)) {
            statement.setInt(1, key);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = createOrderType(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }

    @Override
    public List<OrderType> findAll() {
        List<OrderType> result = new ArrayList<>();
        try (PoolConnection connection = pool.getConnection();
             PreparedStatement statement = connection.getConnection().prepareStatement(SQL_FIND_ALL);
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                OrderType orderType = createOrderType(resultSet);
                result.add(orderType);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return result;
    }

    @Override
    public boolean update(OrderType orderType) {
        try (PoolConnection connection = pool.getConnection();
             PreparedStatement statement = connection.getConnection().prepareStatement(SQL_UPDATE)) {
            lock.lock();
            statement.setString(1, orderType.getType());
            statement.setInt(2, orderType.getId());
            statement.execute();
            return true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return false;
    }

    @Override
    public boolean delete(int key) {
        boolean result = false;
        try (PoolConnection poolConnection = pool.getConnection();
             PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_DELETE)) {
            lock.lock();
            statement.setInt(1, key);
            statement.executeUpdate();
            result = true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return result;
    }

    @Override
    public List<OrderType> execute(QueriesEnum query, Object[] params) {
        return null;
    }

    @Override
    public List<OrderType> execute(QueriesEnum query, int param) {
        return null;
    }

    private OrderType createOrderType(ResultSet resultSet) {
        OrderType result = null;
        try {
            int id = resultSet.getInt(ORDER_TYPE_ID);
            String type = resultSet.getString(ORDER_TYPE_TYPE);
            result = new OrderType(id, type);
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return result;
    }
}
