package by.epam.shevelyov.hostel.dao.factory;

import by.epam.shevelyov.hostel.dao.BaseDAO;
import by.epam.shevelyov.hostel.entity.impl.*;

/**
 * <p>Interface for DAO factory for all entities</p>
 *
 * @author Ilya Shevelyov
 */
public interface DAOFactory {
    BaseDAO<Ban> getBanDAO();
    BaseDAO<RoomType> getRoomTypeDAO();
    BaseDAO<Room> getRoomDAO();
    BaseDAO<Discount> getDiscountDAO();
    BaseDAO<OrderStatus> getOrderStatusDAO();
    BaseDAO<OrderType> getOrderTypeDAO();
    BaseDAO<Order> getOrderDAO();
    BaseDAO<User> getUserDAO();
}
