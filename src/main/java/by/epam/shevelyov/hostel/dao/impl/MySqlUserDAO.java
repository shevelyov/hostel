package by.epam.shevelyov.hostel.dao.impl;

import by.epam.shevelyov.hostel.dao.BaseDAO;
import by.epam.shevelyov.hostel.dao.QueriesEnum;
import by.epam.shevelyov.hostel.entity.impl.User;
import by.epam.shevelyov.hostel.entity.util.RoleEnum;
import by.epam.shevelyov.hostel.pool.ConnectionsPool;
import by.epam.shevelyov.hostel.pool.PoolConnection;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


/**
 * <p>Class for working with the {@link User} entity from MySQL database</p>
 *
 * @author Ilya Shevelyov
 */
public class MySqlUserDAO implements BaseDAO<User> {
    private static final Logger LOGGER = Logger.getLogger(MySqlUserDAO.class);
    private static final String SQL_CREATE = "INSERT INTO user(login, password, first_name, last_name, email, phone_number, role) VALUES(?,?,?,?,?,?,?)";
    private static final String SQL_READ = "SELECT * FROM user WHERE user_id=? LIMIT 1";
    private static final String SQL_READ_ALL = "SELECT * FROM user";
    private static final String SQL_UPDATE = "UPDATE user SET login=?, password=?, first_name=?, last_name=?, email=?, phone_number=?, role=? WHERE user_id=?";
    private static final String SQL_DELETE = "DELETE FROM user WHERE user_id = ?";
    private static final String SQL_READ_BY_LOGIN = "SELECT * FROM user WHERE login = ?";
    private static final String SQL_READ_BY_EMAIL = "SELECT * FROM user WHERE email = ?";
    private static final String SQL_READ_BY_ROLE = "SELECT * FROM user WHERE role = ?";
    private static final String USER_ID = "user.user_id";
    private static final String USER_LOGIN = "user.login";
    private static final String USER_PASSWORD = "user.password";
    private static final String USER_FIRST_NAME = "user.first_name";
    private static final String USER_LAST_NAME = "user.last_name";
    private static final String USER_EMAIL = "user.email";
    private static final String USER_PHONE_NUMBER = "user.phone_number";
    private static final String USER_ROLE = "user.role";
    private Lock lock = new ReentrantLock();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    private static class InstanceHolder {
        private static final MySqlUserDAO INSTANCE = new MySqlUserDAO();
    }

    private MySqlUserDAO() {
    }

    public static MySqlUserDAO getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public boolean create(User user) {
        try (PoolConnection connection = pool.getConnection();
             PreparedStatement statement = connection.getConnection().prepareStatement(SQL_CREATE)) {
            lock.lock();
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getFirstName());
            statement.setString(4, user.getLastName());
            statement.setString(5, user.getEmail());
            statement.setString(6, user.getPhoneNumber());
            statement.setString(7, user.getRole().getValue());
            statement.execute();
            return true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return false;
    }

    @Override
    public User findEntityById(int key) {
        User result = null;
        ResultSet resultSet = null;
        try (PoolConnection poolConnection = pool.getConnection();
             PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_READ)) {
            statement.setInt(1, key);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = createUser(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }

    @Override
    public List<User> findAll() {
        List<User> result = new ArrayList<>();
        try (PoolConnection connection = pool.getConnection();
             PreparedStatement statement = connection.getConnection().prepareStatement(SQL_READ_ALL);
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                User user = createUser(resultSet);
                result.add(user);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return result;
    }

    @Override
    public boolean update(User user) {
        try (PoolConnection connection = pool.getConnection();
             PreparedStatement statement = connection.getConnection().prepareStatement(SQL_UPDATE)) {
            lock.lock();
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getFirstName());
            statement.setString(4, user.getLastName());
            statement.setString(5, user.getEmail());
            statement.setString(6, user.getPhoneNumber());
            statement.setString(7, user.getRole().getValue());
            statement.setInt(8, user.getId());
            statement.execute();
            return true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return false;
    }

    @Override
    public boolean delete(int key) {
        boolean result = false;
        try (PoolConnection poolConnection = pool.getConnection();
             PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_DELETE)) {
            lock.lock();
            statement.setInt(1, key);
            statement.executeUpdate();
            result = true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return result;
    }

    @Override
    public List<User> execute(QueriesEnum query, Object[] params) {
        List<User> result = null;
        User user;
        switch (query) {
            case USER_BY_LOGIN:
                String login = (String) params[0];
                user = readByLogin(login);
                result = new ArrayList<>();
                if (user != null) {
                    result.add(user);
                }
                break;
            case USER_BY_EMAIL:
                String email = (String) params[0];
                user = readByEmail(email);
                result = new ArrayList<>();
                if (user != null) {
                    result.add(user);
                }
                break;
            case USER_BY_ROLE:
                String role = (String) params[0];
                result = readByRole(role);
                break;
        }
        return result;
    }

    @Override
    public List<User> execute(QueriesEnum query, int param) {
        return null;
    }

    /**
     * <p>Method for reading User entity from ResultSet</p>
     *
     * @param resultSet {@link ResultSet} that contains data from query
     * @return {@link User} from resultSet
     */
    private User createUser(ResultSet resultSet) {
        User result = null;
        try {
            int id = resultSet.getInt(USER_ID);
            String login = resultSet.getString(USER_LOGIN);
            String pass = resultSet.getString(USER_PASSWORD);
            String firstName = resultSet.getString(USER_FIRST_NAME);
            String lastName = resultSet.getString(USER_LAST_NAME);
            String email = resultSet.getString(USER_EMAIL);
            String phoneNumber = resultSet.getString(USER_PHONE_NUMBER);
            String role = resultSet.getString(USER_ROLE);
            result = new User(id, login, pass, firstName, lastName, email, phoneNumber, RoleEnum.valueOf(role.toUpperCase()));
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return result;
    }

    private User readByLogin(String login) {
        User result = null;
        ResultSet resultSet = null;
        try (PoolConnection poolConnection = pool.getConnection();
             PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_READ_BY_LOGIN)) {
            statement.setString(1, login);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = createUser(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }

    private User readByEmail(String email) {
        User result = null;
        ResultSet resultSet = null;
        try (PoolConnection poolConnection = pool.getConnection();
             PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_READ_BY_EMAIL)) {
            statement.setString(1, email);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = createUser(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }

    private List<User> readByRole(String role) {
        List<User> result = new ArrayList<>();
        ResultSet resultSet = null;
        try (PoolConnection poolConnection = pool.getConnection();
             PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_READ_BY_ROLE)) {
            statement.setString(1, role);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                User user = createUser(resultSet);
                result.add(user);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }
}



