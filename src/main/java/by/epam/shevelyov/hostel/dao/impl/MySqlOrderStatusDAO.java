package by.epam.shevelyov.hostel.dao.impl;

import by.epam.shevelyov.hostel.dao.BaseDAO;
import by.epam.shevelyov.hostel.dao.QueriesEnum;
import by.epam.shevelyov.hostel.entity.impl.OrderStatus;
import by.epam.shevelyov.hostel.pool.ConnectionsPool;
import by.epam.shevelyov.hostel.pool.PoolConnection;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * <p>Class for working with the {@link OrderStatus} entity from MySQL database</p>
 *
 * @author Ilya Shevelyov
 */
public class MySqlOrderStatusDAO implements BaseDAO<OrderStatus> {
    private static final Logger LOGGER = Logger.getLogger(MySqlOrderStatusDAO.class);
    private static final String SQL_CREATE = "INSERT INTO order_status(status) VALUES(?)";
    private static final String SQL_READ = "SELECT * FROM order_status WHERE order_status_id=? LIMIT 1";
    private static final String SQL_READ_ALL = "SELECT * FROM order_status";
    private static final String SQL_UPDATE = "UPDATE order_status SET status=? WHERE order_status_id=?";
    private static final String SQL_DELETE = "DELETE FROM order_status WHERE order_status_id = ?";
    private static final String SQL_FIND_BY_STATUS = "SELECT * FROM order_status WHERE status = ?";
    private static final String ORDER_STATUS_ID = "order_status.order_status_id";
    private static final String ORDER_STATUS_STATUS = "order_status.status";
    private Lock lock = new ReentrantLock();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    private static class InstanceHolder {
        private static final MySqlOrderStatusDAO INSTANCE = new MySqlOrderStatusDAO();
    }

    private MySqlOrderStatusDAO() {
    }

    public static MySqlOrderStatusDAO getInstance() {
        return MySqlOrderStatusDAO.InstanceHolder.INSTANCE;
    }

    @Override
    public boolean create(OrderStatus orderStatus) {
        try (PoolConnection connection = pool.getConnection();
             PreparedStatement statement = connection.getConnection().prepareStatement(SQL_CREATE)) {
            lock.lock();
            statement.setString(1, orderStatus.getStatus());
            statement.execute();
            return true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return false;
    }

    @Override
    public OrderStatus findEntityById(int key) {
        OrderStatus result = null;
        ResultSet resultSet = null;
        try (PoolConnection poolConnection = pool.getConnection();
             PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_READ)) {
            statement.setInt(1, key);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = createOrderStatus(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }

    @Override
    public List<OrderStatus> findAll() {
        List<OrderStatus> result = new ArrayList<>();
        try (PoolConnection connection = pool.getConnection();
             PreparedStatement statement = connection.getConnection().prepareStatement(SQL_READ_ALL);
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                OrderStatus orderStatus = createOrderStatus(resultSet);
                result.add(orderStatus);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return result;
    }

    @Override
    public boolean update(OrderStatus orderStatus) {
        try (PoolConnection connection = pool.getConnection();
             PreparedStatement statement = connection.getConnection().prepareStatement(SQL_UPDATE)) {
            lock.lock();
            statement.setString(1, orderStatus.getStatus());
            statement.setInt(2, orderStatus.getId());
            statement.execute();
            return true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return false;
    }

    @Override
    public boolean delete(int key) {
        boolean result = false;
        try (PoolConnection poolConnection = pool.getConnection();
             PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_DELETE)) {
            lock.lock();
            statement.setInt(1, key);
            statement.executeUpdate();
            result = true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return result;
    }

    @Override
    public List<OrderStatus> execute(QueriesEnum query, Object[] params) {
        List<OrderStatus> result = null;
        switch (query) {
            case ORDER_STATUS_BY_STATUS:
                String status = (String) params[0];
                result = findByStatus(status);
                break;
        }
        return result;
    }

    @Override
    public List<OrderStatus> execute(QueriesEnum query, int param) {
        return null;
    }

    private OrderStatus createOrderStatus(ResultSet resultSet) {
        OrderStatus result = null;
        try {
            int id = resultSet.getInt(ORDER_STATUS_ID);
            String status = resultSet.getString(ORDER_STATUS_STATUS);
            result = new OrderStatus(id, status);
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return result;
    }

    private List<OrderStatus> findByStatus(String status) {
        List<OrderStatus> result = new ArrayList<>();
        ResultSet resultSet = null;
        try (PoolConnection poolConnection = pool.getConnection();
             PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_FIND_BY_STATUS)) {
            statement.setString(1, status);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                OrderStatus orderStatus = createOrderStatus(resultSet);
                result.add(orderStatus);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }
}
