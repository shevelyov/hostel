package by.epam.shevelyov.hostel.dao.factory.impl;


import by.epam.shevelyov.hostel.dao.factory.DAOFactory;
import by.epam.shevelyov.hostel.dao.impl.*;
import by.epam.shevelyov.hostel.dao.BaseDAO;
import by.epam.shevelyov.hostel.entity.impl.*;

/**
 * <p>MySQL factory that that returns the right DAO based on type of entity</p>
 *
 * @author Ilya Shevelyov
 */
public class MySqlDAOFactory implements DAOFactory {

    @Override
    public BaseDAO<Ban> getBanDAO() {
        return MySqlBanDAO.getInstance();
    }

    @Override
    public BaseDAO<RoomType> getRoomTypeDAO() {
        return MySqlRoomTypeDAO.getInstance();
    }

    @Override
    public BaseDAO<Room> getRoomDAO() {
        return MySqlRoomDAO.getInstance();
    }

    @Override
    public BaseDAO<Discount> getDiscountDAO() {
        return MySqlDiscountDAO.getInstance();
    }

    @Override
    public BaseDAO<OrderStatus> getOrderStatusDAO() {
        return MySqlOrderStatusDAO.getInstance();
    }

    @Override
    public BaseDAO<OrderType> getOrderTypeDAO() {
        return MySqlOrderTypeDAO.getInstance();
    }

    @Override
    public BaseDAO<Order> getOrderDAO() {
        return MySqlOrderDAO.getInstance();
    }

    @Override
    public BaseDAO<User> getUserDAO() {
        return MySqlUserDAO.getInstance();
    }
}
