package by.epam.shevelyov.hostel.dao.impl;

import by.epam.shevelyov.hostel.dao.BaseDAO;
import by.epam.shevelyov.hostel.dao.QueriesEnum;
import by.epam.shevelyov.hostel.entity.impl.Ban;
import by.epam.shevelyov.hostel.entity.impl.User;
import by.epam.shevelyov.hostel.pool.ConnectionsPool;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import by.epam.shevelyov.hostel.pool.PoolConnection;
import by.epam.shevelyov.hostel.service.UserService;
import org.apache.log4j.Logger;

/**
 * <p>Class for working with the {@link Ban} entity from MySQL database</p>
 *
 * @author Ilya Shevelyov
 */
public class MySqlBanDAO implements BaseDAO<Ban> {
    private static final Logger LOGGER = Logger.getLogger(MySqlBanDAO.class);
    private static final String SQL_CREATE = "INSERT INTO ban(user_id, administrator_id, start_date, end_date, reason) VALUES(?,?,?,?,?)";
    private static final String SQL_FIND = "SELECT * FROM ban WHERE ban_id=? LIMIT 1";
    private static final String SQL_FIND_ALL = "SELECT * FROM ban";
    private static final String SQL_UPDATE = "UPDATE ban SET user_id=?, administrator_id=?, start_date=?, end_date=?, reason=? WHERE ban_id=?";
    private static final String SQL_DELETE = "DELETE FROM ban WHERE ban_id = ?";
    private static final String SQL_FIND_BY_USER_ID = "SELECT * FROM ban WHERE user_id = ?";
    private static final String SQL_FIND_BY_REASON = "SELECT * FROM ban WHERE reason = ?";
    private static final String BAN_ID = "ban.ban_id";
    private static final String BAN_USER_ID = "ban.user_id";
    private static final String BAN_ADMINISTRATOR_ID = "ban.administrator_id";
    private static final String BAN_START_DATE = "ban.start_date";
    private static final String BAN_END_DATE = "ban.end_date";
    private static final String BAN_REASON = "ban.reason";
    private Lock lock = new ReentrantLock();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    private static class InstanceHolder {
        private static final MySqlBanDAO INSTANCE = new MySqlBanDAO();
    }

    private MySqlBanDAO() {
    }

    public static MySqlBanDAO getInstance() {
        return MySqlBanDAO.InstanceHolder.INSTANCE;
    }

    @Override
    public boolean create(Ban ban) {
        try (PoolConnection connection = pool.getConnection();
             PreparedStatement statement = connection.getConnection().prepareStatement(SQL_CREATE)) {
            lock.lock();
            statement.setInt(1, ban.getUser().getId());
            statement.setInt(2, ban.getAdmin().getId());
            statement.setDate(3, ban.getStartDate());
            statement.setDate(4, ban.getEndDate());
            statement.setString(5, ban.getReason());
            statement.execute();
            return true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return false;
    }

    @Override
    public Ban findEntityById(int key) {
        Ban result = null;
        ResultSet resultSet = null;
        try (PoolConnection poolConnection = pool.getConnection();
             PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_FIND)) {
            statement.setInt(1, key);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = createBan(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }

    @Override
    public List<Ban> findAll() {
        List<Ban> result = new ArrayList<>();
        try (PoolConnection connection = pool.getConnection();
             PreparedStatement statement = connection.getConnection().prepareStatement(SQL_FIND_ALL);
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Ban ban = createBan(resultSet);
                result.add(ban);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return result;
    }

    @Override
    public boolean update(Ban ban) {
        try (PoolConnection connection = pool.getConnection();
             PreparedStatement statement = connection.getConnection().prepareStatement(SQL_UPDATE)) {
            lock.lock();
            statement.setInt(1, ban.getUser().getId());
            statement.setInt(2, ban.getAdmin().getId());
            statement.setDate(3, ban.getStartDate());
            statement.setDate(4, ban.getEndDate());
            statement.setString(5, ban.getReason());
            statement.setInt(6, ban.getId());
            statement.execute();
            return true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return false;
    }

    @Override
    public boolean delete(int key) {
        boolean result = false;
        try (PoolConnection poolConnection = pool.getConnection();
             PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_DELETE)) {
            lock.lock();
            statement.setInt(1, key);
            statement.executeUpdate();
            result = true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return result;
    }

    @Override
    public List<Ban> execute(QueriesEnum query, Object[] params) {
        List<Ban> result = null;
        switch (query) {
            case BAN_BY_REASON:
                String reason = (String) params[0];
                result = findByReason(reason);
                break;
        }
        return result;
    }

    @Override
    public List<Ban> execute(QueriesEnum query, int param) {
        List<Ban> result = null;
        switch (query) {
            case BAN_BY_USER_ID:
                result = findByUserId(param);
                break;
        }
        return result;
    }

    /**
     * <p>Method for reading Ban entity from ResultSet</p>
     *
     * @param resultSet {@link ResultSet} that contains data from query
     * @return {@link Ban} from resultSet
     */
    private Ban createBan(ResultSet resultSet) {
        Ban result = null;
        try {
            int id = resultSet.getInt(BAN_ID);
            int user_id = resultSet.getInt(BAN_USER_ID);
            int admin_id = resultSet.getInt(BAN_ADMINISTRATOR_ID);
            Date startDate = resultSet.getDate(BAN_START_DATE);
            Date endDate = resultSet.getDate(BAN_END_DATE);
            String reason = resultSet.getString(BAN_REASON);
            User user = UserService.getUserById(user_id);
            User admin = UserService.getUserById(admin_id);
            result = new Ban(id, user, admin, startDate, endDate, reason);
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return result;
    }

    private List<Ban> findByUserId(int userId) {
        List<Ban> result = new ArrayList<>();
        ResultSet resultSet = null;
        try (PoolConnection poolConnection = pool.getConnection();
             PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_FIND_BY_USER_ID)) {
            statement.setInt(1, userId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Ban ban = createBan(resultSet);
                result.add(ban);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }

    private List<Ban> findByReason(String reason) {
        List<Ban> result = new ArrayList<>();
        ResultSet resultSet = null;
        try (PoolConnection poolConnection = pool.getConnection();
             PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_FIND_BY_REASON)) {
            statement.setString(1, reason);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Ban ban = createBan(resultSet);
                result.add(ban);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }
}
