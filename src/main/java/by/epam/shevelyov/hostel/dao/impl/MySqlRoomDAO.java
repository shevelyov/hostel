package by.epam.shevelyov.hostel.dao.impl;

import by.epam.shevelyov.hostel.dao.QueriesEnum;
import by.epam.shevelyov.hostel.dao.BaseDAO;
import by.epam.shevelyov.hostel.entity.impl.Room;
import by.epam.shevelyov.hostel.entity.impl.RoomType;
import by.epam.shevelyov.hostel.pool.ConnectionsPool;
import by.epam.shevelyov.hostel.pool.PoolConnection;
import by.epam.shevelyov.hostel.service.RoomTypeService;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * <p>Class for working with the {@link Room} entity from MySQL database</p>
 *
 * @author Ilya Shevelyov
 */
public class MySqlRoomDAO implements BaseDAO<Room> {
    private static final Logger LOGGER = Logger.getLogger(MySqlRoomDAO.class);
    private static final String SQL_CREATE = "INSERT INTO room(room_type_id, room_number) VALUES(?,?)";
    private static final String SQL_FIND = "SELECT * FROM room WHERE room_id=? LIMIT 1";
    private static final String SQL_FIND_ALL = "SELECT * FROM room";
    private static final String SQL_UPDATE = "UPDATE room SET room_type_id=?, room_number=? WHERE room_id=?";
    private static final String SQL_DELETE = "DELETE FROM room WHERE room_id = ?";
    private static final String SQL_FIND_BY_ROOM_TYPE_ID = "SELECT * FROM room WHERE room_type_id = ?";
    private static final String ROOM_ID = "room.room_id";
    private static final String ROOM_ROOM_NUMBER = "room.room_number";
    private static final String ROOM_ROOM_TYPE_ID = "room.room_type_id";
    private Lock lock = new ReentrantLock();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    private static class InstanceHolder {
        private static final MySqlRoomDAO INSTANCE = new MySqlRoomDAO();
    }

    private MySqlRoomDAO() {
    }

    public static MySqlRoomDAO getInstance() {
        return MySqlRoomDAO.InstanceHolder.INSTANCE;
    }

    @Override
    public boolean create(Room room) {
        try (PoolConnection connection = pool.getConnection();
             PreparedStatement statement = connection.getConnection().prepareStatement(SQL_CREATE)) {
            lock.lock();
            statement.setInt(1, room.getRoomType().getId());
            statement.setString(2, room.getRoomNumber());
            statement.execute();
            return true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return false;
    }

    @Override
    public Room findEntityById(int key) {
        Room result = null;
        ResultSet resultSet = null;
        try (PoolConnection poolConnection = pool.getConnection();
             PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_FIND)) {
            statement.setInt(1, key);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = createRoom(resultSet);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }

    @Override
    public List<Room> findAll() {
        List<Room> result = new ArrayList<>();
        try (PoolConnection connection = pool.getConnection();
             PreparedStatement statement = connection.getConnection().prepareStatement(SQL_FIND_ALL);
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Room room = createRoom(resultSet);
                result.add(room);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return result;
    }

    @Override
    public boolean update(Room room) {
        try (PoolConnection connection = pool.getConnection();
             PreparedStatement statement = connection.getConnection().prepareStatement(SQL_UPDATE)) {
            lock.lock();
            statement.setInt(1, room.getRoomType().getId());
            statement.setString(2, room.getRoomNumber());
            statement.setInt(3, room.getId());
            statement.execute();
            return true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return false;
    }

    @Override
    public boolean delete(int key) {
        boolean result = false;
        try (PoolConnection poolConnection = pool.getConnection();
             PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_DELETE)) {
            lock.lock();
            statement.setInt(1, key);
            statement.executeUpdate();
            result = true;
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            lock.unlock();
        }
        return result;
    }

    @Override
    public List<Room> execute(QueriesEnum query, Object[] params) {
        return null;
    }

    @Override
    public List<Room> execute(QueriesEnum query, int param) {
        List<Room> result = null;
        switch (query) {
            case ROOM_BY_ROOM_TYPE_ID:
                result = findByRoomTypeId(param);
                break;
        }
        return result;
    }


    /**
     * <p>Method for reading Room entity from ResultSet</p>
     *
     * @param resultSet {@link ResultSet} that contains data from query
     * @return {@link Room} from resultSet
     */
    private Room createRoom(ResultSet resultSet) {
        Room result = null;
        try {
            int id = resultSet.getInt(ROOM_ID);
            int roomTypeId = resultSet.getInt(ROOM_ROOM_TYPE_ID);
            String roomNumber = resultSet.getString(ROOM_ROOM_NUMBER);
            RoomType roomType = RoomTypeService.getRoomTypeById(roomTypeId);
            result = new Room(id, roomType, roomNumber);
        } catch (SQLException e) {
            LOGGER.error(e);
        }
        return result;
    }

    private List<Room> findByRoomTypeId(int roomTypeId) {
        List<Room> result = new ArrayList<>();
        ResultSet resultSet = null;
        try (PoolConnection poolConnection = pool.getConnection();
             PreparedStatement statement = poolConnection.getConnection().prepareStatement(SQL_FIND_BY_ROOM_TYPE_ID)) {
            statement.setInt(1, roomTypeId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Room room = createRoom(resultSet);
                result.add(room);
            }
        } catch (SQLException e) {
            LOGGER.error(e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    LOGGER.error(e);
                }
            }
        }
        return result;
    }

}
