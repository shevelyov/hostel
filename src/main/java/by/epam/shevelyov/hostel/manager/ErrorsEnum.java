package by.epam.shevelyov.hostel.manager;

/**
 * <p>Enum that store keys for all errors messages to the log-file</p>
 *
 * @author Ilya Shevelyov
 */
public enum ErrorsEnum {
    PROPERTIES_LOAD_FAILED("error.properties.load.failed"),
    DB_CONFIG_LOAD_FAILED("error.db.config.load.failed"),
    DB_DRIVER_LOAD_FAILED("error.db.driver.load.failed"),
    DB_POOL_INIT("error.db.pool.init"),
    DB_POOL_GET_CONNECTION("error.db.pool.get.connection"),
    DB_POOL_DESTROY("error.db.pool.destroy"),
    DB_POOL_TIMEOUT("error.db.pool.timeout"),
    DAO_ADMIN_CREATE("error.dao.admin.create"),
    DAO_ADMIN_FIND("error.dao.admin.find"),
    DAO_ADMIN_DELETE("error.dao.admin.delete"),
    DAO_ADMIN_UPDATE("error.dao.admin.update"),
    DAO_ADMIN_FIND_ALL("error.dao.admin.find.all"),
    DAO_USER_CREATE("error.dao.user.create"),
    DAO_USER_FIND("error.dao.user.find"),
    DAO_USER_DELETE("error.dao.user.delete"),
    DAO_USER_UPDATE("error.dao.user.update"),
    DAO_USER_FIND_ALL("error.dao.user.find.all"),
    DAO_BAN_CREATE("error.dao.ban.create"),
    DAO_BAN_FIND("error.dao.ban.find"),
    DAO_BAN_FIND_ALL("error.dao.ban.find.all"),
    DAO_BAN_DELETE("error.dao.ban.delete"),
    DAO_BAN_UPDATE("error.dao.ban.update"),
    DAO_DISCOUNT_CREATE("error.dao.discount.create"),
    DAO_DISCOUNT_UPDATE("error.dao.discount.update"),
    DAO_DISCOUNT_FIND("error.dao.discount.find"),
    DAO_DISCOUNT_FIND_ALL("error.dao.discount.find.all"),
    DAO_DISCOUNT_DELETE("error.dao.discount.delete"),
    DAO_ORDER_STATUS_CREATE("error.dao.orderStatus.create"),
    DAO_ORDER_STATUS_FIND("error.dao.orderStatus.find"),
    DAO_ORDER_STATUS_FIND_ALL("error.dao.orderStatus.find.all"),
    DAO_ORDER_STATUS_DELETE("error.dao.orderStatus.delete"),
    DAO_ORDER_STATUS_UPDATE("error.dao.orderStatus.update"),
    DAO_ORDER_TYPE_CREATE("error.dao.orderStatus.create"),
    DAO_ORDER_TYPE_FIND("error.dao.orderStatus.find"),
    DAO_ORDER_TYPE_FIND_ALL("error.dao.orderStatus.find.all"),
    DAO_ORDER_TYPE_DELETE("error.dao.orderStatus.delete"),
    DAO_ORDER_TYPE_UPDATE("error.dao.orderStatus.update"),
    DAO_ROOM_TYPE_CREATE("error.dao.roomType.create"),
    DAO_ROOM_TYPE_FIND("error.dao.roomType.find"),
    DAO_ROOM_TYPE_FIND_ALL("error.dao.roomType.find.all"),
    DAO_ROOM_TYPE_DELETE("error.dao.roomType.delete"),
    DAO_ROOM_TYPE_UPDATE("error.dao.roomType.update"),
    DAO_ROOM_CREATE("error.dao.room.create"),
    DAO_ROOM_FIND("error.dao.room.find"),
    DAO_ROOM_FIND_ALL("error.dao.room.find.all"),
    DAO_ROOM_DELETE("error.dao.room.delete"),
    DAO_ROOM_UPDATE("error.dao.room.update"),
    DAO_ORDER_CREATE("error.dao.order.create"),
    DAO_ORDER_FIND("error.dao.order.find"),
    DAO_ORDER_FIND_ALL("error.dao.order.find.all"),
    DAO_ORDER_UPDATE("error.dao.order.update"),
    DAO_ORDER_DELETE("error.dao.order.delete"),
    CONTEXT_EXTRACT_FAILED("error.context.extract.failed"),
    CONTEXT_INSERT_FAILED("error.context.insert.failed"),
    CONTEXT_EMPTY_ACTION("error.context.empty.action"),
    VALIDATE_EMPTY_FIELDS("error.validate.empty.fields"),
    VALIDATE_DIFFERENT_PASS("error.validate.different.pass"),
    VALIDATE_EXIST_LOGIN("error.validate.exist.login"),
    VALIDATE_EXIST_EMAIL("error.validate.exist.email"),
    VALIDATE_INVALID_VALUES("error.validate.invalid.values"),
    VALIDATE_CONTEXT("error.validate.context"),
    VALIDATE_WRONG_LOGIN("error.validate.wrong.login"),
    VALIDATE_WRONG_PASSWORD("error.validate.wrong.pass"),
    VALIDATE_SESSION_USER("error.validate.session.user"),
    REGISTRATION_FAILS("error.registration.fails");

    String value;

    ErrorsEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
