package by.epam.shevelyov.hostel.manager;

/**
 * <p>Enum that store keys for pages</p>
 *
 * @author Ilya Shevelyov
 */
public enum PagesEnum {
    INDEX("page.index"),
    MAIN("page.main"),
    ADMIN("page.admin"),
    CATALOG("page.catalog"),
    ROOMS("page.rooms"),
    BOOK("page.book"),
    GALLERY("page.gallery"),
    SERVICE("page.service"),
    ROOMTYPE_DETAILS("page.roomtype.details"),
    PROFILE("page.profile"),
    ERROR_404("page.error.404"),
    GO_TO_MAIN("page.go.to.main"),
    GO_TO_ADD_ADMINISTRATOR("page.go.to.add.administrator"),
    GO_TO_ADD_BAN("page.go.to.add.ban"),
    GO_TO_ADD_DISCOUNT("page.go.to.add.discount"),
    GO_TO_ADD_ROOMTYPE("page.go.to.add.roomtype"),
    GO_TO_ADD_ROOM("page.go.to.add.room"),
    GO_TO_ALL_USERS("page.go.to.all.users"),
    GO_TO_ALL_BANS("page.go.to.all.bans"),
    GO_TO_ALL_DISCOUNTS("page.go.to.all.discounts"),
    GO_TO_ALL_ROOMS("page.go.to.all.rooms"),
    GO_TO_ALL_ROOMTYPES("page.go.to.all.roomtypes"),
    GO_TO_ALL_ADMINISTRATORS("page.go.to.all.administrators"),
    GO_TO_ALL_ORDERS("page.go.to.all.orders"),
    GO_TO_PROFILE("page.go.to.profile"),
    USER_CHANGE_INFORMATION("page.user.change.information"),
    ADMIN_ALL_ORDERS("page.admin.all.orders"),
    ADMIN_ALL_BANS("page.admin.all.bans"),
    ADMIN_ALL_DISCOUNTS("page.admin.all.discounts"),
    ADMIN_ALL_USERS("page.admin.all.users"),
    ADMIN_ALL_ADMINISTRATORS("page.admin.all.administrators"),
    ADMIN_ALL_ROOMS("page.admin.all.rooms"),
    ADMIN_ALL_TEST("page.admin.all.test"),
    ADMIN_ALL_ROOMTYPES("page.admin.all.roomtypes"),
    ADMIN_ADD_ADMINISTRATOR("page.admin.add.administrator"),
    ADMIN_ADD_BAN("page.admin.add.ban"),
    ADMIN_ADD_DISCOUNT("page.admin.add.discount"),
    ADMIN_ADD_ROOMTYPE("page.admin.add.roomtype"),
    ADMIN_ADD_ROOM("page.admin.add.room"),
    ADMIN_SET_DISCOUNT("page.admin.set.discount"),
    ADMIN_USER_DETAILS("page.admin.user.details"),
    ADMIN_CHANGE_ADMINISTRATOR("page.admin.change.administrator"),
    ADMIN_CHANGE_BAN("page.admin.change.ban"),
    ADMIN_CHANGE_DISCOUNT("page.admin.change.discount"),
    ADMIN_CHANGE_ROOMTYPE("page.admin.change.roomtype"),
    ADMIN_CHANGE_ROOM("page.admin.change.room"),
    ADMIN_CHOOSE_PERIOD("page.admin.choose.period"),
    ADMIN_SHOW_AVAILABILITY("page.admin.show.availability"),

    FORMAT_BAN_DETAILS("page.format.ban.details"),
    FORMAT_TOUR_DETAILS("page.format.tour.details"),
    FORMAT_PLACE_DETAILS("page.format.place.details"),
    FORMAT_HOTEL_DETAILS("page.format.hotel.details");

    String value;

    PagesEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}

