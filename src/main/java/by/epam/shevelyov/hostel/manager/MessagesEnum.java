package by.epam.shevelyov.hostel.manager;

/**
 * <p>Enum that store keys for all messages that will be sent to the page</p>
 *
 * @author Ilya Shevelyov
 */
public enum MessagesEnum {
    SUCCESS("param.success"),
    LOG_SERVLET_INIT("log.servlet.init"),
    LOG_SERVLET_DESTROY("log.servlet.destroy"),
    ERROR_BAD_REQUEST("error.bad.request"),
    ERROR_PROCESS_REQUEST("error.process.request"),
    LOG_SERVLET_PROCESS_REQ_START("log.servlet.process.req.start"),
    LOG_SERVLET_PROCESS_REQ_END("log.servlet.process.req.end"),
    LOG_POOL_INIT("log.pool.init"),
    LOG_POOL_DESTROY("log.pool.destroy"),
    LOG_LOGIN("log.login"),
    LOG_LOGOUT("log.logout"),
    LOG_REGISTRATION("log.registration"),
    LOG_ORDER_CREATE("log.order.create"),
    LOG_DISCOUNT_DELETE("log.discount.delete"),
    LOG_ROOM_DELETE("log.room.delete"),
    LOG_ROOMTYPE_DELETE("log.roomtype.delete"),
    LOG_ADMINISTRATOR_DELETE("log.administrator.delete"),
    LOG_ADMINISTRATOR_ADD("log.administrator.add"),
    LOG_BAN_ADD("log.ban.add"),
    LOG_DISCOUNT_ADD("log.discount.add"),
    LOG_ROOMTYPE_ADD("log.roomtype.add"),
    LOG_ROOM_ADD("log.room.add"),
    LOG_ADMINISTRATOR_CHANGE("log.administrator.change"),
    LOG_BAN_CHANGE("log.ban.change"),
    LOG_DISCOUNT_CHANGE("log.discount.change"),
    LOG_ROOM_CHANGE("log.room.change"),
    LOG_ROOMTYPE_CHANGE("log.roomtype.change"),
    LOG_BAN_DELETE("log.ban.delete"),
    LOG_SET_DISCOUNT("log.set.discount"),
    LOG_ORDER_ADDED("log.order.added"),
    LOG_ORDER_DELETE("log.order.delete"),
    LOG_ORDER_CANCEL("log.order.cancel"),
    SUCCESS_LOGIN("success.login"),
    SUCCESS_REGISTRATION("success.registration"),
    SUCCESS_DISCOUNT_DELETE("success.discount.delete"),
    SUCCESS_ROOM_DELETE("success.room.delete"),
    SUCCESS_ROOMTYPE_DELETE("success.roomtype.delete"),
    SUCCESS_BAN_DELETE("success.ban.delete"),
    SUCCESS_ADMINISTRATOR_DELETE("success.administrator.delete"),
    SUCCESS_ADMINISTRATOR_ADD("success.administrator.add"),
    SUCCESS_CHANGE_ORDER_STATUS("success.change.order.status"),
    SUCCESS_ORDER_CREATE("success.order.create"),
    ERROR_DISCOUNT_DELETE("error.discount.delete"),
    ERROR_ROOM_DELETE("error.room.delete"),
    ERROR_ROOMTYPE_DELETE("error.roomtype.delete"),
    ERROR_ADMINISTRATOR_ADD("error.administrator.add"),
    ERROR_USER_CHANGE("error.user.change"),
    ERROR_BAN_ADD("error.ban.add"),
    ERROR_DISCOUNT_ADD("error.discount.add"),
    ERROR_ROOMTYPE_ADD("error.roomtype.add"),
    ERROR_ROOM_ADD("error.room.add"),
    ERROR_BAN_DELETE("error.ban.delete"),
    ERROR_ADMINISTRATOR_DELETE("error.administrator.delete"),
    ERROR_CHANGE_ORDER_STATUS("error.change.order.status"),
    ERROR_CANCEL_ORDER("error.cancel.order"),
    ERROR_SET_DISCOUNT("error.set.discount"),

    ERROR_ADMIN_LOGIN("error.admin.login"),
    ERROR_ADMIN_ACCESS("error.admin.access"),
    ERROR_USER_LOGIN("error.user.login"),
    ERROR_USER_REGISTRATION("error.user.registration"),

    ERROR_ORDER_BOOKING("error.order.booking"),
    ERROR_VALIDATE_EMPTY_ADMINISTRATOR("error.validate.empty.administrator"),
    ERROR_VALIDATE_EMPTY_BAN("error.validate.empty.ban"),
    ERROR_VALIDATE_EMPTY_DISCOUNT("error.validate.empty.discount"),
    ERROR_VALIDATE_EMPTY_USER("error.validate.empty.user"),
    ERROR_VALIDATE_EMPTY_ROOM("error.validate.empty.room"),
    ERROR_VALIDATE_EMPTY_ROOMTYPE("error.validate.empty.roomtype"),
    ERROR_VALIDATE_EMPTY_ORDERTYPE("error.validate.empty.ordertype"),
    ERROR_VALIDATE_EMPTY_FIELDS("error.validate.empty.fields"),
    ERROR_VALIDATE_EMPTY_ORDER("error.validate.empty.order"),
    ERROR_VALIDATE_EMPTY_STATUS("error.validate.empty.status"),
    ERROR_VALIDATE_WRONG_BAN("error.validate.wrong.ban"),
    ERROR_VALIDATE_WRONG_DISCOUNT("error.validate.wrong.discount"),
    ERROR_VALIDATE_WRONG_ADMINISTRATOR("error.validate.wrong.administrator"),
    ERROR_VALIDATE_WRONG_USER("error.validate.wrong.user"),
    ERROR_VALIDATE_WRONG_ROOM("error.validate.wrong.room"),
    ERROR_VALIDATE_WRONG_SHOW("error.validate.wrong.show"),
    ERROR_VALIDATE_WRONG_ROOMTYPE("error.validate.wrong.roomtype"),
    ERROR_VALIDATE_WRONG_ORDERTYPE("error.validate.wrong.ordertype"),
    ERROR_VALIDATE_WRONG_DATE("error.validate.wrong.date"),
    ERROR_VALIDATE_WRONG_DATES("error.validate.wrong.dates"),
    ERROR_VALIDATE_WRONG_GUESTS("error.validate.wrong.guests"),
    ERROR_VALIDATE_WRONG_DATE_FORMAT("error.validate.wrong.date.format"),
    ERROR_VALIDATE_WRONG_PARAMS("error.validate.wrong.params"),
    ERROR_VALIDATE_WRONG_PAGE("error.validate.wrong.page"),
    ERROR_VALIDATE_WRONG_ACTION("error.validate.wrong.action"),
    ERROR_VALIDATE_WRONG_ORDER("error.validate.wrong.order"),
    ERROR_VALIDATE_WRONG_STATUS("error.validate.wrong.status"),
    ERROR_VALIDATE_NO_PLACES("error.validate.no.places"),
    ERROR_VALIDATE_SESSION_USER("error.validate.session.user"),
    ERROR_VALIDATE_WRONG_LANGUAGE("error.validate.wrong.language"),
    ERROR_VALIDATE_WRONG_LOGIN("error.validate.wrong.login"),
    ERROR_VALIDATE_WRONG_PASSWORD("error.validate.wrong.password"),
    ERROR_VALIDATE_WRONG_ROLE("error.validate.wrong.role"),
    ERROR_VALIDATE_BANNED_USER("error.validate.banned.user"),
    ERROR_VALIDATE_EXIST_LOGIN("error.validate.exist.login"),
    ERROR_VALIDATE_EXIST_EMAIL("error.validate.exist.email");

    String value;

    MessagesEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

}
