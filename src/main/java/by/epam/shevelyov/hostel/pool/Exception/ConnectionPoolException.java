package by.epam.shevelyov.hostel.pool.Exception;

import by.epam.shevelyov.hostel.pool.PoolConnection;

/**
 * <p>Exception throws when error occurred while working with {@link PoolConnection}</p>
 *
 * @author Ilya Shevelyov
 */
public class ConnectionPoolException extends Exception {
    public ConnectionPoolException() {
        super();
    }

    public ConnectionPoolException(String message) {
        super(message);
    }

    public ConnectionPoolException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConnectionPoolException(Throwable cause) {
        super(cause);
    }
}