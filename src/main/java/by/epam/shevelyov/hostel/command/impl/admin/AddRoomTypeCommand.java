package by.epam.shevelyov.hostel.command.impl.admin;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.controller.MethodsEnum;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.RoomType;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.MessagesManager;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;
import by.epam.shevelyov.hostel.service.RoomTypeService;
import by.epam.shevelyov.hostel.validator.impl.admin.AddRoomTypeValidator;
import org.apache.log4j.Logger;

public class AddRoomTypeCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(AddRoomTypeCommand.class);
    private static final String METHOD = "method";
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String DESCRIPTION = "description";
    private static final String CAPACITY = "capacity";
    private static final String AMOUNT = "amount";
    private AddRoomTypeValidator validator = AddRoomTypeValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_ADD_ROOMTYPE);
            }
            String description = context.getRequestParameter(DESCRIPTION)[0];
            int capacity = Integer.parseInt(context.getRequestParameter(CAPACITY)[0]);
            double amount = Double.parseDouble(context.getRequestParameter(AMOUNT)[0]);
            RoomType roomType  = new RoomType(0, description, capacity, amount);
            if (!RoomTypeService.addRoomType(roomType)) {
                context.addRequestAttribute(ERROR_MESSAGE, MessagesEnum.ERROR_ROOMTYPE_ADD.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_ADD_ROOMTYPE);
            } else {
                context.addRequestAttribute(METHOD, MethodsEnum.REDIRECT);
            }
            result = PagesManager.getPage(PagesEnum.GO_TO_ALL_ROOMTYPES);
            LOGGER.info(MessagesManager.getMessage(MessagesEnum.LOG_ROOMTYPE_ADD));
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}