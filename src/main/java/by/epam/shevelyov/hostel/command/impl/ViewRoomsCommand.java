package by.epam.shevelyov.hostel.command.impl;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.comparator.SmallestRoomTypeComparator;
import by.epam.shevelyov.hostel.comparator.CheapestRoomTypeComparator;
import by.epam.shevelyov.hostel.controller.MethodsEnum;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.RoomType;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;
import by.epam.shevelyov.hostel.service.RoomTypeService;
import by.epam.shevelyov.hostel.validator.impl.ViewRoomsValidator;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class ViewRoomsCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(ViewRoomsCommand.class);
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String METHOD = "method";
    private static final String ACTION = "action";
    private static final String BIGGEST_ROOM = "biggest_room";
    private static final String SMALLEST_ROOM = "smallest_room";
    private static final String CHEAPEST_ROOM = "cheapest_room";
    private static final String MOST_EXPENSIVE_ROOM = "most_expensive_room";
    private static final String ROOMTYPES = "roomtypes";
    private static final String PAGE = "page";
    private static final int ROOMS_PER_PAGE = 3;
    private ViewRoomsValidator validator = ViewRoomsValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_MAIN);
            }
            int page = Integer.parseInt(context.getRequestParameter(PAGE)[0]);
            List<RoomType> allRoomTypes = defineRooms(context);
            List<RoomType> roomTypes = createRooms(allRoomTypes, page);
            context.addRequestAttribute(PAGE, page);
            context.addRequestAttribute(ROOMTYPES, roomTypes);
            result = PagesManager.getPage(PagesEnum.ROOMS);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }

    private List<RoomType> createRooms(List<RoomType> allRoomTypes, int page) {
        List<RoomType> result = new ArrayList<>();
        int start = (page - 1) * ROOMS_PER_PAGE;
        int end = start + ROOMS_PER_PAGE - 1;
        for (int i = start; i <= end; i++) {
            if (i > allRoomTypes.size() - 1) {
                break;
            }
            result.add(allRoomTypes.get(i));
        }
        return result;
    }

    private List<RoomType> defineRooms(RequestContext context) {
        String action = context.getRequestParameter(ACTION)[0];
        List<RoomType> result = RoomTypeService.getAllRoomTypes();
        switch (action) {
            case SMALLEST_ROOM:
                context.addRequestAttribute(ACTION, action);
                Collections.sort(result, new SmallestRoomTypeComparator());
                break;
            case BIGGEST_ROOM:
                context.addRequestAttribute(ACTION, action);
                Collections.sort(result, Collections.reverseOrder(new SmallestRoomTypeComparator()));
                break;
            case CHEAPEST_ROOM:
                context.addRequestAttribute(ACTION, action);
                Collections.sort(result, new CheapestRoomTypeComparator());
                break;
            case MOST_EXPENSIVE_ROOM:
                context.addRequestAttribute(ACTION, action);
                Collections.sort(result, Collections.reverseOrder(new CheapestRoomTypeComparator()));
                break;
            default:
                Collections.sort(result, new CheapestRoomTypeComparator());
        }
        return result;
    }
}
