package by.epam.shevelyov.hostel.command.impl.admin;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.controller.MethodsEnum;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.Discount;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.MessagesManager;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;
import by.epam.shevelyov.hostel.service.DiscountService;
import by.epam.shevelyov.hostel.validator.impl.admin.AddDiscountValidator;
import org.apache.log4j.Logger;

public class ChangeDiscountCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(ChangeDiscountCommand.class);
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String METHOD = "method";
    private static final String DISCOUNT = "discount";
    private static final String TITLE = "title";
    private static final String PERCENTAGE = "percentage";
    private static final String DESCRIPTION = "description";
    private AddDiscountValidator validator = AddDiscountValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_ALL_DISCOUNTS);
            }
            int discountId = Integer.parseInt(context.getRequestParameter(DISCOUNT)[0]);
            Discount discount = DiscountService.getDiscountById(discountId);
            changeDiscount(context, discount);
            if (DiscountService.updateDiscount(discount)) {
                context.addRequestAttribute(METHOD, MethodsEnum.REDIRECT);
            } else {
                context.addRequestAttribute(ERROR_MESSAGE, MessagesEnum.ERROR_DISCOUNT_ADD);
                return PagesManager.getPage(PagesEnum.GO_TO_ALL_DISCOUNTS);
            }
            result = PagesManager.getPage(PagesEnum.GO_TO_ALL_DISCOUNTS);
            LOGGER.info(MessagesManager.getMessage(MessagesEnum.LOG_DISCOUNT_CHANGE));
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }

    private void changeDiscount(RequestContext context, Discount discount) {
        String title = context.getRequestParameter(TITLE)[0];
        discount.setTitle(title);
        String percentage = context.getRequestParameter(PERCENTAGE)[0];
        discount.setPercentage(Integer.parseInt(percentage));
        String description = context.getRequestParameter(DESCRIPTION)[0];
        discount.setDescription(description);
    }
}
