package by.epam.shevelyov.hostel.command.impl.admin;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.controller.MethodsEnum;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.User;
import by.epam.shevelyov.hostel.entity.util.RoleEnum;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;
import by.epam.shevelyov.hostel.service.UserService;
import by.epam.shevelyov.hostel.validator.impl.UserValidator;
import org.apache.log4j.Logger;

public class GoToAddBanCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(GoToAddAdministratorCommand.class);
    private static final String METHOD = "method";
    private static final String USER = "user";
    private UserValidator validator = UserValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            MessagesEnum validateResult = validator.validate(context);
            if (validateResult.equals(MessagesEnum.SUCCESS)) {
                int userId = Integer.parseInt(context.getRequestParameter(USER)[0]);
                User user = UserService.getUserById(userId);
                if (user.getRole().equals(RoleEnum.USER)) {
                    context.addRequestAttribute(USER, user);
                }
            }
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            result = PagesManager.getPage(PagesEnum.ADMIN_ADD_BAN);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}