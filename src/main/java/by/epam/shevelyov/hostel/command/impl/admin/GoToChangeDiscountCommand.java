package by.epam.shevelyov.hostel.command.impl.admin;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.controller.MethodsEnum;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.Discount;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;
import by.epam.shevelyov.hostel.service.DiscountService;
import by.epam.shevelyov.hostel.validator.impl.DiscountValidator;
import org.apache.log4j.Logger;

public class GoToChangeDiscountCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(GoToChangeDiscountCommand.class);
    private static final String METHOD = "method";
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String DISCOUNT = "discount";
    private DiscountValidator validator = DiscountValidator.getInstance();


    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_ALL_DISCOUNTS);
            }
            int discountId = Integer.parseInt(context.getRequestParameter(DISCOUNT)[0]);
            Discount discount = DiscountService.getDiscountById(discountId);
            context.addRequestAttribute(DISCOUNT, discount);
            result = PagesManager.getPage(PagesEnum.ADMIN_CHANGE_DISCOUNT);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}