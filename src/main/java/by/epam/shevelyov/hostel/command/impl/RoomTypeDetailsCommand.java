package by.epam.shevelyov.hostel.command.impl;


import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.controller.MethodsEnum;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.RoomType;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;
import by.epam.shevelyov.hostel.service.RoomTypeService;
import by.epam.shevelyov.hostel.validator.impl.RoomTypeValidator;
import org.apache.log4j.Logger;

public class RoomTypeDetailsCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(RoomTypeDetailsCommand.class);
    private static final String METHOD = "method";
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String ROOMTYPE = "roomtype";
    private RoomTypeValidator validator = RoomTypeValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.ROOMS);
            }
            int roomTypeId = Integer.parseInt(context.getRequestParameter(ROOMTYPE)[0]);
            RoomType roomType = RoomTypeService.getRoomTypeById(roomTypeId);
            context.addRequestAttribute(ROOMTYPE, roomType);
            result = PagesManager.getPage(PagesEnum.ROOMTYPE_DETAILS);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}
