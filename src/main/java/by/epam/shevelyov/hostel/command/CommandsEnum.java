package by.epam.shevelyov.hostel.command;

import by.epam.shevelyov.hostel.command.impl.*;
import by.epam.shevelyov.hostel.command.impl.admin.*;

/**
 * <p>Enum that contains all command in project</p>
 *
 * @author Ilya Shevelyov
 */
public enum CommandsEnum {
    EMPTY(new EmptyCommand()),
    CHANGE_LANGUAGE(new ChangeLanguageCommand()),
    REGISTRATION(new RegistrationCommand()),
    MAIN_PAGE(new MainPageCommand()),
    LOGIN(new LoginCommand()),
    PROFILE(new ProfileCommand()),
    GO_TO_BOOK_PAGE(new GoToBookPageCommand()),
    BOOK(new BookCommand()),
    LOGOUT(new LogoutCommand()),
    BIGGEST_ROOM(new ViewRoomsCommand()),
    SMALLEST_ROOM(new ViewRoomsCommand()),
    CHEAPEST_ROOM(new ViewRoomsCommand()),
    MOST_EXPENSIVE_ROOM(new ViewRoomsCommand()),
    VIEW_ROOMS(new ViewRoomsCommand()),
    GALLERY_PAGE(new GalleryPageCommand()),
    SERVICE_PAGE(new ServicePageCommand()),
    ALL_ORDERS(new AllOrdersCommand()),
    ALL_USERS(new AllUsersCommand()),
    ALL_BANS(new AllBansCommand()),
    ALL_DISCOUNTS(new AllDiscountsCommand()),
    ALL_ADMINISTRATORS(new AllAdministratorsCommand()),
    ALL_ROOMS(new AllRoomsCommand()),
    ALL_ROOMTYPES(new AllRoomTypesCommand()),
    DELETE_ADMINISTRATOR(new DeleteAdministratorCommand()),
    DELETE_BAN(new DeleteBanCommand()),
    DELETE_DISCOUNT(new DeleteDiscountCommand()),
    DELETE_ROOM(new DeleteRoomCommand()),
    DELETE_ROOMTYPE(new DeleteRoomTypeCommand()),
    ADD_ADMINISTRATOR(new AddAdministratorCommand()),
    ADD_BAN(new AddBanCommand()),
    ADD_DISCOUNT(new AddDiscountCommand()),
    ADD_ROOMTYPE(new AddRoomTypeCommand()),
    ADD_ROOM(new AddRoomCommand()),
    CHANGE_ADMINISTRATOR(new ChangeAdministratorCommand()),
    CHANGE_BAN(new ChangeBanCommand()),
    CHANGE_DISCOUNT(new ChangeDiscountCommand()),
    CHANGE_ROOMTYPE(new ChangeRoomTypeCommand()),
    CHANGE_ROOM(new ChangeRoomCommand()),
    CHANGE_USER_INFO(new ChangeUserInfoCommand()),
    ADMIN_CHANGE_ORDER_STATUS(new AdminChangeOrderStatusCommand()),
    USER_CHANGE_ORDER_STATUS(new UserChangeOrderStatusCommand()),
    SET_DISCOUNT(new SetDiscountCommand()),
    SHOW_AVAILABILITY(new ShowAvailabilityCommand()),
    GO_TO_ADD_ADMINISTRATOR(new GoToAddAdministratorCommand()),
    GO_TO_ADD_BAN(new GoToAddBanCommand()),
    GO_TO_ADD_DISCOUNT(new GoToAddDiscountCommand()),
    GO_TO_ADD_ROOMTYPE(new GoToAddRoomTypeCommand()),
    GO_TO_ADD_ROOM(new GoToAddRoomCommand()),
    GO_TO_CHANGE_ADMINISTRATOR(new GoToChangeAdministratorCommand()),
    GO_TO_CHANGE_BAN(new GoToChangeBanCommand()),
    GO_TO_CHANGE_DISCOUNT(new GoToChangeDiscountCommand()),
    GO_TO_CHANGE_ROOMTYPE(new GoToChangeRoomTypeCommand()),
    GO_TO_CHANGE_ROOM(new GoToChangeRoomCommand()),
    GO_TO_CHANGE_USER_INFO(new GoToChangeUserInfoCommand()),
    GO_TO_SHOW_AVAILABILITY(new GoToChoosePeriod()),
    GO_TO_CHOOSE_PERIOD(new GoToChoosePeriod()),
    GO_TO_SET_DISCOUNT(new GoToSetDiscount()),
    USER_DETAILS(new UserDetailsCommand()),
    ROOMTYPE_DETAILS(new RoomTypeDetailsCommand());


    ActionCommand command;

    CommandsEnum(ActionCommand command) {
        this.command = command;
    }

    public ActionCommand getCommand() {
        return this.command;
    }
}
