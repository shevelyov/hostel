package by.epam.shevelyov.hostel.command.impl;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.controller.MethodsEnum;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.Order;
import by.epam.shevelyov.hostel.entity.impl.User;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;
import by.epam.shevelyov.hostel.service.OrderService;
import org.apache.log4j.Logger;

import java.util.List;

public class ProfileCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(ProfileCommand.class);
    private static final String METHOD = "method";
    private static final String USER = "user";
    private static final String ORDERS = "orders";

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            User user = (User) context.getSessionAttribute(USER);
            List<Order> orders = OrderService.getAllOrdersByUserId(user.getId());
            context.addRequestAttribute(ORDERS, orders);

            result = PagesManager.getPage(PagesEnum.PROFILE);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}
