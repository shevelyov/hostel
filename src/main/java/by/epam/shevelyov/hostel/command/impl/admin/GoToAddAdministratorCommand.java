package by.epam.shevelyov.hostel.command.impl.admin;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.controller.MethodsEnum;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.util.RoleEnum;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;
import org.apache.log4j.Logger;


public class GoToAddAdministratorCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(GoToAddAdministratorCommand.class);
    private static final String ROLES = "roles";
    private static final String METHOD = "method";

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            RoleEnum[] roles = new RoleEnum[2];
            roles[0]= RoleEnum.ADMIN;
            roles[1]= RoleEnum.SUPER;
            context.addRequestAttribute(ROLES, roles);
            result = PagesManager.getPage(PagesEnum.ADMIN_ADD_ADMINISTRATOR);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}