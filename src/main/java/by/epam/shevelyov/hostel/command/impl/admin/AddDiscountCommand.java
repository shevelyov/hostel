package by.epam.shevelyov.hostel.command.impl.admin;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.controller.MethodsEnum;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.Discount;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.MessagesManager;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;
import by.epam.shevelyov.hostel.service.DiscountService;
import by.epam.shevelyov.hostel.validator.impl.admin.AddDiscountValidator;
import org.apache.log4j.Logger;

public class AddDiscountCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(AddDiscountCommand.class);
    private static final String METHOD = "method";
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String TITLE = "title";
    private static final String PERCENTAGE = "percentage";
    private static final String DESCRIPTION = "description";
    private AddDiscountValidator validator = AddDiscountValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_ADD_DISCOUNT);
            }
            String title = context.getRequestParameter(TITLE)[0];
            int percentage = Integer.parseInt(context.getRequestParameter(PERCENTAGE)[0]);
            String description = context.getRequestParameter(DESCRIPTION)[0];
            Discount discount  = new Discount(0, title, percentage, description);
            if (!DiscountService.addDiscount(discount)) {
                context.addRequestAttribute(ERROR_MESSAGE, MessagesEnum.ERROR_DISCOUNT_ADD.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_ADD_DISCOUNT);
            } else {
                context.addRequestAttribute(METHOD, MethodsEnum.REDIRECT);
            }
            result = PagesManager.getPage(PagesEnum.GO_TO_ALL_DISCOUNTS);
            LOGGER.info(MessagesManager.getMessage(MessagesEnum.LOG_DISCOUNT_ADD));
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}