package by.epam.shevelyov.hostel.command.factory;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.command.CommandsEnum;
import by.epam.shevelyov.hostel.command.EmptyCommand;
import by.epam.shevelyov.hostel.command.exception.CommandException;
import by.epam.shevelyov.hostel.manager.ErrorsEnum;
import by.epam.shevelyov.hostel.manager.ErrorsManager;
import org.apache.log4j.Logger;

/**
 * <p>Factory that defines commands by parameter</p>
 *
 * @author Ilya Shevelyov
 */
public class ActionFactory {
    private static final Logger LOGGER = Logger.getLogger(ActionFactory.class);

    private static class InstanceHolder {
        private static final ActionFactory INSTANCE = new ActionFactory();
    }

    private ActionFactory() {}

    public static ActionFactory getInstance() {
        return InstanceHolder.INSTANCE;
    }

    /**
     * <p>Method for defining command by parameter</p>
     *
     * @param param Parameter for defining command
     * @return {@link ActionCommand} that will be executed
     */
    public static ActionCommand defineCommand(String param) {
        ActionCommand result = new EmptyCommand();
        try {
            if (param == null) {
                throw new CommandException(ErrorsManager.getMessage(ErrorsEnum.CONTEXT_EMPTY_ACTION));
            }
            result = CommandsEnum.valueOf(param.toUpperCase()).getCommand();
        } catch (CommandException | IllegalArgumentException e) {
            LOGGER.error(e);
        }
        return result;
    }
}
