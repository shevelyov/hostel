package by.epam.shevelyov.hostel.command.impl.admin;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.controller.MethodsEnum;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.encoder.Encoder;
import by.epam.shevelyov.hostel.entity.impl.User;
import by.epam.shevelyov.hostel.entity.util.RoleEnum;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.MessagesManager;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;
import by.epam.shevelyov.hostel.service.UserService;
import by.epam.shevelyov.hostel.validator.impl.admin.ChangeAdministratorValidator;
import org.apache.log4j.Logger;

public class ChangeAdministratorCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(ChangeAdministratorCommand.class);
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String METHOD = "method";
    private static final String USER = "user";
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String EMAIL = "email";
    private static final String FIRST_NAME = "firstname";
    private static final String LAST_NAME = "lastname";
    private static final String PHONE = "phone";
    private static final String ROLE = "role";
    private ChangeAdministratorValidator validator = ChangeAdministratorValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_ALL_ADMINISTRATORS);
            }

            int userId = Integer.parseInt(context.getRequestParameter(USER)[0]);
            User user = UserService.getUserById(userId);
            changeAdministrator(context, user);
            if (UserService.updateUser(user)) {
                context.addRequestAttribute(METHOD, MethodsEnum.REDIRECT);
            } else {
                context.addRequestAttribute(ERROR_MESSAGE, MessagesEnum.ERROR_ADMINISTRATOR_ADD);
                return PagesManager.getPage(PagesEnum.GO_TO_ALL_ADMINISTRATORS);
            }
            result = PagesManager.getPage(PagesEnum.GO_TO_ALL_ADMINISTRATORS);
            LOGGER.info(MessagesManager.getMessage(MessagesEnum.LOG_ADMINISTRATOR_CHANGE));
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }

    private void changeAdministrator(RequestContext context, User user) {
        String login = context.getRequestParameter(LOGIN)[0];
        user.setLogin(login);
        String password = context.getRequestParameter(PASSWORD)[0];
        String encodedPassword = Encoder.encode(password);
        user.setPassword(encodedPassword);
        String email = context.getRequestParameter(EMAIL)[0];
        user.setEmail(email);
        String phone = context.getRequestParameter(PHONE)[0];
        user.setPhoneNumber(phone);
        String firstname = context.getRequestParameter(FIRST_NAME)[0];
        user.setFirstName(firstname);
        String lastname = context.getRequestParameter(LAST_NAME)[0];
        user.setLastName(lastname);
        String role = context.getRequestParameter(ROLE)[0];
        user.setRole(RoleEnum.valueOf(role.toUpperCase()));
    }
}
