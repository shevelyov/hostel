package by.epam.shevelyov.hostel.command.impl.admin;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.controller.MethodsEnum;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.RoomType;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;
import by.epam.shevelyov.hostel.service.RoomTypeService;
import org.apache.log4j.Logger;

import java.util.List;

public class GoToAddRoomCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(GoToAddRoomCommand.class);
    private static final String METHOD = "method";
    private static final String ROOMTYPES = "roomtypes";

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            List<RoomType> roomTypes = RoomTypeService.getAllRoomTypes();
            context.addRequestAttribute(ROOMTYPES,roomTypes);
            result = PagesManager.getPage(PagesEnum.ADMIN_ADD_ROOM);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}