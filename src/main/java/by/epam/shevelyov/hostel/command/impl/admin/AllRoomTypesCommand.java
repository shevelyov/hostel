package by.epam.shevelyov.hostel.command.impl.admin;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.controller.MethodsEnum;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.RoomType;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;
import by.epam.shevelyov.hostel.service.RoomTypeService;
import by.epam.shevelyov.hostel.validator.impl.admin.AdminCatalogValidator;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class AllRoomTypesCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(AllRoomTypesCommand.class);
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String METHOD = "method";
    private static final String ROOMTYPES = "roomtypes";
    private static final String PAGE = "page";
    private static final int ROOMTYPES_PER_PAGE = 10;
    private AdminCatalogValidator validator = AdminCatalogValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
            }
            int page = Integer.parseInt(context.getRequestParameter(PAGE)[0]);
            List<RoomType> allRoomTypes = RoomTypeService.getAllRoomTypes();
            List<RoomType> roomTypes = createRoomTypes(allRoomTypes, page, ROOMTYPES_PER_PAGE);
            context.addRequestAttribute(PAGE, page);
            context.addRequestAttribute(ROOMTYPES, roomTypes);
            result = PagesManager.getPage(PagesEnum.ADMIN_ALL_ROOMTYPES);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }

    private List<RoomType> createRoomTypes(List<RoomType> allRoomTypes, int page, int count) {
        List<RoomType> result = new ArrayList<>();
        int start = (page - 1) * count;
        int end = start + count - 1;
        for (int i = start; i <= end; i++) {
            if (i > allRoomTypes.size() - 1) {
                break;
            }
            result.add(allRoomTypes.get(i));
        }
        return result;
    }

}
