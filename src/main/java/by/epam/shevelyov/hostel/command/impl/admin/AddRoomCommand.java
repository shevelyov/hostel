package by.epam.shevelyov.hostel.command.impl.admin;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.controller.MethodsEnum;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.Room;
import by.epam.shevelyov.hostel.entity.impl.RoomType;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.MessagesManager;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;
import by.epam.shevelyov.hostel.service.RoomService;
import by.epam.shevelyov.hostel.service.RoomTypeService;
import by.epam.shevelyov.hostel.validator.impl.admin.AddRoomValidator;
import org.apache.log4j.Logger;

public class AddRoomCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(AddRoomCommand.class);
    private static final String METHOD = "method";
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String ROOM_NUMBER = "roomnumber";
    private static final String ROOMTYPE = "roomtype";
    private AddRoomValidator validator = AddRoomValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_ADD_ROOM);
            }
            String roomNumber = context.getRequestParameter(ROOM_NUMBER)[0];
            int roomTypeId = Integer.parseInt(context.getRequestParameter(ROOMTYPE)[0]);
            RoomType roomType = RoomTypeService.getRoomTypeById(roomTypeId);
            Room room  = new Room(0, roomType, roomNumber);
            if (!RoomService.addRoom(room)) {
                context.addRequestAttribute(ERROR_MESSAGE, MessagesEnum.ERROR_ROOM_ADD.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_ADD_ROOM);
            } else {
                context.addRequestAttribute(METHOD, MethodsEnum.REDIRECT);
            }
            result = PagesManager.getPage(PagesEnum.GO_TO_ALL_ROOMS);
            LOGGER.info(MessagesManager.getMessage(MessagesEnum.LOG_ROOM_ADD));
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}