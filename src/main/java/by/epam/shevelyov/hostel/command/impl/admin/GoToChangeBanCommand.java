package by.epam.shevelyov.hostel.command.impl.admin;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.controller.MethodsEnum;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.Ban;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;
import by.epam.shevelyov.hostel.service.BanService;
import by.epam.shevelyov.hostel.validator.impl.BanValidator;
import org.apache.log4j.Logger;

public class GoToChangeBanCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(GoToChangeBanCommand.class);
    private static final String METHOD = "method";
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String BAN = "ban";
    private BanValidator validator = BanValidator.getInstance();


    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_ALL_BANS);
            }
            int banId = Integer.parseInt(context.getRequestParameter(BAN)[0]);
            Ban ban = BanService.getBanById(banId);
            context.addRequestAttribute(BAN, ban);
            result = PagesManager.getPage(PagesEnum.ADMIN_CHANGE_BAN);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}
