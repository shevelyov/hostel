package by.epam.shevelyov.hostel.command.impl.admin;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.controller.MethodsEnum;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.Ban;
import by.epam.shevelyov.hostel.entity.impl.User;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.MessagesManager;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;
import by.epam.shevelyov.hostel.service.BanService;
import by.epam.shevelyov.hostel.service.UserService;
import by.epam.shevelyov.hostel.validator.impl.admin.AddBanValidator;
import org.apache.log4j.Logger;

import java.sql.Date;

public class AddBanCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(AddBanCommand.class);
    private static final String METHOD = "method";
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String LOGIN = "login";
    private static final String USER = "user";
    private static final String START_DATE = "startDate";
    private static final String END_DATE = "endDate";
    private static final String REASON = "reason";
    private AddBanValidator validator = AddBanValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_ADD_BAN);
            }
            User user = UserService.getUserByLogin(context.getRequestParameter(LOGIN)[0]);
            User admin = (User) context.getSessionAttribute(USER);
            String startDate = context.getRequestParameter(START_DATE)[0];
            String endDate = context.getRequestParameter(END_DATE)[0];
            String reason = context.getRequestParameter(REASON)[0];
            Ban ban = new Ban(0, user, admin, Date.valueOf(startDate), Date.valueOf(endDate), reason);
            if (!BanService.addBan(ban)) {
                context.addRequestAttribute(ERROR_MESSAGE, MessagesEnum.ERROR_BAN_ADD.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_ADD_BAN);
            } else {
                context.addRequestAttribute(METHOD, MethodsEnum.REDIRECT);
            }
            result = PagesManager.getPage(PagesEnum.GO_TO_ALL_BANS);
            LOGGER.info(MessagesManager.getMessage(MessagesEnum.LOG_BAN_ADD));
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}