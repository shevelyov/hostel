package by.epam.shevelyov.hostel.command;

import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;

/**
 * <p>Class that will be executed if a servlet has been accessed without a command</p>
 *
 * @author Ilya Shevelyov
 */
public class EmptyCommand implements ActionCommand {

    @Override
    public String execute(RequestContext context) {
        return PagesManager.getPage(PagesEnum.ERROR_404);
    }

}
