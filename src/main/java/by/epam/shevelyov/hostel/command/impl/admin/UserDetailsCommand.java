package by.epam.shevelyov.hostel.command.impl.admin;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.controller.MethodsEnum;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.Ban;
import by.epam.shevelyov.hostel.entity.impl.Order;
import by.epam.shevelyov.hostel.entity.impl.User;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;
import by.epam.shevelyov.hostel.service.BanService;
import by.epam.shevelyov.hostel.service.OrderService;
import by.epam.shevelyov.hostel.service.UserService;
import by.epam.shevelyov.hostel.validator.impl.UserValidator;
import org.apache.log4j.Logger;

import java.util.List;

public class UserDetailsCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(UserDetailsCommand.class);
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String METHOD = "method";
    private static final String USER = "user";
    private static final String ORDERS = "orders";
    private static final String BANS = "bans";
    private UserValidator validator = UserValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_ALL_USERS);
            }
            int userId = Integer.parseInt(context.getRequestParameter(USER)[0]);
            User user = UserService.getUserById(userId);
            context.addRequestAttribute(USER, user);
            List<Order> orders = OrderService.getAllOrdersByUserId(user.getId());
            context.addRequestAttribute(ORDERS, orders);
            List<Ban> bans = BanService.getAllBansByUserId(user.getId());
            context.addRequestAttribute(BANS, bans);

            result = PagesManager.getPage(PagesEnum.ADMIN_USER_DETAILS);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}
