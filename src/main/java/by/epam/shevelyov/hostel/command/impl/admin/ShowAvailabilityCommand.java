package by.epam.shevelyov.hostel.command.impl.admin;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.controller.MethodsEnum;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.Room;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;
import by.epam.shevelyov.hostel.service.OrderService;
import by.epam.shevelyov.hostel.service.RoomService;
import by.epam.shevelyov.hostel.validator.impl.admin.ShowAvailabilityValidator;
import org.apache.log4j.Logger;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShowAvailabilityCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(ShowAvailabilityCommand.class);
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String METHOD = "method";
    private static final String START = "start";
    private static final String END = "end";
    private static final String DATES = "dates";
    private static final String MAP = "map";
    private ShowAvailabilityValidator validator = ShowAvailabilityValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.ADMIN_CHOOSE_PERIOD);
            }
            List<Room> rooms = RoomService.getAllRooms();
            Map<LocalDate,Integer> map = new HashMap<>();
            Map<String,List<Integer>> freePlaces = new HashMap<>();
            String startDate = context.getRequestParameter(START)[0];
            String endDate = context.getRequestParameter(END)[0];
            for (Room room : rooms) {
                map = OrderService.getFreePlacesByDatesAndRoomId(room.getId(),startDate,endDate);
                freePlaces.put(room.getRoomType().getDescription(),new ArrayList<>(map.values()));
            }
            List<LocalDate> allDates = new ArrayList<>(map.keySet());
            List<String> dates = new ArrayList<>();
            for (LocalDate localDate : allDates) {
                dates.add(localDate.toString());
            }

            context.addRequestAttribute(DATES, dates);
            context.addRequestAttribute(MAP, freePlaces);
            result = PagesManager.getPage(PagesEnum.ADMIN_SHOW_AVAILABILITY);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}
