package by.epam.shevelyov.hostel.command.impl.admin;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.controller.MethodsEnum;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.Discount;
import by.epam.shevelyov.hostel.entity.impl.Order;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.MessagesManager;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;
import by.epam.shevelyov.hostel.service.DiscountService;
import by.epam.shevelyov.hostel.service.OrderService;
import by.epam.shevelyov.hostel.validator.impl.DiscountValidator;
import org.apache.log4j.Logger;

public class SetDiscountCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(SetDiscountCommand.class);
    private static final String METHOD = "method";
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String ORDER = "order";
    private static final String DISCOUNT = "discount";
    private DiscountValidator validator = DiscountValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_ALL_ORDERS);
            }
            int discountId = Integer.parseInt(context.getRequestParameter(DISCOUNT)[0]);
            Discount discount = DiscountService.getDiscountById(discountId);
            int orderId = Integer.parseInt(context.getRequestParameter(ORDER)[0]);
            Order order = OrderService.getOrderById(orderId);
            order.setDiscount(discount);
            order.setTotalAmount(order.getTotalAmount() - (order.getTotalAmount() * discount.getPercentage() / 100));
            if (!OrderService.updateOrder(order)) {
                context.addRequestAttribute(ERROR_MESSAGE, MessagesEnum.ERROR_SET_DISCOUNT.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_ALL_ORDERS);
            } else {
                context.addRequestAttribute(METHOD, MethodsEnum.REDIRECT);
            }
            result = PagesManager.getPage(PagesEnum.GO_TO_ALL_ORDERS);
            LOGGER.info(MessagesManager.getMessage(MessagesEnum.LOG_SET_DISCOUNT));
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}