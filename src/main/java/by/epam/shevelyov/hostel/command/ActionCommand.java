package by.epam.shevelyov.hostel.command;

import by.epam.shevelyov.hostel.controller.RequestContext;

/**
 * <p>Interface that describes execute operation in all commands</p>
 *
 * @author Ilya Shevelyov
 */
public interface ActionCommand {

    /**
     * <p>Method for doing user's request</p>
     *
     * @param context Context with information from the request
     * @return Result page
     */
    String execute(RequestContext context);
}
