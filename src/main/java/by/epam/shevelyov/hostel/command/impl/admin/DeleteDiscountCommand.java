package by.epam.shevelyov.hostel.command.impl.admin;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.controller.MethodsEnum;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.MessagesManager;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;
import by.epam.shevelyov.hostel.service.DiscountService;
import by.epam.shevelyov.hostel.validator.impl.BanValidator;
import by.epam.shevelyov.hostel.validator.impl.DiscountValidator;
import org.apache.log4j.Logger;

public class DeleteDiscountCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(DeleteDiscountCommand.class);
    private static final String METHOD = "method";
    private static final String DISCOUNT = "discount";
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String SUCCESS_MESSAGE = "successMessage";
    private DiscountValidator validator = DiscountValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_ALL_DISCOUNTS);
            }
            int discountId = Integer.parseInt(context.getRequestParameter(DISCOUNT)[0]);
            if (DiscountService.deleteDiscount(discountId)) {
                context.addRequestAttribute(SUCCESS_MESSAGE, MessagesEnum.SUCCESS_DISCOUNT_DELETE.getValue());
            } else {
                context.addRequestAttribute(ERROR_MESSAGE, MessagesEnum.ERROR_DISCOUNT_DELETE.getValue());
            }
            result = PagesManager.getPage(PagesEnum.GO_TO_ALL_DISCOUNTS);
            LOGGER.info(MessagesManager.getMessage(MessagesEnum.LOG_DISCOUNT_DELETE));
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}
