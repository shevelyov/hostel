package by.epam.shevelyov.hostel.command.impl.admin;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.controller.MethodsEnum;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.encoder.Encoder;
import by.epam.shevelyov.hostel.entity.impl.User;
import by.epam.shevelyov.hostel.entity.util.RoleEnum;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.MessagesManager;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;
import by.epam.shevelyov.hostel.service.UserService;
import by.epam.shevelyov.hostel.validator.impl.admin.AddAdministratorValidator;
import org.apache.log4j.Logger;

public class AddAdministratorCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(AddAdministratorCommand.class);
    private static final String METHOD = "method";
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String EMAIL = "email";
    private static final String FIRST_NAME = "firstname";
    private static final String LAST_NAME = "lastname";
    private static final String PHONE = "phone";
    private static final String ROLE = "role";
    private AddAdministratorValidator validator = AddAdministratorValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_ADD_ADMINISTRATOR);
            }
            String login = context.getRequestParameter(LOGIN)[0];
            String password = context.getRequestParameter(PASSWORD)[0];
            String email = context.getRequestParameter(EMAIL)[0];
            String phone = context.getRequestParameter(PHONE)[0];
            String firstname = context.getRequestParameter(FIRST_NAME)[0];
            String lastname = context.getRequestParameter(LAST_NAME)[0];
            String role = context.getRequestParameter(ROLE)[0];
            String encodedPassword = Encoder.encode(password);
            User user = new User(0, login, encodedPassword, firstname, lastname, email, phone, RoleEnum.valueOf(role.toUpperCase()));
            if (!UserService.registerUser(user)) {
                context.addRequestAttribute(ERROR_MESSAGE, MessagesEnum.ERROR_ADMINISTRATOR_ADD.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_ADD_ADMINISTRATOR);
            } else {
                context.addRequestAttribute(METHOD, MethodsEnum.REDIRECT);
            }
            result = PagesManager.getPage(PagesEnum.GO_TO_ALL_ADMINISTRATORS);
            LOGGER.info(MessagesManager.getMessage(MessagesEnum.LOG_ADMINISTRATOR_ADD));
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}