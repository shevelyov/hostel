package by.epam.shevelyov.hostel.command.impl;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.controller.MethodsEnum;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.*;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.MessagesManager;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;
import by.epam.shevelyov.hostel.service.*;
import by.epam.shevelyov.hostel.validator.impl.BookValidator;
import org.apache.log4j.Logger;

import java.sql.Date;
import java.util.Calendar;

public class BookCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(BookCommand.class);
    private static final String METHOD = "method";
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String SUCCESS_MESSAGE = "successMessage";
    private static final String ROOMTYPE = "roomtype";
    private static final String ORDERTYPE = "ordertype";
    private static final String GUESTS = "guests";
    private static final String CHECKIN = "checkin";
    private static final String CHECKOUT = "checkout";
    private static final String USER = "user";
    private BookValidator validator = BookValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;

        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_MAIN);
            }
            String[] roomParam = context.getRequestParameter(ROOMTYPE);
            String[] orderTypeParam = context.getRequestParameter(ORDERTYPE);
            String[] guestsParam = context.getRequestParameter(GUESTS);
            String[] checkInParam = context.getRequestParameter(CHECKIN);
            String[] checkOutParam = context.getRequestParameter(CHECKOUT);
            User user = (User) context.getSessionAttribute(USER);
            int guest = Integer.parseInt(guestsParam[0]);
            Date registrationDate = new java.sql.Date(Calendar.getInstance().getTime().getTime());
            Date checkIn = Date.valueOf(checkInParam[0]);
            Date checkOut = Date.valueOf(checkOutParam[0]);
            RoomType roomType = RoomTypeService.getRoomTypeById(Integer.parseInt(roomParam[0]));
            Room room = RoomService.getAllRoomsByRoomTypeId(roomType.getId()).get(0);
            OrderType orderType = OrderTypeService.getOrderTypeById(Integer.parseInt(orderTypeParam[0]));
            Discount discount = DiscountService.getDiscountById(1);
            Order order = new Order(0, user, guest, registrationDate, checkIn, checkOut, room, OrderStatusService.getOrderStatusById(1),
                    orderType, discount, getTotalAmount(roomType, guest, checkIn, checkOut, discount));
            if (!OrderService.addOrder(order)) {
                context.addRequestAttribute(ERROR_MESSAGE,
                        MessagesEnum.ERROR_ORDER_BOOKING.getValue());
            } else {
                context.addRequestAttribute(SUCCESS_MESSAGE, MessagesEnum.SUCCESS_ORDER_CREATE.getValue());
            }
            result = PagesManager.getPage(PagesEnum.GO_TO_MAIN);
            LOGGER.info(MessagesManager.getMessage(MessagesEnum.LOG_ORDER_CREATE));
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }

    private double getTotalAmount(RoomType roomType, int guest, Date checkIn, Date checkOut, Discount discount) {
        double amountPerDay = roomType.getAmount();
        long dateDiff = (checkOut.getTime() - checkIn.getTime()) / (24 * 60 * 60 * 1000);
        return amountPerDay * guest * dateDiff * (1 - discount.getPercentage() / 100);
    }
}
