package by.epam.shevelyov.hostel.command.impl.admin;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.controller.MethodsEnum;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.User;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;
import by.epam.shevelyov.hostel.service.UserService;
import by.epam.shevelyov.hostel.validator.impl.admin.AdminCatalogValidator;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class AllUsersCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(AllUsersCommand.class);
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String METHOD = "method";
    private static final String USERS = "users";
    private static final String USER = "User";
    private static final String PAGE = "page";
    private static final int ORDERS_PER_PAGE = 10;
    private AdminCatalogValidator validator = AdminCatalogValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
            }
            int page = Integer.parseInt(context.getRequestParameter(PAGE)[0]);
            List<User> allUsers = UserService.getAllUsersByRole(USER);
            List<User> users = createUsers(allUsers, page, ORDERS_PER_PAGE);
            context.addRequestAttribute(PAGE, page);
            context.addRequestAttribute(USERS, users);
            result = PagesManager.getPage(PagesEnum.ADMIN_ALL_USERS);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }

    private List<User> createUsers(List<User> allUsers, int page, int count) {
        List<User> result = new ArrayList<>();
        int start = (page - 1) * count;
        int end = start + count - 1;
        for (int i = start; i <= end; i++) {
            if (i > allUsers.size() - 1) {
                break;
            }
            result.add(allUsers.get(i));
        }
        return result;
    }
}
