package by.epam.shevelyov.hostel.command.exception;

/**
 * <p>Exception throws when error occurred while command is executing</p>
 *
 * @author Ilya Shevelyov
 */
public class CommandException extends Exception {

    public CommandException() {
    }

    public CommandException(String message) {
        super(message);
    }

    public CommandException(String message, Throwable cause) {
        super(message, cause);
    }

    public CommandException(Throwable cause) {
        super(cause);
    }
}
