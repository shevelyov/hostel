package by.epam.shevelyov.hostel.command.impl.admin;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.controller.MethodsEnum;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.Ban;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.MessagesManager;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;
import by.epam.shevelyov.hostel.service.BanService;
import by.epam.shevelyov.hostel.validator.impl.admin.ChangeBanValidator;
import org.apache.log4j.Logger;

import java.sql.Date;

public class ChangeBanCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(ChangeBanCommand.class);
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String METHOD = "method";
    private static final String BAN = "ban";
    private static final String END_DATE = "endDate";
    private static final String REASON = "reason";
    private ChangeBanValidator validator = ChangeBanValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_ALL_BANS);
            }
            int banId = Integer.parseInt(context.getRequestParameter(BAN)[0]);
            Ban ban = BanService.getBanById(banId);
            changeBan(context, ban);
            if (BanService.updateBan(ban)) {
                context.addRequestAttribute(METHOD, MethodsEnum.REDIRECT);
            } else {
                context.addRequestAttribute(ERROR_MESSAGE, MessagesEnum.ERROR_BAN_ADD);
                return PagesManager.getPage(PagesEnum.GO_TO_ALL_BANS);
            }
            result = PagesManager.getPage(PagesEnum.GO_TO_ALL_BANS);
            LOGGER.info(MessagesManager.getMessage(MessagesEnum.LOG_BAN_CHANGE));
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }

    private void changeBan(RequestContext context, Ban ban) {
        String endDate = context.getRequestParameter(END_DATE)[0];
        ban.setEndDate(Date.valueOf(endDate));
        String reason = context.getRequestParameter(REASON)[0];
        ban.setReason(reason);
    }
}
