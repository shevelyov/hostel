package by.epam.shevelyov.hostel.command.impl.admin;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.controller.MethodsEnum;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.Room;
import by.epam.shevelyov.hostel.entity.impl.RoomType;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;
import by.epam.shevelyov.hostel.service.RoomService;
import by.epam.shevelyov.hostel.service.RoomTypeService;
import by.epam.shevelyov.hostel.validator.impl.RoomValidator;
import org.apache.log4j.Logger;

import java.util.List;

public class GoToChangeRoomCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(GoToChangeRoomCommand.class);
    private static final String METHOD = "method";
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String ROOM = "room";
    private static final String ROOMTYPES = "roomtypes";
    private RoomValidator validator = RoomValidator.getInstance();


    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_ALL_ROOMS);
            }
            int roomId = Integer.parseInt(context.getRequestParameter(ROOM)[0]);
            Room room  = RoomService.getRoomById(roomId);
            context.addRequestAttribute(ROOM, room);
            List<RoomType> roomTypes = RoomTypeService.getAllRoomTypes();
            context.addRequestAttribute(ROOMTYPES,roomTypes);
            result = PagesManager.getPage(PagesEnum.ADMIN_CHANGE_ROOM);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}