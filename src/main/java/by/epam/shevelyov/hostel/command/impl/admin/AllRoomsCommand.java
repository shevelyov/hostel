package by.epam.shevelyov.hostel.command.impl.admin;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.controller.MethodsEnum;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.Room;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;
import by.epam.shevelyov.hostel.service.RoomService;
import by.epam.shevelyov.hostel.validator.impl.admin.AdminCatalogValidator;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class AllRoomsCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(AllRoomsCommand.class);
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String METHOD = "method";
    private static final String ROOMS = "rooms";
    private static final String PAGE = "page";
    private static final int ROOMS_PER_PAGE = 10;
    private AdminCatalogValidator validator = AdminCatalogValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
            }
            int page = Integer.parseInt(context.getRequestParameter(PAGE)[0]);
            List<Room> allRooms = RoomService.getAllRooms();
            List<Room> rooms = createRooms(allRooms, page, ROOMS_PER_PAGE);
            context.addRequestAttribute(PAGE, page);
            context.addRequestAttribute(ROOMS, rooms);
            result = PagesManager.getPage(PagesEnum.ADMIN_ALL_ROOMS);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }

    private List<Room> createRooms(List<Room> allRooms, int page, int count) {
        List<Room> result = new ArrayList<>();
        int start = (page - 1) * count;
        int end = start + count - 1;
        for (int i = start; i <= end; i++) {
            if (i > allRooms.size() - 1) {
                break;
            }
            result.add(allRooms.get(i));
        }
        return result;
    }
}
