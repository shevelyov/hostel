package by.epam.shevelyov.hostel.command.impl.admin;


import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.controller.MethodsEnum;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.MessagesManager;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;
import by.epam.shevelyov.hostel.service.RoomService;
import by.epam.shevelyov.hostel.validator.impl.DiscountValidator;
import by.epam.shevelyov.hostel.validator.impl.RoomValidator;
import org.apache.log4j.Logger;

public class DeleteRoomCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(DeleteRoomCommand.class);
    private static final String METHOD = "method";
    private static final String ROOM = "room";
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String SUCCESS_MESSAGE = "successMessage";
    private RoomValidator validator = RoomValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_ALL_ROOMS);
            }
            int roomId = Integer.parseInt(context.getRequestParameter(ROOM)[0]);
            if (RoomService.deleteRoom(roomId)) {
                context.addRequestAttribute(SUCCESS_MESSAGE, MessagesEnum.SUCCESS_ROOM_DELETE.getValue());
            } else {
                context.addRequestAttribute(ERROR_MESSAGE, MessagesEnum.ERROR_ROOM_DELETE.getValue());
            }
            result = PagesManager.getPage(PagesEnum.GO_TO_ALL_ROOMS);
            LOGGER.info(MessagesManager.getMessage(MessagesEnum.LOG_ROOM_DELETE));
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}
