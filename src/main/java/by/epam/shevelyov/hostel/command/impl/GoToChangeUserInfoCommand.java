package by.epam.shevelyov.hostel.command.impl;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.command.impl.admin.GoToChangeAdministratorCommand;
import by.epam.shevelyov.hostel.controller.MethodsEnum;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.encoder.Encoder;
import by.epam.shevelyov.hostel.entity.impl.User;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;
import by.epam.shevelyov.hostel.service.UserService;
import by.epam.shevelyov.hostel.validator.impl.UserValidator;
import org.apache.log4j.Logger;

public class GoToChangeUserInfoCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(GoToChangeAdministratorCommand.class);
    private static final String METHOD = "method";
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String PASSWORD = "password";
    private static final String USER = "user";
    private UserValidator validator = UserValidator.getInstance();


    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_PROFILE);
            }
            int userId = Integer.parseInt(context.getRequestParameter(USER)[0]);
            User user = UserService.getUserById(userId);
            String password = Encoder.decode(user.getPassword());
            context.addRequestAttribute(USER, user);
            context.addRequestAttribute(PASSWORD, password);
            result = PagesManager.getPage(PagesEnum.USER_CHANGE_INFORMATION);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}
