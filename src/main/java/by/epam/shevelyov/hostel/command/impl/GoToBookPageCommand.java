package by.epam.shevelyov.hostel.command.impl;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.controller.MethodsEnum;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.OrderType;
import by.epam.shevelyov.hostel.entity.impl.RoomType;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;
import by.epam.shevelyov.hostel.service.OrderTypeService;
import by.epam.shevelyov.hostel.service.RoomTypeService;
import org.apache.log4j.Logger;

import java.util.List;


public class GoToBookPageCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(GoToBookPageCommand.class);
    private static final String METHOD = "method";
    private static final String ROOMTYPES = "roomtypes";
    private static final String ORDERTYPES = "ordertypes";

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            List<RoomType> roomTypes = RoomTypeService.getAllRoomTypes();
            context.addRequestAttribute(ROOMTYPES, roomTypes);
            List<OrderType> orderTypes = OrderTypeService.getAllOrderTypes();
            context.addRequestAttribute(ORDERTYPES, orderTypes);
            result = PagesManager.getPage(PagesEnum.BOOK);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}
