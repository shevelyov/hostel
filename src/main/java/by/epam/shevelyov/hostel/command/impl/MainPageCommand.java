package by.epam.shevelyov.hostel.command.impl;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.controller.MethodsEnum;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.Order;
import by.epam.shevelyov.hostel.entity.impl.RoomType;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;
import by.epam.shevelyov.hostel.service.RoomTypeService;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Random;

public class MainPageCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(MainPageCommand.class);
    private static final String METHOD = "method";
    private static final String ROOMTYPE_1 = "roomtype1";
    private static final String ROOMTYPE_2 = "roomtype2";
    private static final String ROOMTYPE_3 = "roomtype3";

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            List<RoomType> roomTypes = RoomTypeService.getAllRoomTypes();
            context.addRequestAttribute(ROOMTYPE_1, getRandomRoomType(roomTypes));
            context.addRequestAttribute(ROOMTYPE_2, getRandomRoomType(roomTypes));
            context.addRequestAttribute(ROOMTYPE_3, getRandomRoomType(roomTypes));
            result = PagesManager.getPage(PagesEnum.MAIN);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }

    private RoomType getRandomRoomType (List<RoomType> roomTypes){
        Random random = new Random();
        RoomType roomType = RoomTypeService.getRoomTypeById(random.nextInt(roomTypes.size())+1);
        return roomType;
    }

}

