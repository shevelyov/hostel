package by.epam.shevelyov.hostel.command.impl.admin;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.controller.MethodsEnum;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.MessagesManager;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;
import by.epam.shevelyov.hostel.service.UserService;
import by.epam.shevelyov.hostel.validator.impl.UserValidator;
import org.apache.log4j.Logger;

public class DeleteAdministratorCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(DeleteAdministratorCommand.class);
    private static final String METHOD = "method";
    private static final String USER = "user";
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String SUCCESS_MESSAGE = "successMessage";
    private UserValidator validator = UserValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_ALL_ADMINISTRATORS);
            }
            int userId = Integer.parseInt(context.getRequestParameter(USER)[0]);
            if (UserService.delete(userId)) {
                context.addRequestAttribute(SUCCESS_MESSAGE, MessagesEnum.SUCCESS_ADMINISTRATOR_DELETE.getValue());
            } else {
                context.addRequestAttribute(ERROR_MESSAGE, MessagesEnum.ERROR_ADMINISTRATOR_DELETE.getValue());
            }
            result = PagesManager.getPage(PagesEnum.GO_TO_ALL_ADMINISTRATORS);
            LOGGER.info(MessagesManager.getMessage(MessagesEnum.LOG_ADMINISTRATOR_DELETE));
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}
