package by.epam.shevelyov.hostel.command.impl.admin;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.controller.MethodsEnum;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.MessagesManager;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;
import by.epam.shevelyov.hostel.service.RoomTypeService;
import by.epam.shevelyov.hostel.validator.impl.RoomTypeValidator;
import org.apache.log4j.Logger;

public class DeleteRoomTypeCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(DeleteRoomTypeCommand.class);
    private static final String METHOD = "method";
    private static final String ROOMTYPE = "roomtype";
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String SUCCESS_MESSAGE = "successMessage";
    private RoomTypeValidator validator = RoomTypeValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_ALL_ROOMTYPES);
            }
            int roomTypeId = Integer.parseInt(context.getRequestParameter(ROOMTYPE)[0]);
            if (RoomTypeService.deleteRoomType(roomTypeId)) {
                context.addRequestAttribute(SUCCESS_MESSAGE, MessagesEnum.SUCCESS_ROOMTYPE_DELETE.getValue());
            } else {
                context.addRequestAttribute(ERROR_MESSAGE, MessagesEnum.ERROR_ROOMTYPE_DELETE.getValue());
            }
            result = PagesManager.getPage(PagesEnum.GO_TO_ALL_ROOMTYPES);
            LOGGER.info(MessagesManager.getMessage(MessagesEnum.LOG_ROOMTYPE_DELETE));
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}
