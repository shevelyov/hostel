package by.epam.shevelyov.hostel.command.impl;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.controller.MethodsEnum;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.MessagesManager;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;
import org.apache.log4j.Logger;

public class LogoutCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(LogoutCommand.class);
    private static final String METHOD = "method";

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.REDIRECT);
            context.invalidateSession();
            result = PagesManager.getPage(PagesEnum.INDEX);
            LOGGER.info(MessagesManager.getMessage(MessagesEnum.LOG_LOGOUT));
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}

