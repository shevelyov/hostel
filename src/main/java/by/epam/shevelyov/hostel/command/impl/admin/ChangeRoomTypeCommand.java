package by.epam.shevelyov.hostel.command.impl.admin;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.controller.MethodsEnum;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.RoomType;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.MessagesManager;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;
import by.epam.shevelyov.hostel.service.RoomTypeService;
import by.epam.shevelyov.hostel.validator.impl.admin.AddRoomTypeValidator;
import org.apache.log4j.Logger;

public class ChangeRoomTypeCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(ChangeRoomTypeCommand.class);
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String METHOD = "method";
    private static final String ROOMTYPE = "roomtype";
    private static final String DESCRIPTION = "description";
    private static final String CAPACITY = "capacity";
    private static final String AMOUNT = "amount";
    private AddRoomTypeValidator validator = AddRoomTypeValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_ALL_ROOMTYPES);
            }
            int roomTypeId = Integer.parseInt(context.getRequestParameter(ROOMTYPE)[0]);
            RoomType roomType  = RoomTypeService.getRoomTypeById(roomTypeId);
            changeRoomType(context, roomType);
            if (RoomTypeService.updateRoomType(roomType)) {
                context.addRequestAttribute(METHOD, MethodsEnum.REDIRECT);
            } else {
                context.addRequestAttribute(ERROR_MESSAGE, MessagesEnum.ERROR_ROOMTYPE_ADD);
                return PagesManager.getPage(PagesEnum.GO_TO_ALL_ROOMTYPES);
            }
            result = PagesManager.getPage(PagesEnum.GO_TO_ALL_ROOMTYPES);
            LOGGER.info(MessagesManager.getMessage(MessagesEnum.LOG_ROOMTYPE_CHANGE));
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }

    private void changeRoomType(RequestContext context, RoomType roomType) {
        String description = context.getRequestParameter(DESCRIPTION)[0];
        roomType.setDescription(description);
        int capacity = Integer.parseInt(context.getRequestParameter(CAPACITY)[0]);
        roomType.setCapacity(capacity);
        double amount = Double.parseDouble(context.getRequestParameter(AMOUNT)[0]);
        roomType.setAmount(amount);
    }
}
