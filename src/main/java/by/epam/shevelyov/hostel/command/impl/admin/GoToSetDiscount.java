package by.epam.shevelyov.hostel.command.impl.admin;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.controller.MethodsEnum;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.Discount;
import by.epam.shevelyov.hostel.entity.impl.Order;
import by.epam.shevelyov.hostel.entity.impl.RoomType;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;
import by.epam.shevelyov.hostel.service.DiscountService;
import by.epam.shevelyov.hostel.service.OrderService;
import by.epam.shevelyov.hostel.service.RoomTypeService;
import by.epam.shevelyov.hostel.validator.impl.OrderValidator;
import org.apache.log4j.Logger;

import java.util.List;

import static javax.servlet.RequestDispatcher.ERROR_MESSAGE;

public class GoToSetDiscount implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(GoToSetDiscount.class);
    private static final String METHOD = "method";
    private static final String ORDER = "order";
    private static final String DISCOUNTS = "discounts";
    private OrderValidator validator = OrderValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_ALL_ORDERS);
            }
            int orderId = Integer.parseInt(context.getRequestParameter(ORDER)[0]);
            Order order = OrderService.getOrderById(orderId);
            context.addRequestAttribute(ORDER, order);
            List<Discount> discounts = DiscountService.getAllDiscounts();
            context.addRequestAttribute(DISCOUNTS,discounts);
            result = PagesManager.getPage(PagesEnum.ADMIN_SET_DISCOUNT);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}
