package by.epam.shevelyov.hostel.command.impl;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.controller.MethodsEnum;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.validator.impl.ChangeLanguageValidator;
import org.apache.log4j.Logger;

public class ChangeLanguageCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(ChangeLanguageCommand.class);
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String METHOD = "method";
    private static final String LANG = "lang";
    private static final String REQUEST_PAGE = "requestPage";
    private ChangeLanguageValidator validator = ChangeLanguageValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.REDIRECT);
            MessagesEnum validateResult = validator.validate(context);
            result = context.getRequestParameter(REQUEST_PAGE)[0];
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return result;
            }
            String lang = context.getRequestParameter(LANG)[0];
            context.addSessionAttribute(LANG, lang);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}
