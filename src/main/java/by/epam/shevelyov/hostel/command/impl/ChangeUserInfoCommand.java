package by.epam.shevelyov.hostel.command.impl;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.controller.MethodsEnum;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.encoder.Encoder;
import by.epam.shevelyov.hostel.entity.impl.User;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;
import by.epam.shevelyov.hostel.service.UserService;
import by.epam.shevelyov.hostel.validator.impl.ChangeUserInfoValidator;
import org.apache.log4j.Logger;

public class ChangeUserInfoCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(ChangeUserInfoCommand.class);
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String METHOD = "method";
    private static final String USER = "user";
    private static final String PASSWORD = "password";
    private static final String EMAIL = "email";
    private static final String FIRST_NAME = "firstName";
    private static final String LAST_NAME = "lastName";
    private static final String PHONE = "phone";
    private ChangeUserInfoValidator validator = ChangeUserInfoValidator.getInstance();

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            MessagesEnum validateResult = validator.validate(context);
            if (!validateResult.equals(MessagesEnum.SUCCESS)) {
                context.addRequestAttribute(ERROR_MESSAGE, validateResult.getValue());
                return PagesManager.getPage(PagesEnum.GO_TO_PROFILE);
            }
            int userId = Integer.parseInt(context.getRequestParameter(USER)[0]);
            User user = UserService.getUserById(userId);
            changeUser(context, user);
            if (UserService.updateUser(user)) {
                context.addRequestAttribute(METHOD, MethodsEnum.REDIRECT);
            } else {
                context.addRequestAttribute(ERROR_MESSAGE, MessagesEnum.ERROR_USER_CHANGE);
                return PagesManager.getPage(PagesEnum.GO_TO_PROFILE);
            }
            result = PagesManager.getPage(PagesEnum.GO_TO_PROFILE);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }

    private void changeUser(RequestContext context, User user) {
        String password = context.getRequestParameter(PASSWORD)[0];
        String encodedPassword = Encoder.encode(password);
        user.setPassword(encodedPassword);
        String email = context.getRequestParameter(EMAIL)[0];
        user.setEmail(email);
        String phone = context.getRequestParameter(PHONE)[0];
        user.setPhoneNumber(phone);
        String firstName = context.getRequestParameter(FIRST_NAME)[0];
        user.setFirstName(firstName);
        String lastName = context.getRequestParameter(LAST_NAME)[0];
        user.setLastName(lastName);
    }
}
