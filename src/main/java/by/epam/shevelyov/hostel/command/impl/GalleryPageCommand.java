package by.epam.shevelyov.hostel.command.impl;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.controller.MethodsEnum;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;
import org.apache.log4j.Logger;


public class GalleryPageCommand implements ActionCommand {
    private static final Logger LOGGER = Logger.getLogger(GalleryPageCommand.class);
    private static final String METHOD = "method";

    @Override
    public String execute(RequestContext context) {
        String result = null;
        try {
            context.addRequestAttribute(METHOD, MethodsEnum.FORWARD);
            result = PagesManager.getPage(PagesEnum.GALLERY);
        } catch (Exception e) {
            LOGGER.error(e);
        }
        return result;
    }
}
