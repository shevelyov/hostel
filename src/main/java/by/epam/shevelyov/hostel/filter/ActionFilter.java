package by.epam.shevelyov.hostel.filter;

import by.epam.shevelyov.hostel.command.CommandsEnum;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <p>Filter for rejection wrong action and setting UTF-8 character encoding</p>
 *
 * @author Ilya Shevelyov
 */
@WebFilter(filterName = "ActionFilter", servletNames = { "Controller" })
public class ActionFilter implements Filter {
    private static final String ACTION = "action";
    private static final String UTF_8 = "UTF-8";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        servletRequest.setCharacterEncoding(UTF_8);
        servletResponse.setCharacterEncoding(UTF_8);
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        if (!processFilter(request)) {
            if (!response.isCommitted()) {
                response.sendError(404);
            }
        }
        filterChain.doFilter(request, response);
    }

    /**
     * <p>Method for checking action from user</p>
     *
     * @param request http request from user
     * @return true if action is right, false otherwise
     * @throws IOException
     * @throws ServletException
     */
    private boolean processFilter(HttpServletRequest request) throws IOException,
            ServletException {
        String action = request.getParameter(ACTION);
        if (action == null || action.isEmpty()) {
            return false;
        }
        for (CommandsEnum command : CommandsEnum.values()) {
            if (command.name().equals(action.toUpperCase())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void destroy() {

    }
}

