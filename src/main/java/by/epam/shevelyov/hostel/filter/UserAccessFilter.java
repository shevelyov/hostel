package by.epam.shevelyov.hostel.filter;

import by.epam.shevelyov.hostel.entity.impl.User;
import by.epam.shevelyov.hostel.entity.util.RoleEnum;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <p>Filter for checking action that requires to sign in</p>
 *
 * @author Ilya Shevelyov
 */
@WebFilter(filterName = "UserAccessFilter", servletNames = { "Controller" })
public class UserAccessFilter implements Filter {
    private static final String ACTION = "action";
    private static final String USER = "user";
    private static final int CODE_404 = 404;

    private enum UserCommand {
        PROFILE, BOOK, GO_TO_BOOK_PAGE, CHANGE_USER_INFO, USER_CHANGE_ORDER_STATUS, GO_TO_CHANGE_USER_INFO
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        processFilter(request, response);
        filterChain.doFilter(request, response);
    }

    /**
     * <p>Method for checking action from client and possibility to do this action for client</p>
     *
     * @param request  http request from client
     * @param response http response for client
     * @throws IOException
     * @throws ServletException
     */
    private void processFilter(HttpServletRequest request, HttpServletResponse response) throws IOException,
            ServletException {
        String action = request.getParameter(ACTION);
        if (action == null || action.isEmpty()) {
            if (!response.isCommitted()) {
                response.sendError(CODE_404);
            }
            return;
        }
        User user = (User) request.getSession().getAttribute(USER);
        for (UserCommand command : UserCommand.values()) {
            if (command.name().equals(action.toUpperCase())) {
                if (user == null) {
                    response.sendError(CODE_404);
                    return;
                }
                if (user.getRole() == null) {
                    response.sendError(CODE_404);
                    return;
                }
                if (!user.getRole().equals(RoleEnum.USER)) {
                    if (!response.isCommitted()) {
                        response.sendError(CODE_404);
                    }
                    return;
                }
                break;
            }
        }
    }

    @Override
    public void destroy() {

    }

}
