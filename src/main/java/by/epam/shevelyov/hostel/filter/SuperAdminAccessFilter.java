package by.epam.shevelyov.hostel.filter;

import by.epam.shevelyov.hostel.entity.impl.User;
import by.epam.shevelyov.hostel.entity.util.RoleEnum;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <p>Filter for checking access to the super admin pages</p>
 *
 * @author Ilya Shevelyov
 */
@WebFilter(filterName = "SuperAdminAccessFilter", servletNames = {"Controller"})
public class SuperAdminAccessFilter implements Filter {
    private static final String ACTION = "action";
    private static final String USER = "user";
    private static final int CODE_404 = 404;

    private enum AdminCommands {
        DELETE_ADMINISTRATOR, DELETE_DISCOUNT, DELETE_ROOM, DELETE_ROOMTYPE, ADD_ADMINISTRATOR,
        ADD_ROOMTYPE, ADD_ROOM, ADD_DISCOUNT, CHANGE_ADMINISTRATOR, CHANGE_DISCOUNT, CHANGE_ROOMTYPE,
        CHANGE_ROOM, GO_TO_ADD_ADMINISTRATOR, GO_TO_ADD_DISCOUNT, GO_TO_ADD_ROOMTYPE, GO_TO_ADD_ROOM,
        GO_TO_CHANGE_ADMINISTRATOR, GO_TO_CHANGE_DISCOUNT, GO_TO_CHANGE_ROOMTYPE, GO_TO_CHANGE_ROOM
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        processFilter(request, response);
        filterChain.doFilter(request, response);
    }

    /**
     * <p>Method for checking action from user and user access rights for this action</p>
     *
     * @param request  http request from user
     * @param response http response for user
     * @throws IOException
     * @throws ServletException
     */
    private void processFilter(HttpServletRequest request, HttpServletResponse response) throws IOException,
            ServletException {
        String action = request.getParameter(ACTION);
        if (action == null || action.isEmpty()) {
            return;
        }
        User user = (User) request.getSession().getAttribute(USER);
        for (AdminCommands command : AdminCommands.values()) {
            if (command.name().equals(action.toUpperCase())) {
                if (user == null) {
                    response.sendError(CODE_404);
                    return;
                }
                if (user.getRole() == null) {
                    response.sendError(CODE_404);
                    return;
                }
                if (!(user.getRole().equals(RoleEnum.SUPER))) {
                    response.sendError(CODE_404);
                    return;
                }
                break;
            }
        }
    }

    @Override
    public void destroy() {

    }

}

