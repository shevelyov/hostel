package by.epam.shevelyov.hostel.filter;

import by.epam.shevelyov.hostel.entity.impl.User;
import by.epam.shevelyov.hostel.entity.util.RoleEnum;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <p>Filter for checking access to the admin pages</p>
 *
 * @author Ilya Shevelyov
 */
@WebFilter(filterName = "AdminAccessFilter", servletNames = {"Controller"})
public class AdminAccessFilter implements Filter {
    private static final String ACTION = "action";
    private static final String USER = "user";
    private static final int CODE_404 = 404;

    private enum AdminCommands {
        ALL_ORDERS, ALL_USERS, ALL_BANS, ALL_DISCOUNTS, ALL_ADMINISTRATORS, ALL_ROOMS, ALL_ROOMTYPES,
        DELETE_BAN, ADD_BAN, CHANGE_BAN, ADMIN_CHANGE_ORDER_STATUS, SET_DISCOUNT, SHOW_AVAILABILITY, GO_TO_ADD_BAN,
        GO_TO_CHANGE_BAN, GO_TO_SHOW_AVAILABILITY, GO_TO_CHOOSE_PERIOD, GO_TO_SET_DISCOUNT, USER_DETAILS
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        processFilter(request, response);
        filterChain.doFilter(request, response);
    }

    /**
     * <p>Method for checking action from user and user access rights for this action</p>
     *
     * @param request  http request from user
     * @param response http response for user
     * @throws IOException
     * @throws ServletException
     */
    private void processFilter(HttpServletRequest request, HttpServletResponse response) throws IOException,
            ServletException {
        String action = request.getParameter(ACTION);
        if (action == null || action.isEmpty()) {
            return;
        }
        User user = (User) request.getSession().getAttribute(USER);
        for (AdminCommands command : AdminCommands.values()) {
            if (command.name().equals(action.toUpperCase())) {
                if (user == null) {
                    response.sendError(CODE_404);
                    return;
                }
                if (user.getRole() == null) {
                    response.sendError(CODE_404);
                    return;
                }
                if (!(user.getRole().equals(RoleEnum.ADMIN) || user.getRole().equals(RoleEnum.SUPER))) {
                    response.sendError(CODE_404);
                    return;
                }
                break;
            }
        }
    }

    @Override
    public void destroy() {

    }

}

