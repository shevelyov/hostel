package by.epam.shevelyov.hostel.validator.impl.admin;

import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.RoomType;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.MessagesManager;
import by.epam.shevelyov.hostel.service.RoomTypeService;
import by.epam.shevelyov.hostel.validator.Validator;
import by.epam.shevelyov.hostel.validator.exception.ValidateException;
import by.epam.shevelyov.hostel.validator.impl.RoomTypeValidator;
import org.apache.log4j.Logger;

import java.util.regex.Pattern;

public class AddRoomValidator implements Validator {
    private static final Logger LOGGER = Logger.getLogger(AddRoomValidator.class);
    private static final String ROOM_NUMBER = "roomnumber";
    private static final String REGEX_ROOM_NUMBER = "^[a-zA-Z0-9 -_#]{1,5}$";
    private RoomTypeValidator roomTypeValidator = RoomTypeValidator.getInstance();


    private static class InstanceHolder {
        private static final AddRoomValidator INSTANCE = new AddRoomValidator();
    }

    private AddRoomValidator() {
    }

    public static AddRoomValidator getInstance() {
        return AddRoomValidator.InstanceHolder.INSTANCE;
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_EMPTY_FIELDS;
        try {
            result = roomTypeValidator.validate(context);
            if (!result.equals(MessagesEnum.SUCCESS)) {
                throw new ValidateException();
            }
            String[] roomNumberParam = context.getRequestParameter(ROOM_NUMBER);
            if (!validateOnEmpty(roomNumberParam)) {
                result = MessagesEnum.ERROR_VALIDATE_EMPTY_FIELDS;
                throw new ValidateException();
            }
            String roomNumber = roomNumberParam[0];
            if (!Pattern.compile(REGEX_ROOM_NUMBER).matcher(roomNumber).matches()) {
                result = MessagesEnum.ERROR_VALIDATE_WRONG_PARAMS;
                throw new ValidateException();
            }
            result = MessagesEnum.SUCCESS;
        } catch (ValidateException e) {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }

    private boolean validateOnEmpty(String[]... args) {
        for (String[] mass : args) {
            if (mass == null || mass.length == 0 || mass[0] == null || mass[0].isEmpty()) {
                return false;
            }
        }
        return true;
    }
}

