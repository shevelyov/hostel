package by.epam.shevelyov.hostel.validator.impl;

import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.User;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.MessagesManager;
import by.epam.shevelyov.hostel.service.UserService;
import by.epam.shevelyov.hostel.validator.Validator;
import by.epam.shevelyov.hostel.validator.exception.ValidateException;
import org.apache.log4j.Logger;

public class UserValidator implements Validator {
    private static final Logger LOGGER = Logger.getLogger(UserValidator.class);
    private static final String USER = "user";

    private static class InstanceHolder {
        private static final UserValidator INSTANCE = new UserValidator();
    }

    private UserValidator() {}

    public static UserValidator getInstance() {
        return UserValidator.InstanceHolder.INSTANCE;
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_EMPTY_USER;
        try {
            String[] userParam = context.getRequestParameter(USER);
            if (userParam == null || userParam.length == 0 || userParam[0] == null || userParam[0].isEmpty()) {
                throw new ValidateException();
            }
            result = MessagesEnum.ERROR_VALIDATE_WRONG_USER;
            int userId = Integer.parseInt(userParam[0]);
            User user = UserService.getUserById(userId);
            if (user == null) {
                throw new ValidateException();
            }
            result = MessagesEnum.SUCCESS;
        } catch (ValidateException | IllegalArgumentException e) {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }
}
