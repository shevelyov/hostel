package by.epam.shevelyov.hostel.validator.impl;

import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.RoomType;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.MessagesManager;
import by.epam.shevelyov.hostel.service.RoomTypeService;
import by.epam.shevelyov.hostel.validator.Validator;
import by.epam.shevelyov.hostel.validator.exception.ValidateException;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ViewRoomsValidator implements Validator {
    private static final Logger LOGGER = Logger.getLogger(ViewRoomsValidator.class);
    private static final String PAGE = "page";
    private static final String ACTION = "action";
    private static final String BIGGEST_ROOM = "biggest_room";
    private static final String SMALLEST_ROOM = "smallest_room";
    private static final String CHEAPEST_ROOM = "cheapest_room";
    private static final String MOST_EXPENSIVE_ROOM = "most_expensive_room";
    private static final String VIEW_ROOMS = "view_rooms";
    private static final String PAGES_COUNT = "pagesCount";
    private static final String REGEX_PAGE = "(?m)[1-9]{1}[\\d]{0,5}";
    private static final int ROOMS_PER_PAGE = 3;

    private static class InstanceHolder {
        private static final ViewRoomsValidator INSTANCE = new ViewRoomsValidator();
    }

    private ViewRoomsValidator() {
    }

    public static ViewRoomsValidator getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_WRONG_ACTION;
        try {
            String action = context.getRequestParameter(ACTION)[0];
            String[] pageParam = context.getRequestParameter(PAGE);
            if (action.equals(BIGGEST_ROOM) || action.equals(SMALLEST_ROOM) ||
                    action.equals(CHEAPEST_ROOM) || action.equals(MOST_EXPENSIVE_ROOM) ||action.equals(VIEW_ROOMS)) {
                result = MessagesEnum.ERROR_VALIDATE_WRONG_PAGE;
                List<RoomType> allRoomTypes = RoomTypeService.getAllRoomTypes();
                int maxPage = calculateMaxPage(allRoomTypes, ROOMS_PER_PAGE);
                context.addRequestAttribute(PAGES_COUNT, maxPage);
                validatePageParam(context, pageParam);
                int page = Integer.parseInt(pageParam[0]);
                result = validatePage(context, page, maxPage);
            }
        } catch (ValidateException e) {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }

    private int calculateMaxPage(List<RoomType> list, int countPerPage) {
        int listSize = list.size();
        int maxPage = listSize / countPerPage;
        if (listSize % countPerPage != 0) {
            maxPage++;
        }
        return maxPage;
    }

    private void validatePageParam(RequestContext context, String[] param) throws ValidateException {
        if (param == null || param.length == 0 || param[0] == null || param[0].isEmpty()) {
            context.addRequestParameter(PAGE, new String[]{"1"});
            throw new ValidateException();
        }
        Matcher pageMatcher = Pattern.compile(REGEX_PAGE).matcher(param[0]);
        if (!pageMatcher.matches()) {
            context.addRequestParameter(PAGE, new String[]{"1"});
            throw new ValidateException();
        }
    }

    private MessagesEnum validatePage(RequestContext context, int page, int maxPage) throws ValidateException {
        MessagesEnum result = MessagesEnum.SUCCESS;
        if (page > maxPage) {
            context.addRequestParameter(PAGE, new String[]{"1"});
            if (maxPage != 0) {
                result = MessagesEnum.ERROR_VALIDATE_WRONG_PAGE;
            }
        }
        return result;
    }
}
