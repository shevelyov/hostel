package by.epam.shevelyov.hostel.validator.impl.admin;

import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.User;
import by.epam.shevelyov.hostel.entity.util.RoleEnum;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.MessagesManager;
import by.epam.shevelyov.hostel.service.UserService;
import by.epam.shevelyov.hostel.validator.Validator;
import by.epam.shevelyov.hostel.validator.exception.ValidateException;
import org.apache.log4j.Logger;

import java.sql.Date;
import java.util.Calendar;
import java.util.regex.Pattern;

public class AddBanValidator implements Validator {
    private static final Logger LOGGER = Logger.getLogger(AddBanValidator.class);
    private static final String LOGIN = "login";
    private static final String START_DATE = "startDate";
    private static final String END_DATE = "endDate";
    private static final String REASON = "reason";
    private static final String REGEX_REASON = "^.+$";


    private static class InstanceHolder {
        private static final AddBanValidator INSTANCE = new AddBanValidator();
    }

    private AddBanValidator() {
    }

    public static AddBanValidator getInstance() {
        return AddBanValidator.InstanceHolder.INSTANCE;
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_EMPTY_FIELDS;
        try {
            String[] loginParam = context.getRequestParameter(LOGIN);
            String[] startDateParam = context.getRequestParameter(START_DATE);
            String[] endDateParam = context.getRequestParameter(END_DATE);
            String[] reasonParam = context.getRequestParameter(REASON);
            if (!validateOnEmpty(loginParam, startDateParam, endDateParam, reasonParam)) {
                result = MessagesEnum.ERROR_VALIDATE_EMPTY_FIELDS;
                throw new ValidateException();
            }
            String login = loginParam[0];
            Date startDate = Date.valueOf(startDateParam[0]);
            Date endDate = Date.valueOf(endDateParam[0]);
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE,-1);
            Date today = new Date(calendar.getTime().getTime());
            String reason = reasonParam[0];
            if (!startDate.after(today) || !endDate.after(startDate)) {
                result = MessagesEnum.ERROR_VALIDATE_WRONG_DATES;
                throw new ValidateException();
            }
            User user = UserService.getUserByLogin(login);
            if (user == null|| !user.getRole().equals(RoleEnum.USER)) {
                result = MessagesEnum.ERROR_VALIDATE_WRONG_LOGIN;
                throw new ValidateException();
            }
            if (!Pattern.compile(REGEX_REASON).matcher(reason).matches()) {
                result = MessagesEnum.ERROR_VALIDATE_WRONG_PARAMS;
                throw new ValidateException();
            }
            result = MessagesEnum.SUCCESS;
        } catch (
                ValidateException e)

        {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }

    private boolean validateOnEmpty(String[]... args) {
        for (String[] mass : args) {
            if (mass == null || mass.length == 0 || mass[0] == null || mass[0].isEmpty()) {
                return false;
            }
        }
        return true;
    }
}

