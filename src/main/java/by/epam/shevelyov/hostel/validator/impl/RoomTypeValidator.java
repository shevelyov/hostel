package by.epam.shevelyov.hostel.validator.impl;

import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.RoomType;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.MessagesManager;
import by.epam.shevelyov.hostel.service.RoomTypeService;
import by.epam.shevelyov.hostel.validator.Validator;
import by.epam.shevelyov.hostel.validator.exception.ValidateException;
import org.apache.log4j.Logger;

public class RoomTypeValidator implements Validator {
    private static final Logger LOGGER = Logger.getLogger(RoomTypeValidator.class);
    private static final String ROOMTYPE = "roomtype";

    private static class InstanceHolder {
        private static final RoomTypeValidator INSTANCE = new RoomTypeValidator();
    }

    private RoomTypeValidator() {
    }

    public static RoomTypeValidator getInstance() {
        return RoomTypeValidator.InstanceHolder.INSTANCE;
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_EMPTY_ROOMTYPE;
        try {
            String[] roomTypeParam = context.getRequestParameter(ROOMTYPE);
            if (roomTypeParam == null || roomTypeParam.length == 0 || roomTypeParam[0] == null || roomTypeParam[0].isEmpty()) {
                throw new ValidateException();
            }
            result = MessagesEnum.ERROR_VALIDATE_WRONG_ROOMTYPE;
            int roomTypeId = Integer.parseInt(roomTypeParam[0]);
            RoomType roomType = RoomTypeService.getRoomTypeById(roomTypeId);
            if (roomType == null) {
                throw new ValidateException();
            }
            result = MessagesEnum.SUCCESS;
        } catch (ValidateException | IllegalArgumentException e) {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }
}
