package by.epam.shevelyov.hostel.validator.impl;

import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.Order;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.MessagesManager;
import by.epam.shevelyov.hostel.service.OrderService;
import by.epam.shevelyov.hostel.validator.Validator;
import by.epam.shevelyov.hostel.validator.exception.ValidateException;
import org.apache.log4j.Logger;

public class OrderValidator implements Validator {
    private static final Logger LOGGER = Logger.getLogger(OrderValidator.class);
    private static final String ORDER = "order";

    private static class InstanceHolder {
        private static final OrderValidator INSTANCE = new OrderValidator();
    }

    private OrderValidator() {}

    public static OrderValidator getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_EMPTY_ORDER;
        try {
            String[] orderParam = context.getRequestParameter(ORDER);
            if (orderParam == null || orderParam.length == 0 ||
                    orderParam[0] == null || orderParam[0].isEmpty()) {
                throw new ValidateException();
            }
            result = MessagesEnum.ERROR_VALIDATE_WRONG_ORDER;
            int orderId = Integer.parseInt(orderParam[0]);
            Order order = OrderService.getOrderById(orderId);
            if (order == null) {
                throw new ValidateException();
            }
            result = MessagesEnum.SUCCESS;
        } catch (ValidateException | IllegalArgumentException e) {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }
}
