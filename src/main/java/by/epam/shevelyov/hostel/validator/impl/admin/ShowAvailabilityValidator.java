package by.epam.shevelyov.hostel.validator.impl.admin;

import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.Room;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.MessagesManager;
import by.epam.shevelyov.hostel.service.RoomService;
import by.epam.shevelyov.hostel.validator.Validator;
import by.epam.shevelyov.hostel.validator.exception.ValidateException;
import org.apache.log4j.Logger;

import java.sql.Date;
import java.util.List;

public class ShowAvailabilityValidator implements Validator {
    private static final Logger LOGGER = Logger.getLogger(ShowAvailabilityValidator.class);
    private static final String START = "start";
    private static final String END = "end";

    private static class InstanceHolder {
        private static final ShowAvailabilityValidator INSTANCE = new ShowAvailabilityValidator();
    }

    private ShowAvailabilityValidator() {
    }

    public static ShowAvailabilityValidator getInstance() {
        return ShowAvailabilityValidator.InstanceHolder.INSTANCE;
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_EMPTY_FIELDS;
        try {
            String[] startParam = context.getRequestParameter(START);
            String[] endParam = context.getRequestParameter(END);
            if (!validateOnEmpty(startParam) || !validateOnEmpty(endParam)) {
                throw new ValidateException();
            }
            List<Room> rooms = RoomService.getAllRooms();
            if (rooms.isEmpty() || rooms.size() == 0 || rooms == null){
                result = MessagesEnum.ERROR_VALIDATE_WRONG_SHOW;
                throw new ValidateException();
            }
            Date start = Date.valueOf(startParam[0]);
            Date end = Date.valueOf(endParam[0]);
            if (start.after(end)) {
                result = MessagesEnum.ERROR_VALIDATE_WRONG_DATES;
                throw new ValidateException();
            }
            result = MessagesEnum.SUCCESS;
        } catch (
                ValidateException e)

        {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }

    private boolean validateOnEmpty(String[]... args) {
        for (String[] mass : args) {
            if (mass == null || mass.length == 0 || mass[0] == null || mass[0].isEmpty()) {
                return false;
            }
        }
        return true;
    }
}


