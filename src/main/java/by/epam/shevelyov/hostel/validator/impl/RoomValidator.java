package by.epam.shevelyov.hostel.validator.impl;

import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.Room;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.MessagesManager;
import by.epam.shevelyov.hostel.service.RoomService;
import by.epam.shevelyov.hostel.validator.Validator;
import by.epam.shevelyov.hostel.validator.exception.ValidateException;
import org.apache.log4j.Logger;

public class RoomValidator implements Validator{
    private static final Logger LOGGER = Logger.getLogger(RoomValidator.class);
    private static final String ROOM = "room";

    private static class InstanceHolder {
        private static final RoomValidator INSTANCE = new RoomValidator();
    }

    private RoomValidator() {}

    public static RoomValidator getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_EMPTY_ROOM;
        try {
            String[] roomParam = context.getRequestParameter(ROOM);
            if (roomParam == null || roomParam.length == 0 || roomParam[0] == null || roomParam[0].isEmpty()) {
                throw new ValidateException();
            }
            result = MessagesEnum.ERROR_VALIDATE_WRONG_ROOM;
            int roomId = Integer.parseInt(roomParam[0]);
            Room room = RoomService.getRoomById(roomId);
            if (room == null) {

                throw new ValidateException();
            }
            result = MessagesEnum.SUCCESS;
        } catch (ValidateException | IllegalArgumentException e) {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }
}
