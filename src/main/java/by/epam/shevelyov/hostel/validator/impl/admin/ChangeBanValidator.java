package by.epam.shevelyov.hostel.validator.impl.admin;

import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.Ban;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.MessagesManager;
import by.epam.shevelyov.hostel.service.BanService;
import by.epam.shevelyov.hostel.validator.Validator;
import by.epam.shevelyov.hostel.validator.exception.ValidateException;
import by.epam.shevelyov.hostel.validator.impl.BanValidator;
import org.apache.log4j.Logger;

import java.sql.Date;
import java.util.Calendar;
import java.util.regex.Pattern;

public class ChangeBanValidator implements Validator {
    private static final Logger LOGGER = Logger.getLogger(ChangeBanValidator.class);
    private static final String BAN = "ban";
    private static final String END_DATE = "endDate";
    private static final String REASON = "reason";
    private static final String REGEX_REASON = "^.+$";
    private BanValidator banValidator = BanValidator.getInstance();


    private static class InstanceHolder {
        private static final ChangeBanValidator INSTANCE = new ChangeBanValidator();
    }

    private ChangeBanValidator() {
    }

    public static ChangeBanValidator getInstance() {
        return ChangeBanValidator.InstanceHolder.INSTANCE;
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = null;
        try {
            result = banValidator.validate(context);
            if (!result.equals(MessagesEnum.SUCCESS)) {
                throw new ValidateException();
            }
            String[] endDateParam = context.getRequestParameter(END_DATE);
            String[] reasonParam = context.getRequestParameter(REASON);
            String[] banParam = context.getRequestParameter(BAN);
            Ban ban = BanService.getBanById(Integer.parseInt(banParam[0]));
            if (!validateOnEmpty(endDateParam, reasonParam)) {
                result = MessagesEnum.ERROR_VALIDATE_EMPTY_FIELDS;
                throw new ValidateException();
            }
            Date startDate = ban.getStartDate();
            Date endDate = Date.valueOf(endDateParam[0]);
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, -1);
            Date today = new Date(calendar.getTime().getTime());
            String reason = reasonParam[0];
            if (!endDate.after(today) || !endDate.after(startDate)) {
                result = MessagesEnum.ERROR_VALIDATE_WRONG_DATES;
                throw new ValidateException();
            }
            if (!Pattern.compile(REGEX_REASON).matcher(reason).matches()) {
                result = MessagesEnum.ERROR_VALIDATE_WRONG_PARAMS;
                throw new ValidateException();
            }
            result = MessagesEnum.SUCCESS;
        } catch (
                ValidateException e)

        {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }

    private boolean validateOnEmpty(String[]... args) {
        for (String[] mass : args) {
            if (mass == null || mass.length == 0 || mass[0] == null || mass[0].isEmpty()) {
                return false;
            }
        }
        return true;
    }
}

