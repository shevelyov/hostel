package by.epam.shevelyov.hostel.validator.impl;

import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.Order;
import by.epam.shevelyov.hostel.entity.impl.OrderType;
import by.epam.shevelyov.hostel.entity.impl.Room;
import by.epam.shevelyov.hostel.entity.impl.RoomType;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.MessagesManager;
import by.epam.shevelyov.hostel.service.OrderService;
import by.epam.shevelyov.hostel.service.OrderTypeService;
import by.epam.shevelyov.hostel.service.RoomService;
import by.epam.shevelyov.hostel.service.RoomTypeService;
import by.epam.shevelyov.hostel.validator.Validator;
import by.epam.shevelyov.hostel.validator.exception.ValidateException;
import org.apache.log4j.Logger;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class BookValidator implements Validator {
    private static final Logger LOGGER = Logger.getLogger(BookValidator.class);
    private static final String ROOMTYPE = "roomtype";
    private static final String GUESTS = "guests";
    private static final String CHECKIN = "checkin";
    private static final String CHECKOUT = "checkout";
    private RoomTypeValidator roomTypeValidator = RoomTypeValidator.getInstance();
    private OrderTypeValidator orderTypeValidator = OrderTypeValidator.getInstance();

    private static class InstanceHolder {
        private static final BookValidator INSTANCE = new BookValidator();
    }

    private BookValidator() {
    }

    public static BookValidator getInstance() {
        return BookValidator.InstanceHolder.INSTANCE;
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_EMPTY_FIELDS;
        try {
            result = roomTypeValidator.validate(context);
            if (!result.equals(MessagesEnum.SUCCESS)) {
                throw new ValidateException();
            }
            result = orderTypeValidator.validate(context);
            if (!result.equals(MessagesEnum.SUCCESS)) {
                throw new ValidateException();
            }
            String[] roomTypeParam = context.getRequestParameter(ROOMTYPE);
            String[] guestsParam = context.getRequestParameter(GUESTS);
            String[] checkInParam = context.getRequestParameter(CHECKIN);
            String[] checkOutParam = context.getRequestParameter(CHECKOUT);
            if (!validateOnEmpty(guestsParam) || !validateOnEmpty(checkInParam) || !validateOnEmpty(checkOutParam)) {
                throw new ValidateException();
            }
            RoomType roomType = RoomTypeService.getRoomTypeById(Integer.parseInt(roomTypeParam[0]));
            int guest = Integer.parseInt(guestsParam[0]);
            Date today = new Date(Calendar.getInstance().getTime().getTime());
            Date checkin = Date.valueOf(checkInParam[0]);
            Date checkout = Date.valueOf(checkOutParam[0]);
            if (!checkin.after(today) || !checkout.after(checkin)) {
                result = MessagesEnum.ERROR_VALIDATE_WRONG_DATE;
                throw new ValidateException();
            }
            if (guest < 1 || guest > roomType.getCapacity()) {
                result = MessagesEnum.ERROR_VALIDATE_WRONG_GUESTS;
                throw new ValidateException();
            }
            if (!isPlacesAvailable(roomType.getId(), checkin.toString(), checkout.toString(), guest)) {
                result = MessagesEnum.ERROR_VALIDATE_NO_PLACES;
                throw new ValidateException();
            }

            result = MessagesEnum.SUCCESS;
        } catch (ValidateException e) {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }

    private boolean validateOnEmpty(String[] param) {
        return (param != null && param.length > 0 && param[0] != null && !param[0].isEmpty());
    }

    private boolean isPlacesAvailable(int roomTypeId, String checkin, String checkout, int guests) {
        boolean result = true;
        List<Room> rooms = RoomService.getAllRoomsByRoomTypeId(roomTypeId);
        for (Room room : rooms) {
            Map<LocalDate, Integer> freePlacesByDates = OrderService.getFreePlacesByDatesAndRoomId(room.getId(), checkin, checkout);
            for (LocalDate date = LocalDate.parse(checkin); date.isBefore(LocalDate.parse(checkout)); date = date.plusDays(1)) {
                if (freePlacesByDates.containsKey(date)) {
                    if (guests > freePlacesByDates.get(date)) {
                        result = false;
                    }
                }

            }
        }
        return result;
    }
}
