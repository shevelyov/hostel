package by.epam.shevelyov.hostel.validator.impl.admin;

import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.MessagesManager;
import by.epam.shevelyov.hostel.validator.Validator;
import by.epam.shevelyov.hostel.validator.exception.ValidateException;
import org.apache.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AddDiscountValidator implements Validator {
    private static final Logger LOGGER = Logger.getLogger(AddDiscountValidator.class);
    private static final String TITLE = "title";
    private static final String PERCENTAGE = "percentage";
    private static final String DESCRIPTION = "description";
    private static final String REGEX_TEXT = "^.+$";
    private static final String REGEX_PERCENTAGE = "^[0-9]{1,2}$";


    private static class InstanceHolder {
        private static final AddDiscountValidator INSTANCE = new AddDiscountValidator();
    }

    private AddDiscountValidator() {
    }

    public static AddDiscountValidator getInstance() {
        return AddDiscountValidator.InstanceHolder.INSTANCE;
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_EMPTY_FIELDS;
        try {
            String[] titleParam = context.getRequestParameter(TITLE);
            String[] percentageParam = context.getRequestParameter(PERCENTAGE);
            String[] descriptionParam = context.getRequestParameter(DESCRIPTION);
            if (!validateOnEmpty(titleParam, percentageParam, descriptionParam)) {
                throw new ValidateException();
            }
            String title = titleParam[0];
            String percentage = percentageParam[0];
            String description = descriptionParam[0];
            if (!validateOnRegex(title, percentage, description)) {
                result = MessagesEnum.ERROR_VALIDATE_WRONG_PARAMS;
                throw new ValidateException();
            }
            result = MessagesEnum.SUCCESS;
        } catch (
                ValidateException e)

        {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }

    private boolean validateOnEmpty(String[]... args) {
        for (String[] mass : args) {
            if (mass == null || mass.length == 0 || mass[0] == null || mass[0].isEmpty()) {
                return false;
            }
        }
        return true;
    }

    private boolean validateOnRegex(String title, String percentage, String description) throws ValidateException {
        Matcher titleMatcher = Pattern.compile(REGEX_TEXT).matcher(title);
        Matcher percentageMatcher = Pattern.compile(REGEX_PERCENTAGE).matcher(percentage);
        Matcher descriptionMatcher = Pattern.compile(REGEX_TEXT).matcher(description);
        return (titleMatcher.matches() && percentageMatcher.matches() && descriptionMatcher.matches());
    }
}

