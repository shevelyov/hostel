package by.epam.shevelyov.hostel.validator.impl;

import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.Ban;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.MessagesManager;
import by.epam.shevelyov.hostel.service.BanService;
import by.epam.shevelyov.hostel.validator.Validator;
import by.epam.shevelyov.hostel.validator.exception.ValidateException;
import org.apache.log4j.Logger;

public class BanValidator implements Validator{
    private static final Logger LOGGER = Logger.getLogger(BanValidator.class);
    private static final String BAN = "ban";

    private static class InstanceHolder {
        private static final BanValidator INSTANCE = new BanValidator();
    }

    private BanValidator() {}

    public static BanValidator getInstance() {
        return BanValidator.InstanceHolder.INSTANCE;
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_EMPTY_BAN;
        try {
            String[] banParam = context.getRequestParameter(BAN);
            if (banParam == null || banParam.length == 0 || banParam[0] == null || banParam[0].isEmpty()) {
                throw new ValidateException();
            }
            result = MessagesEnum.ERROR_VALIDATE_WRONG_BAN;
            int banId = Integer.parseInt(banParam[0]);
            Ban ban = BanService.getBanById(banId);
            if (ban == null) {
                throw new ValidateException();
            }
            result = MessagesEnum.SUCCESS;
        } catch (ValidateException | IllegalArgumentException e) {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }
}
