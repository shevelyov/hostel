package by.epam.shevelyov.hostel.validator.impl.admin;

import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.Entity;
import by.epam.shevelyov.hostel.entity.impl.*;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.MessagesManager;
import by.epam.shevelyov.hostel.service.*;
import by.epam.shevelyov.hostel.validator.Validator;
import by.epam.shevelyov.hostel.validator.exception.ValidateException;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AdminCatalogValidator implements Validator {
    private static final Logger LOGGER = Logger.getLogger(AdminCatalogValidator.class);
    private static final String PAGE = "page";
    private static final String ACTION = "action";
    private static final String ALL_BANS = "all_bans";
    private static final String ALL_DISCOUNTS = "all_discounts";
    private static final String ALL_USERS = "all_users";
    private static final String ALL_ORDERS = "all_orders";
    private static final String ALL_ADMINISTRATORS = "all_administrators";
    private static final String ALL_ROOMS = "all_rooms";
    private static final String ALL_ROOMTYPES = "all_roomtypes";
    private static final String PAGES_COUNT = "pagesCount";
    private static final String USER = "user";
    private static final String REGEX_PAGE = "(?m)[1-9]{1}[\\d]{0,5}";
    private static final int COUNT_PER_PAGE = 10;

    private static class InstanceHolder {
        private static final AdminCatalogValidator INSTANCE = new AdminCatalogValidator();
    }

    private AdminCatalogValidator() {
    }

    public static AdminCatalogValidator getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_WRONG_PAGE;
        try {
            String action = context.getRequestParameter(ACTION)[0];
            switch (action) {
                case ALL_ORDERS:
                    List<Order> allOrders = OrderService.getAllOrders();
                    result = validate(context, allOrders, COUNT_PER_PAGE);
                    break;
                case ALL_USERS:
                    List<User> allUsers = UserService.getAllUsersByRole(USER);
                    result = validate(context, allUsers, COUNT_PER_PAGE);
                    break;
                case ALL_BANS:
                    List<Ban> allBans = BanService.getAllBans();
                    result = validate(context, allBans, COUNT_PER_PAGE);
                    break;
                case ALL_DISCOUNTS:
                    List<Discount> allDiscounts = DiscountService.getAllDiscounts();
                    result = validate(context, allDiscounts, COUNT_PER_PAGE);
                    break;
                case ALL_ADMINISTRATORS:
                    List<User> allAdministrators = UserService.getAllAdmins();
                    result = validate(context, allAdministrators, COUNT_PER_PAGE);
                    break;
                case ALL_ROOMS:
                    List<Room> allRooms = RoomService.getAllRooms();
                    result = validate(context, allRooms, COUNT_PER_PAGE);
                    break;
                case ALL_ROOMTYPES:
                    List<RoomType> allRoomTypes = RoomTypeService.getAllRoomTypes();
                    result = validate(context, allRoomTypes, COUNT_PER_PAGE);
                    break;
            }
        } catch (ValidateException e) {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }

    private MessagesEnum validate(RequestContext context, List<? extends Entity> list, int countPerPage)
            throws ValidateException {
        int maxPage = calculateMaxPage(list, countPerPage);
        context.addRequestAttribute(PAGES_COUNT, maxPage);
        String[] pageParam = context.getRequestParameter(PAGE);
        validatePageParam(context, pageParam);
        int page = Integer.parseInt(pageParam[0]);
        return validatePage(context, page, maxPage);
    }

    private int calculateMaxPage(List<? extends Entity> list, int countPerPage) {
        int listSize = list.size();
        int maxPage = listSize / countPerPage;
        if (listSize % countPerPage != 0) {
            maxPage++;
        }
        return maxPage;
    }

    private void validatePageParam(RequestContext context, String[] param) throws ValidateException {
        if (param == null || param.length == 0 || param[0] == null || param[0].isEmpty()) {
            context.addRequestParameter(PAGE, new String[]{"1"});
            throw new ValidateException();
        }
        Matcher pageMatcher = Pattern.compile(REGEX_PAGE).matcher(param[0]);
        if (!pageMatcher.matches()) {
            context.addRequestParameter(PAGE, new String[]{"1"});
            throw new ValidateException();
        }
    }

    private MessagesEnum validatePage(RequestContext context, int page, int maxPage) throws ValidateException {
        MessagesEnum result = MessagesEnum.SUCCESS;
        if (page > maxPage) {
            context.addRequestParameter(PAGE, new String[]{"1"});
            if (maxPage != 0) {
                result = MessagesEnum.ERROR_VALIDATE_WRONG_PAGE;
            }
        }
        return result;
    }
}
