package by.epam.shevelyov.hostel.validator;

import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.manager.MessagesEnum;

/**
 * <p>Interface that describes validate operations by validators</p>
 *
 * @author Ilya Shevelyov
 */
public interface Validator {

    /**
     * <p>Method that validate request</p>
     *
     * @param context Context with information from the request
     * @return {@link MessagesEnum} that contains key of message to send it to user
     */
    MessagesEnum validate(RequestContext context);
}
