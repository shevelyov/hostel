package by.epam.shevelyov.hostel.validator.impl;

import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.User;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.MessagesManager;
import by.epam.shevelyov.hostel.service.UserService;
import by.epam.shevelyov.hostel.validator.Validator;
import by.epam.shevelyov.hostel.validator.exception.ValidateException;
import org.apache.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegistrationValidator implements Validator {
    private static final Logger LOGGER = Logger.getLogger(RegistrationValidator.class);
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String REPEAT_PASSWORD = "repeatPassword";
    private static final String EMAIL = "email";
    private static final String FIRST_NAME = "firstName";
    private static final String LAST_NAME = "lastName";
    private static final String PHONE = "phone";
    private static final String REGEX_LOGIN = "^[a-zA-Z](.[\\w9_-]*){3,20}";
    private static final String REGEX_PASSWORD = "^[\\w]{6,30}";
    private static final String REGEX_EMAIL = "^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$";
    private static final String REGEX_PHONE = "^\\s*(?:\\+?(\\d{1,3}))?[-. (]*(\\d{2,3})[-. )]*(\\d{3})[-. ]*(\\d{2})[-. ]*(\\d{2,5})?\\s*$";
    private static final String REGEX_NAME = "^[a-zA-Z ,.'-]+$";

    private static class InstanceHolder {
        private static final RegistrationValidator INSTANCE = new RegistrationValidator();
    }

    private RegistrationValidator() {
    }

    public static RegistrationValidator getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_EMPTY_FIELDS;
        try {
            String[] loginParam = context.getRequestParameter(LOGIN);
            String[] passwordParam = context.getRequestParameter(PASSWORD);
            String[] repeatPasswordParam = context.getRequestParameter(REPEAT_PASSWORD);
            String[] emailParam = context.getRequestParameter(EMAIL);
            String[] phoneParam = context.getRequestParameter(PHONE);
            String[] firstNameParam = context.getRequestParameter(FIRST_NAME);
            String[] lastNameParam = context.getRequestParameter(LAST_NAME);
            if (!validateOnEmpty(loginParam, passwordParam, repeatPasswordParam, emailParam, phoneParam, firstNameParam, lastNameParam)) {
                throw new ValidateException();
            }
            String login = loginParam[0];
            String password = passwordParam[0];
            String repeatPassword = repeatPasswordParam[0];
            String email = emailParam[0];
            String phone = phoneParam[0];
            String firstName = firstNameParam[0];
            String lastName = lastNameParam[0];
            if (!validateOnRegex(login, password, email, phone, firstName, lastName)) {
                result = MessagesEnum.ERROR_VALIDATE_WRONG_PARAMS;
                throw new ValidateException();
            }
            if (!password.equals(repeatPassword)) {
                result = MessagesEnum.ERROR_VALIDATE_WRONG_PASSWORD;
                throw new ValidateException();
            }
            User user = UserService.getUserByLogin(login);
            if (user != null) {
                result = MessagesEnum.ERROR_VALIDATE_EXIST_LOGIN;
                throw new ValidateException();
            }
            user = UserService.getUserByEmail(email);
            if (user != null) {
                result = MessagesEnum.ERROR_VALIDATE_EXIST_EMAIL;
                throw new ValidateException();
            }
            result = MessagesEnum.SUCCESS;
        } catch (ValidateException e) {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }

    private boolean validateOnEmpty(String[]... args) {
        for (String[] mass : args) {
            if (mass == null || mass.length == 0 || mass[0] == null || mass[0].isEmpty()) {
                return false;
            }
        }
        return true;
    }

    private boolean validateOnRegex(String login, String password, String email, String phone, String firstName, String lastName) throws ValidateException {
        Matcher loginMatcher = Pattern.compile(REGEX_LOGIN).matcher(login);
        Matcher passwordMatcher = Pattern.compile(REGEX_PASSWORD).matcher(password);
        Matcher emailMatcher = Pattern.compile(REGEX_EMAIL).matcher(email);
        Matcher phoneMatcher = Pattern.compile(REGEX_PHONE).matcher(phone);
        Matcher firstNameMatcher = Pattern.compile(REGEX_NAME).matcher(firstName);
        Matcher lastNameMatcher = Pattern.compile(REGEX_NAME).matcher(lastName);
        return (loginMatcher.matches() && passwordMatcher.matches() && emailMatcher.matches() && phoneMatcher.matches() && firstNameMatcher.matches() && lastNameMatcher.matches());
    }
}

