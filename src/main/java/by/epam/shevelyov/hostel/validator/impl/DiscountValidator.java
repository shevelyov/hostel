package by.epam.shevelyov.hostel.validator.impl;

import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.Discount;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.MessagesManager;
import by.epam.shevelyov.hostel.service.DiscountService;
import by.epam.shevelyov.hostel.validator.Validator;
import by.epam.shevelyov.hostel.validator.exception.ValidateException;
import org.apache.log4j.Logger;

public class DiscountValidator implements Validator {
    private static final Logger LOGGER = Logger.getLogger(DiscountValidator.class);
    private static final String DISCOUNT = "discount";

    private static class InstanceHolder {
        private static final DiscountValidator INSTANCE = new DiscountValidator();
    }

    private DiscountValidator() {}

    public static DiscountValidator getInstance() {
        return DiscountValidator.InstanceHolder.INSTANCE;
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_EMPTY_DISCOUNT;
        try {
            String[] discountParam = context.getRequestParameter(DISCOUNT);
            if (discountParam == null || discountParam.length == 0 || discountParam[0] == null || discountParam[0].isEmpty()) {
                throw new ValidateException();
            }
            result = MessagesEnum.ERROR_VALIDATE_WRONG_DISCOUNT;
            int discountId = Integer.parseInt(discountParam[0]);
            Discount discount  = DiscountService.getDiscountById(discountId);
            if (discount == null) {
                throw new ValidateException();
            }
            result = MessagesEnum.SUCCESS;
        } catch (ValidateException | IllegalArgumentException e) {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }
}
