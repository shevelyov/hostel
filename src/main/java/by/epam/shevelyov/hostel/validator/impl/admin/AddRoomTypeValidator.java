package by.epam.shevelyov.hostel.validator.impl.admin;

import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.MessagesManager;
import by.epam.shevelyov.hostel.validator.Validator;
import by.epam.shevelyov.hostel.validator.exception.ValidateException;
import org.apache.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AddRoomTypeValidator implements Validator {
    private static final Logger LOGGER = Logger.getLogger(AddRoomTypeValidator.class);
    private static final String DESCRIPTION = "description";
    private static final String CAPACITY = "capacity";
    private static final String AMOUNT = "amount";
    private static final String REGEX_TEXT = "^.+$";
    private static final String REGEX_CAPACITY = "^[0-9]{1,2}$";
    private static final String REGEX_AMOUNT = "^[0-9]{1,5}(\\.[0-9]{0,3})?$";


    private static class InstanceHolder {
        private static final AddRoomTypeValidator INSTANCE = new AddRoomTypeValidator();
    }

    private AddRoomTypeValidator() {
    }

    public static AddRoomTypeValidator getInstance() {
        return AddRoomTypeValidator.InstanceHolder.INSTANCE;
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_EMPTY_FIELDS;
        try {
            String[] descriptionParam = context.getRequestParameter(DESCRIPTION);
            String[] capacityParam = context.getRequestParameter(CAPACITY);
            String[] amountParam = context.getRequestParameter(AMOUNT);
            if (!validateOnEmpty(descriptionParam, capacityParam, amountParam)) {
                throw new ValidateException();
            }
            String description = descriptionParam[0];
            String capacity = capacityParam[0];
            String amount = amountParam[0];
            if (!validateOnRegex(description, capacity, amount)) {
                result = MessagesEnum.ERROR_VALIDATE_WRONG_PARAMS;
                throw new ValidateException();
            }
            result = MessagesEnum.SUCCESS;
        } catch (
                ValidateException e)

        {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }

    private boolean validateOnEmpty(String[]... args) {
        for (String[] mass : args) {
            if (mass == null || mass.length == 0 || mass[0] == null || mass[0].isEmpty()) {
                return false;
            }
        }
        return true;
    }

    private boolean validateOnRegex(String description, String capacity, String amount) throws ValidateException {
        Matcher descriptionMatcher = Pattern.compile(REGEX_TEXT).matcher(description);
        Matcher capacityMatcher = Pattern.compile(REGEX_CAPACITY).matcher(capacity);
        Matcher amountMatcher = Pattern.compile(REGEX_AMOUNT).matcher(amount);
        return (descriptionMatcher.matches() && capacityMatcher.matches() && amountMatcher.matches());
    }
}

