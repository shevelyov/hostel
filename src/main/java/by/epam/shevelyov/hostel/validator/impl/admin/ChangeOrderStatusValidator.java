package by.epam.shevelyov.hostel.validator.impl.admin;

import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.OrderStatus;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.MessagesManager;
import by.epam.shevelyov.hostel.service.OrderStatusService;
import by.epam.shevelyov.hostel.validator.Validator;
import by.epam.shevelyov.hostel.validator.exception.ValidateException;
import by.epam.shevelyov.hostel.validator.impl.OrderValidator;
import org.apache.log4j.Logger;

public class ChangeOrderStatusValidator implements Validator {
    private static final Logger LOGGER = Logger.getLogger(ChangeOrderStatusValidator.class);
    private static final String STATUS = "status";
    private OrderValidator orderValidator = OrderValidator.getInstance();

    private static class InstanceHolder {
        private static final ChangeOrderStatusValidator INSTANCE = new ChangeOrderStatusValidator();
    }

    private ChangeOrderStatusValidator() {
    }

    public static ChangeOrderStatusValidator getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_EMPTY_STATUS;
        try {
            result = orderValidator.validate(context);
            if (!result.equals(MessagesEnum.SUCCESS)) {
                throw new ValidateException();
            }
            String[] statusParam = context.getRequestParameter(STATUS);
            if (statusParam == null || statusParam.length == 0 || statusParam[0] == null || statusParam[0].isEmpty()) {
                result = MessagesEnum.ERROR_VALIDATE_EMPTY_STATUS;
                throw new ValidateException();
            }
            result = MessagesEnum.ERROR_VALIDATE_WRONG_STATUS;
            String status = statusParam[0];
            OrderStatus orderStatus = OrderStatusService.getOrderStatusByStatus(status).get(0);
            if (orderStatus == null) {
                throw new ValidateException();
            }
            result = MessagesEnum.SUCCESS;
        } catch (ValidateException | IllegalArgumentException e) {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }
}
