package by.epam.shevelyov.hostel.validator.impl;

import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.OrderType;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.MessagesManager;
import by.epam.shevelyov.hostel.service.OrderTypeService;
import by.epam.shevelyov.hostel.validator.Validator;
import by.epam.shevelyov.hostel.validator.exception.ValidateException;
import org.apache.log4j.Logger;

public class OrderTypeValidator implements Validator {
    private static final Logger LOGGER = Logger.getLogger(OrderTypeValidator.class);
    private static final String ORDERTYPE = "ordertype";

    private static class InstanceHolder {
        private static final OrderTypeValidator INSTANCE = new OrderTypeValidator();
    }

    private OrderTypeValidator() {
    }

    public static OrderTypeValidator getInstance() {
        return OrderTypeValidator.InstanceHolder.INSTANCE;
    }

    @Override
    public MessagesEnum validate(RequestContext context) {
        MessagesEnum result = MessagesEnum.ERROR_VALIDATE_EMPTY_ORDERTYPE;
        try {
            String[] orderTypeParam = context.getRequestParameter(ORDERTYPE);
            if (orderTypeParam == null || orderTypeParam.length == 0 || orderTypeParam[0] == null || orderTypeParam[0].isEmpty()) {
                throw new ValidateException();
            }
            result = MessagesEnum.ERROR_VALIDATE_WRONG_ORDERTYPE;
            int orderTypeId = Integer.parseInt(orderTypeParam[0]);
            OrderType orderType = OrderTypeService.getOrderTypeById(orderTypeId);
            if (orderType == null) {
                throw new ValidateException();
            }
            result = MessagesEnum.SUCCESS;
        } catch (ValidateException | IllegalArgumentException e) {
            LOGGER.error(MessagesManager.getMessage(result), e);
        }
        return result;
    }
}
