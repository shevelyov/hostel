package by.epam.shevelyov.hostel.controller;

/**
 * <p>Enum from choosing how to redirect user to the result page</p>
 *
 * @author Ilya Shevelyov
 */
public enum MethodsEnum {
    FORWARD, REDIRECT;
}
