package by.epam.shevelyov.hostel.controller;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.command.factory.ActionFactory;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.manager.MessagesManager;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;
import by.epam.shevelyov.hostel.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <p>Main controller to receive and process http requests from users
 * and to generate a http response.</p>
 *
 * @author Ilya Shevelyov
 */

@WebServlet(name = "Controller", urlPatterns = "/controller")
public class Controller extends HttpServlet {
    private static final String PATH_TO_CONFIG = "WEB-INF\\classes\\config\\log4j.xml";
    private static final Logger LOGGER = Logger.getLogger(Controller.class);
    private static final String ERROR_PAGE = PagesManager.getPage(PagesEnum.ERROR_404);
    private static final String ACTION = "action";
    private static final String METHOD = "method";
    private static final String MAIN_PAGE = "main_page";
    private ConnectionsPool pool = ConnectionsPool.getInstance();


    /**
     * <p>Initializing servlet, connection's pool and logger</p>
     *
     * @throws ServletException
     */
    @Override
    public void init() throws ServletException {
        super.init();
        StringBuilder path = new StringBuilder(getServletContext().getRealPath("/"));
        path.append(PATH_TO_CONFIG);
        new DOMConfigurator().doConfigure(path.toString(), LogManager.getLoggerRepository());
        pool.init();
        LOGGER.info(MessagesManager.getMessage(MessagesEnum.LOG_SERVLET_INIT));
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        processRequest(request, response);
    }

    /**
     * <p>Method for processing http request and generation http response</p>
     *
     * @param request  http request from user
     * @param response http response for user
     * @throws ServletException
     * @throws IOException
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        LOGGER.info(MessagesManager.getMessage(MessagesEnum.LOG_SERVLET_PROCESS_REQ_START));
        if (response.isCommitted()) {
            return;
        }
        String page;
        String actionParam = request.getParameter(ACTION);
        RequestContext context = new RequestContext();
        context.extractValues(request);
        ActionCommand command = ActionFactory.defineCommand(actionParam);
        page = command.execute(context);
        if (page == null) {
            response.sendError(404);
            return;
        }
        context.insertValues(request);
        processForward(request, response, page);
    }

    /**
     * <p>Method to choose how to redirect user to the result page</p>
     *
     * @param request  http request from user
     * @param response http response for user
     * @param page     page as result of processing request
     * @throws ServletException
     * @throws IOException
     */
    private void processForward(HttpServletRequest request, HttpServletResponse response, String page) throws
            ServletException, IOException {
        if (request.getParameter(ACTION).equals(MAIN_PAGE)) {
            request.getRequestDispatcher(page).forward(request, response);
            return;
        }
        MethodsEnum method = (MethodsEnum) request.getAttribute(METHOD);
        if (method == null) {
            request.getRequestDispatcher(ERROR_PAGE).forward(request, response);
            return;
        }
        LOGGER.info(MessagesManager.getMessage(MessagesEnum.LOG_SERVLET_PROCESS_REQ_END));
        if (method == MethodsEnum.REDIRECT) {
            response.sendRedirect(page);
            return;
        }
        if (method == MethodsEnum.FORWARD) {
            request.getRequestDispatcher(page).forward(request, response);
        }
    }

    @Override
    public void destroy() {
        super.destroy();
        pool.destroy();
        LOGGER.info(MessagesManager.getMessage(MessagesEnum.LOG_SERVLET_DESTROY));
    }
}

