package by.epam.shevelyov.hostel.comparator;

import by.epam.shevelyov.hostel.entity.impl.RoomType;

import java.util.Comparator;

/**
 * <p>Sorting roomTypes by amount</p>
 *
 * @author Ilya Shevelyov
 */
public class CheapestRoomTypeComparator implements Comparator<RoomType> {
    @Override
    public int compare(RoomType roomType1, RoomType roomType2) {
        return Double.compare(roomType1.getAmount(), roomType2.getAmount());
    }
}