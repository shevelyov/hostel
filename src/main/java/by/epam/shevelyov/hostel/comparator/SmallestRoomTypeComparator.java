package by.epam.shevelyov.hostel.comparator;

import by.epam.shevelyov.hostel.entity.impl.RoomType;

import java.util.Comparator;

/**
 * <p>Sorting roomTypes by capacity</p>
 *
 * @author Ilya Shevelyov
 */
public class SmallestRoomTypeComparator implements Comparator<RoomType> {
    @Override
    public int compare(RoomType roomType1, RoomType roomType2) {
        return Integer.compare(roomType1.getCapacity(), roomType2.getCapacity());
    }
}