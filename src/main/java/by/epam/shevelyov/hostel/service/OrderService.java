package by.epam.shevelyov.hostel.service;

import by.epam.shevelyov.hostel.dao.DAOEnum;
import by.epam.shevelyov.hostel.dao.BaseDAO;
import by.epam.shevelyov.hostel.dao.QueriesEnum;
import by.epam.shevelyov.hostel.dao.factory.BaseFactory;
import by.epam.shevelyov.hostel.dao.factory.DAOFactory;
import by.epam.shevelyov.hostel.entity.impl.Order;
import by.epam.shevelyov.hostel.entity.impl.Room;
import by.epam.shevelyov.hostel.entity.impl.RoomType;
import by.epam.shevelyov.hostel.manager.ErrorsEnum;
import by.epam.shevelyov.hostel.manager.ErrorsManager;
import by.epam.shevelyov.hostel.service.exception.ServiceException;
import org.apache.log4j.Logger;

import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

/**
 * <p>Class that provides methods for manipulate {@link Order} entities with chosen type of database</p>
 *
 * @author Ilya Shevelyov
 */
public class OrderService {
    private static final Logger LOGGER = Logger.getLogger(DiscountService.class);
    private static DAOFactory factory = BaseFactory.defineFactory(DAOEnum.MYSQL);
    private static BaseDAO<Order> orderDAO = factory.getOrderDAO();

    public static boolean addOrder(Order order) {
        boolean result = true;
        try {
            result = orderDAO.create(order);
            if (!result) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ORDER_CREATE));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static boolean deleteOrder(int id) {
        boolean result = false;
        try {
            result = orderDAO.delete(id);
            if (!result) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ORDER_DELETE));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static boolean updateOrder(Order order) {
        boolean result = false;
        try {
            result = orderDAO.update(order);
            if (!result) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ORDER_UPDATE));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static Order getOrderById(int id) {
        Order result = null;
        try {
            result = orderDAO.findEntityById(id);
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ORDER_FIND));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static List<Order> getAllOrders() {
        List<Order> result = null;
        try {
            result = orderDAO.findAll();
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ORDER_FIND_ALL));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static List<Order> getAllOrdersByUserId(int userId) {
        List<Order> result = null;
        try {
            result = orderDAO.execute(QueriesEnum.ORDER_BY_USER_ID, userId);
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ORDER_FIND));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static List<Order> getAllOrdersByRoomId(int roomId) {
        List<Order> result = null;
        try {
            result = orderDAO.execute(QueriesEnum.ORDER_BY_ROOM_ID, roomId);
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ORDER_FIND));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static List<Order> getAllOrdersByOrderStatus(int orderStatus) {
        List<Order> result = null;
        try {
            result = orderDAO.execute(QueriesEnum.ORDER_BY_ORDER_STATUS, orderStatus);
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ORDER_FIND));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static List<Order> getAllOrdersByCheckIn(Date checkIn) {
        List<Order> result = null;
        try {
            result = orderDAO.execute(QueriesEnum.ORDER_BY_DATE_CHECK_IN, new Date[]{checkIn});
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ORDER_FIND));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static List<Order> getAllOrdersByCheckOut(Date checkOut) {
        List<Order> result = null;
        try {
            result = orderDAO.execute(QueriesEnum.ORDER_BY_DATE_CHECK_OUT, new Date[]{checkOut});
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ORDER_FIND));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static List<Order> getAllOrdersByDatesAndRoomId(int roomId, String start, String end) {
        List<Order> result = null;
        try {
            result = orderDAO.execute(QueriesEnum.ORDER_BY_DATES_AND_ROOM_ID, new Object[]{roomId, start, end});
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ORDER_FIND));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static List<Order> getNotConfirmedOrders(int userId) {
        List<Order> result = null;
        try {
            result = orderDAO.execute(QueriesEnum.ORDER_NOT_CONFIRMED, userId);
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ORDER_FIND));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static List<Order> getConfirmedOrders(int userId) {
        List<Order> result = null;
        try {
            result = orderDAO.execute(QueriesEnum.ORDER_CONFIRMED, userId);
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ORDER_FIND));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static List<Order> getCancelledOrders(int userId) {
        List<Order> result = null;
        try {
            result = orderDAO.execute(QueriesEnum.ORDER_CANCELLED, userId);
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ORDER_FIND));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static Map<LocalDate, Integer> getFreePlacesByDatesAndRoomId(int roomId, String start, String end) {
        List<Order> orders = getAllOrdersByDatesAndRoomId(roomId, start, end);
        Room room = RoomService.getRoomById(roomId);
        RoomType roomType = room.getRoomType();
        TreeMap<LocalDate, Integer> map = new TreeMap<>();
        for (LocalDate date = LocalDate.parse(start); date.isBefore(LocalDate.parse(end)); date = date.plusDays(1)) {
            map.put(date, (roomType.getCapacity()));
        }
        for (Order order : orders) {
            LocalDate checkin = order.getCheckIn().toLocalDate();
            LocalDate checkout = order.getCheckOut().toLocalDate();
            for (LocalDate date = checkin; date.isBefore(checkout); date = date.plusDays(1)) {
                if (map.get(date) != null) {
                    map.put(date, map.get(date) - order.getPlaces());
                }
            }
        }
        return map;
    }
}
