package by.epam.shevelyov.hostel.service;

import by.epam.shevelyov.hostel.dao.DAOEnum;
import by.epam.shevelyov.hostel.dao.BaseDAO;
import by.epam.shevelyov.hostel.dao.QueriesEnum;
import by.epam.shevelyov.hostel.dao.factory.BaseFactory;
import by.epam.shevelyov.hostel.dao.factory.DAOFactory;
import by.epam.shevelyov.hostel.entity.impl.RoomType;
import by.epam.shevelyov.hostel.manager.ErrorsEnum;
import by.epam.shevelyov.hostel.manager.ErrorsManager;
import by.epam.shevelyov.hostel.service.exception.ServiceException;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * <p>Class that provides methods for manipulate {@link RoomType} entities with chosen type of database</p>
 *
 * @author Ilya Shevelyov
 */
public class RoomTypeService {
    private static final Logger LOGGER = Logger.getLogger(RoomTypeService.class);
    private static DAOFactory factory = BaseFactory.defineFactory(DAOEnum.MYSQL);
    private static BaseDAO<RoomType> roomTypeDAO = factory.getRoomTypeDAO();

    public static boolean addRoomType(RoomType roomType) {
        boolean result = true;
        try {
            result = roomTypeDAO.create(roomType);
            if (!result) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ROOM_TYPE_CREATE));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static RoomType getRoomTypeById(int id) {
        RoomType result = null;
        try {
            result = roomTypeDAO.findEntityById(id);
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ROOM_TYPE_FIND));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static List<RoomType> getAllRoomTypes() {
        List<RoomType> result = null;
        try {
            result = roomTypeDAO.findAll();
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ROOM_TYPE_FIND_ALL));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static boolean deleteRoomType(int id) {
        boolean result = false;
        try {
            result = roomTypeDAO.delete(id);
            if (!result) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ROOM_TYPE_DELETE));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static List<RoomType> getAllRoomTypesByCapacity(int capacity) {
        List<RoomType> result = null;
        try {
            result = roomTypeDAO.execute(QueriesEnum.ROOM_TYPE_BY_CAPACITY, capacity);
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ROOM_TYPE_UPDATE));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static List<RoomType> getRoomTypeByDescription(String description) {
        List<RoomType> result = null;
        try {
            result = roomTypeDAO.execute(QueriesEnum.ROOM_TYPE_BY_DESCRIPTION, new String[]{description});
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ROOM_TYPE_FIND));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static boolean updateRoomType(RoomType roomType) {
        boolean result = false;
        try {
            result = roomTypeDAO.update(roomType);
            if (!result) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ROOM_TYPE_UPDATE));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }
}
