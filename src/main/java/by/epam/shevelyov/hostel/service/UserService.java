package by.epam.shevelyov.hostel.service;

import by.epam.shevelyov.hostel.dao.BaseDAO;
import by.epam.shevelyov.hostel.dao.DAOEnum;
import by.epam.shevelyov.hostel.dao.QueriesEnum;
import by.epam.shevelyov.hostel.dao.factory.BaseFactory;
import by.epam.shevelyov.hostel.dao.factory.DAOFactory;
import by.epam.shevelyov.hostel.entity.impl.User;
import by.epam.shevelyov.hostel.manager.ErrorsEnum;
import by.epam.shevelyov.hostel.manager.ErrorsManager;
import by.epam.shevelyov.hostel.service.exception.ServiceException;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * <p>Class that provides methods for manipulate {@link User} entities with chosen type of database</p>
 *
 * @author Ilya Shevelyov
 */
public class UserService {
    private static final Logger LOGGER = Logger.getLogger(UserService.class);
    private static DAOFactory factory = BaseFactory.defineFactory(DAOEnum.MYSQL);
    private static BaseDAO<User> userDAO = factory.getUserDAO();

    public static boolean registerUser(User user) {
        boolean result = true;
        try {
            result = userDAO.create(user);
            if (!result) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_USER_CREATE));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static boolean delete(int id) {
        boolean result = false;
        try {
            result = userDAO.delete(id);
            if (!result) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_USER_DELETE));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static List<User> getAllUsers() {
        List<User> result = null;
        try {
            result = userDAO.findAll();
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_USER_FIND_ALL));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static User getUserById(int id) {
        User result = null;
        try {
            result = userDAO.findEntityById(id);
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_USER_FIND));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static boolean updateUser(User user) {
        boolean result = false;
        try {
            result = userDAO.update(user);
            if (!result) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_USER_UPDATE));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static User getUserByLogin(String login) {
        User result = null;
        try {
            List<User> list = userDAO.execute(QueriesEnum.USER_BY_LOGIN, new String[]{login});
            if (list == null || list.size() == 0) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_USER_FIND));
            }
            result = list.get(0);
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static User getUserByEmail(String email) {
        User result = null;
        try {
            List<User> list = userDAO.execute(QueriesEnum.USER_BY_EMAIL, new String[]{email});
            if (list == null || list.size() == 0) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_USER_FIND));
            }
            result = list.get(0);
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static List<User> getAllUsersByRole(String role) {
        List<User> result = null;
        try {
            result = userDAO.execute(QueriesEnum.USER_BY_ROLE, new String[]{role});
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_USER_FIND_ALL));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static List<User> getAllAdmins() {
        List<User> result = null;
        try {
            result = userDAO.execute(QueriesEnum.USER_BY_ROLE, new String[]{"Super"});
            result.addAll(userDAO.execute(QueriesEnum.USER_BY_ROLE, new String[]{"Admin"}));
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_USER_FIND_ALL));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }
}
