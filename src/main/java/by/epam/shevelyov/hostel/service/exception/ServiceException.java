package by.epam.shevelyov.hostel.service.exception;

/**
 * <p>Exception throws when error occurred in service methods</p>
 *
 * @author Ilya Shevelyov
 */
public class ServiceException extends Exception {
    public ServiceException() {
    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }
}
