package by.epam.shevelyov.hostel.service;

import by.epam.shevelyov.hostel.dao.DAOEnum;
import by.epam.shevelyov.hostel.dao.QueriesEnum;
import by.epam.shevelyov.hostel.dao.factory.DAOFactory;
import by.epam.shevelyov.hostel.dao.BaseDAO;
import by.epam.shevelyov.hostel.dao.factory.BaseFactory;
import by.epam.shevelyov.hostel.entity.impl.OrderStatus;
import by.epam.shevelyov.hostel.entity.impl.OrderStatus;
import by.epam.shevelyov.hostel.manager.ErrorsEnum;
import by.epam.shevelyov.hostel.manager.ErrorsManager;
import by.epam.shevelyov.hostel.service.exception.ServiceException;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * <p>Class that provides methods for manipulate {@link OrderStatus} entities with chosen type of database</p>
 *
 * @author Ilya Shevelyov
 */
public class OrderStatusService {
    private static final Logger LOGGER = Logger.getLogger(OrderStatusService.class);
    private static DAOFactory factory = BaseFactory.defineFactory(DAOEnum.MYSQL);
    private static BaseDAO<OrderStatus> orderStatusDAO = factory.getOrderStatusDAO();

    public static boolean addOrderStatus(OrderStatus orderStatus) {
        boolean result = true;
        try {
            result = orderStatusDAO.create(orderStatus);
            if (!result) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ORDER_STATUS_CREATE));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static OrderStatus getOrderStatusById(int id) {
        OrderStatus result = null;
        try {
            result = orderStatusDAO.findEntityById(id);
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ORDER_STATUS_FIND));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static List<OrderStatus> getAllOrderStatuses() {
        List<OrderStatus> result = null;
        try {
            result = orderStatusDAO.findAll();
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ORDER_STATUS_FIND_ALL));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static boolean deleteOrderStatus(int id) {
        boolean result = false;
        try {
            result = orderStatusDAO.delete(id);
            if (!result) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ORDER_STATUS_DELETE));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static boolean updateOrderStatus(OrderStatus orderStatus) {
        boolean result = false;
        try {
            result = orderStatusDAO.update(orderStatus);
            if (!result) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ORDER_STATUS_UPDATE));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static List<OrderStatus> getOrderStatusByStatus(String status) {
        List<OrderStatus> result = null;
        try {
            result = orderStatusDAO.execute(QueriesEnum.ORDER_STATUS_BY_STATUS, new String[]{status});
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ORDER_STATUS_FIND));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }
}
