package by.epam.shevelyov.hostel.service;

import by.epam.shevelyov.hostel.dao.DAOEnum;
import by.epam.shevelyov.hostel.dao.factory.DAOFactory;
import by.epam.shevelyov.hostel.dao.BaseDAO;
import by.epam.shevelyov.hostel.dao.QueriesEnum;
import by.epam.shevelyov.hostel.dao.factory.BaseFactory;
import by.epam.shevelyov.hostel.entity.impl.Room;
import by.epam.shevelyov.hostel.manager.ErrorsEnum;
import by.epam.shevelyov.hostel.manager.ErrorsManager;
import by.epam.shevelyov.hostel.service.exception.ServiceException;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * <p>Class that provides methods for manipulate {@link Room} entities with chosen type of database</p>
 *
 * @author Ilya Shevelyov
 */
public class RoomService {
    private static final Logger LOGGER = Logger.getLogger(RoomService.class);
    private static DAOFactory factory = BaseFactory.defineFactory(DAOEnum.MYSQL);
    private static BaseDAO<Room> roomDAO = factory.getRoomDAO();

    public static boolean addRoom(Room room) {
        boolean result = true;
        try {
            result = roomDAO.create(room);
            if (!result) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ROOM_CREATE));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static Room getRoomById(int id) {
        Room result = null;
        try {
            result = roomDAO.findEntityById(id);
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ROOM_FIND));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static List<Room> getAllRooms() {
        List<Room> result = null;
        try {
            result = roomDAO.findAll();
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ROOM_FIND_ALL));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static boolean deleteRoom(int id) {
        boolean result = false;
        try {
            result = roomDAO.delete(id);
            if (!result) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ROOM_DELETE));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static boolean updateRoom(Room room) {
        boolean result = false;
        try {
            result = roomDAO.update(room);
            if (!result) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ROOM_UPDATE));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static List<Room> getAllRoomsByRoomTypeId(int roomTypeId) {
        List<Room> result = null;
        try {
            result = roomDAO.execute(QueriesEnum.ROOM_BY_ROOM_TYPE_ID, roomTypeId);
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ROOM_FIND));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }
}
