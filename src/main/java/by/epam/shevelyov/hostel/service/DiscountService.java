package by.epam.shevelyov.hostel.service;

import by.epam.shevelyov.hostel.dao.DAOEnum;
import by.epam.shevelyov.hostel.dao.factory.BaseFactory;
import by.epam.shevelyov.hostel.dao.factory.DAOFactory;
import by.epam.shevelyov.hostel.dao.BaseDAO;
import by.epam.shevelyov.hostel.entity.impl.Discount;
import by.epam.shevelyov.hostel.manager.ErrorsEnum;
import by.epam.shevelyov.hostel.manager.ErrorsManager;
import by.epam.shevelyov.hostel.service.exception.ServiceException;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * <p>Class that provides methods for manipulate {@link Discount} entities with chosen type of database</p>
 *
 * @author Ilya Shevelyov
 */
public class DiscountService {
    private static final Logger LOGGER = Logger.getLogger(DiscountService.class);
    private static DAOFactory factory = BaseFactory.defineFactory(DAOEnum.MYSQL);
    private static BaseDAO<Discount> discountDAO = factory.getDiscountDAO();

    public static boolean addDiscount(Discount discount) {
        boolean result = true;
        try {
            result = discountDAO.create(discount);
            if (!result) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_DISCOUNT_CREATE));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static boolean deleteDiscount(int id) {
        boolean result = false;
        try {
            result = discountDAO.delete(id);
            if (!result) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_DISCOUNT_DELETE));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static boolean updateDiscount(Discount discount) {
        boolean result = false;
        try {
            result = discountDAO.update(discount);
            if (!result) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_DISCOUNT_UPDATE));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static Discount getDiscountById(int id) {
        Discount result = null;
        try {
        result = discountDAO.findEntityById(id);
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_DISCOUNT_FIND));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static List<Discount> getAllDiscounts() {
        List<Discount> result = null;
        try {
            result = discountDAO.findAll();
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_DISCOUNT_FIND_ALL));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }
}
