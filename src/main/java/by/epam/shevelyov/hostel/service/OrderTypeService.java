package by.epam.shevelyov.hostel.service;

import by.epam.shevelyov.hostel.dao.BaseDAO;
import by.epam.shevelyov.hostel.dao.DAOEnum;
import by.epam.shevelyov.hostel.dao.factory.BaseFactory;
import by.epam.shevelyov.hostel.dao.factory.DAOFactory;
import by.epam.shevelyov.hostel.entity.impl.OrderType;
import by.epam.shevelyov.hostel.manager.ErrorsEnum;
import by.epam.shevelyov.hostel.manager.ErrorsManager;
import by.epam.shevelyov.hostel.service.exception.ServiceException;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * <p>Class that provides methods for manipulate {@link OrderType} entities with chosen type of database</p>
 *
 * @author Ilya Shevelyov
 */
public class OrderTypeService {
    private static final Logger LOGGER = Logger.getLogger(OrderTypeService.class);
    private static DAOFactory factory = BaseFactory.defineFactory(DAOEnum.MYSQL);
    private static BaseDAO<OrderType> orderTypeDAO = factory.getOrderTypeDAO();

    public static boolean addOrderType(OrderType orderType) {
        boolean result = true;
        try {
            result = orderTypeDAO.create(orderType);
            if (!result) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ORDER_TYPE_CREATE));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static OrderType getOrderTypeById(int id) {
        OrderType result = null;
        try {
            result = orderTypeDAO.findEntityById(id);
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ORDER_TYPE_FIND));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static List<OrderType> getAllOrderTypes() {
        List<OrderType> result = null;
        try {
            result = orderTypeDAO.findAll();
            if (result == null) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ORDER_TYPE_FIND_ALL));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static boolean deleteOrderType(int id) {
        boolean result = false;
        try {
            result = orderTypeDAO.delete(id);
            if (!result) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ORDER_TYPE_DELETE));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    public static boolean updateOrderType(OrderType orderType) {
        boolean result = false;
        try {
            result = orderTypeDAO.update(orderType);
            if (!result) {
                throw new ServiceException(ErrorsManager.getMessage(ErrorsEnum.DAO_ORDER_TYPE_UPDATE));
            }
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }
}

