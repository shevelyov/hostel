package by.epam.shevelyov.hostel.entity;

/**
 * <p>Interface that unit all entities in the project</p>
 *
 * @author Ilya Shevelyov
 */
public interface Entity {
    int getId();
}
