package by.epam.shevelyov.hostel.entity.impl;

import by.epam.shevelyov.hostel.entity.Entity;

/**
 * <p>Class for the order-status-entity</p>
 *
 * @author Ilya Shevelyov
 */
public class OrderStatus implements Entity{
    private int id;
    private String status;

    public OrderStatus(int id, String status) {
        this.id = id;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrderStatus that = (OrderStatus) o;

        if (id != that.id) return false;
        return status.equals(that.status);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + status.hashCode();
        return result;
    }

}
