package by.epam.shevelyov.hostel.entity.util;

/**
 * <p>Enum that describes types of administrators in system</p>
 *
 * @author Ilya Shevelyov
 */
public enum RoleEnum {
    ADMIN("Admin"),
    SUPER("Super"),
    USER("User");

    String value;

    RoleEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
