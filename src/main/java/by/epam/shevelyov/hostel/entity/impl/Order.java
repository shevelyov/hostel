package by.epam.shevelyov.hostel.entity.impl;

import by.epam.shevelyov.hostel.entity.Entity;

import java.sql.Date;

/**
 * <p>Class for the order-entity</p>
 *
 * @author Ilya Shevelyov
 */
public class Order implements Entity {
    private int id;
    private User user;
    private int places;
    private Date registrationDate;
    private Date checkIn;
    private Date checkOut;
    private Room room;
    private OrderStatus orderStatus;
    private OrderType orderType;
    private Discount discount;
    private Double totalAmount;

    public Order(int id, User user, int places, Date registrationDate, Date checkIn, Date checkOut, Room room, OrderStatus orderStatus, OrderType orderType, Discount discount, Double totalAmount) {
        this.id = id;
        this.user = user;
        this.places = places;
        this.registrationDate = registrationDate;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.room = room;
        this.orderStatus = orderStatus;
        this.orderType = orderType;
        this.discount = discount;
        this.totalAmount = totalAmount;
    }

    public Order() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getPlaces() {
        return places;
    }

    public void setPlaces(int places) {
        this.places = places;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Date getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(Date checkIn) {
        this.checkIn = checkIn;
    }

    public Date getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(Date checkOut) {
        this.checkOut = checkOut;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public OrderType getOrderType() {
        return orderType;
    }

    public void setOrderType(OrderType orderType) {
        this.orderType = orderType;
    }

    public Discount getDiscount() {
        return discount;
    }

    public void setDiscount(Discount discount) {
        this.discount = discount;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Order order = (Order) o;

        if (id != order.id) return false;
        if (places != order.places) return false;
        if (!user.equals(order.user)) return false;
        if (!registrationDate.equals(order.registrationDate)) return false;
        if (!checkIn.equals(order.checkIn)) return false;
        if (!checkOut.equals(order.checkOut)) return false;
        if (!room.equals(order.room)) return false;
        if (orderStatus != order.orderStatus) return false;
        if (orderType != order.orderType) return false;
        if (discount != null ? !discount.equals(order.discount) : order.discount != null) return false;
        return totalAmount.equals(order.totalAmount);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + user.hashCode();
        result = 31 * result + places;
        result = 31 * result + registrationDate.hashCode();
        result = 31 * result + checkIn.hashCode();
        result = 31 * result + checkOut.hashCode();
        result = 31 * result + room.hashCode();
        result = 31 * result + orderStatus.hashCode();
        result = 31 * result + orderType.hashCode();
        result = 31 * result + (discount != null ? discount.hashCode() : 0);
        result = 31 * result + totalAmount.hashCode();
        return result;
    }
}
