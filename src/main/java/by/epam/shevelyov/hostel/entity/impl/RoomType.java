package by.epam.shevelyov.hostel.entity.impl;

import by.epam.shevelyov.hostel.entity.Entity;

/**
 * <p>Class for the room type-entity</p>
 *
 * @author Ilya Shevelyov
 */
public class RoomType implements Entity {
    private int id;
    private String description;
    private int capacity;
    private double amount;

    public RoomType(int id, String description, int capacity, double amount) {
        this.id = id;
        this.description = description;
        this.capacity = capacity;
        this.amount = amount;
    }

    public RoomType() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RoomType roomType = (RoomType) o;

        if (id != roomType.id) return false;
        if (capacity != roomType.capacity) return false;
        if (Double.compare(roomType.amount, amount) != 0) return false;
        return description.equals(roomType.description);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id;
        result = 31 * result + description.hashCode();
        result = 31 * result + capacity;
        temp = Double.doubleToLongBits(amount);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

}
