package by.epam.shevelyov.hostel.entity.impl;

import by.epam.shevelyov.hostel.entity.Entity;

import java.sql.Date;

/**
 * <p>Class for the ban-entity</p>
 *
 * @author Ilya Shevelyov
 */
public class Ban implements Entity {
    private int id;
    private User user;
    private User admin;
    private Date startDate;
    private Date endDate;
    private String reason;

    public Ban(int id, User user, User admin, Date startDate, Date endDate, String reason) {
        this.id = id;
        this.user = user;
        this.admin = admin;
        this.startDate = startDate;
        this.endDate = endDate;
        this.reason = reason;
    }

    public Ban(){}

    @Override
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getAdmin() {
        return admin;
    }

    public void setAdmin(User admin) {
        this.admin = admin;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ban ban = (Ban) o;

        if (id != ban.id) return false;
        if (!user.equals(ban.user)) return false;
        if (!admin.equals(ban.admin)) return false;
        if (!startDate.equals(ban.startDate)) return false;
        if (!endDate.equals(ban.endDate)) return false;
        return reason.equals(ban.reason);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + user.hashCode();
        result = 31 * result + admin.hashCode();
        result = 31 * result + startDate.hashCode();
        result = 31 * result + endDate.hashCode();
        result = 31 * result + reason.hashCode();
        return result;
    }
}
