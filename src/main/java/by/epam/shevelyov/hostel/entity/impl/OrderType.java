package by.epam.shevelyov.hostel.entity.impl;

import by.epam.shevelyov.hostel.entity.Entity;

/**
 * <p>Class for the order-type-entity</p>
 *
 * @author Ilya Shevelyov
 */
public class OrderType implements Entity {
    private int id;
    private String type;

    public OrderType(int id, String type) {
        this.id = id;
        this.type = type;
    }

    @Override
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrderType orderType = (OrderType) o;

        if (id != orderType.id) return false;
        return type.equals(orderType.type);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + type.hashCode();
        return result;
    }
}
