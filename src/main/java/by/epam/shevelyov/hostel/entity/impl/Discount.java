package by.epam.shevelyov.hostel.entity.impl;

import by.epam.shevelyov.hostel.entity.Entity;

/**
 * <p>Class for the discount-entity</p>
 *
 * @author Ilya Shevelyov
 */
public class Discount implements Entity {
    private int id;
    private String title;
    private int percentage;
    private String description;

    public Discount(int id, String title, int percentage, String description) {
        this.id = id;
        this.title = title;
        this.percentage = percentage;
        this.description = description;
    }

    public Discount(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPercentage() {
        return percentage;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Discount discount = (Discount) o;

        if (id != discount.id) return false;
        if (percentage != discount.percentage) return false;
        if (!title.equals(discount.title)) return false;
        return description.equals(discount.description);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + title.hashCode();
        result = 31 * result + percentage;
        result = 31 * result + description.hashCode();
        return result;
    }
}
