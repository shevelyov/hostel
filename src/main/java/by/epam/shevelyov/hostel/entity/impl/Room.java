package by.epam.shevelyov.hostel.entity.impl;

import by.epam.shevelyov.hostel.entity.Entity;

/**
 * <p>Class for the room-entity</p>
 *
 * @author Ilya Shevelyov
 */
public class Room implements Entity {
    private int id;
    private RoomType roomType;
    private String roomNumber;

    public Room(int id, RoomType roomType, String roomNumber) {
        this.id = id;
        this.roomType = roomType;
        this.roomNumber = roomNumber;
    }

    public Room() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public RoomType getRoomType() {
        return roomType;
    }

    public void setRoomType(RoomType roomType) {
        this.roomType = roomType;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Room room = (Room) o;

        if (id != room.id) return false;
        if (!roomType.equals(room.roomType)) return false;
        return roomNumber.equals(room.roomNumber);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + roomType.hashCode();
        result = 31 * result + roomNumber.hashCode();
        return result;
    }
}
