<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${lang ne null ? lang : pageContext.request.locale}" />
<fmt:setBundle basename="properties.content" var="content"/>
<fmt:setBundle basename="properties.hostel" var="info" />
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <title><fmt:message key="content.title.room.details"  bundle="${content}"/></title>
</head>
<body>
<jsp:useBean id="roomtype" class="by.epam.shevelyov.hostel.entity.impl.RoomType" scope="request" />

<div id="lang-modal-ru" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.ru" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="ru_RU">
                    <input type="hidden" name="requestPage" value="controller?action=roomtype_details&roomtype=${roomtype.id}">
                </div>
            </form>
        </div>
    </div>
</div>

<div id="lang-modal-en" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.en" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="en_EN">
                    <input type="hidden" name="requestPage" value="controller?action=roomtype_details&roomtype=${roomtype.id}">
                </div>
            </form>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <jsp:include page="common/header.jsp"/>
        </div>
    </div>
</div>
<br><br><br><br><br><br>
<div class="container">    
    <div class="row">
        <div class="col-md-6">
            <div class="right-info">
                <span class="room-info-title"><fmt:message key="room.${fn:toLowerCase(roomtype.description)}" bundle="${info}" /></span>
                <span class="price">${roomtype.amount}$</span>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="room-info-service">
                        <span class="glyphicon glyphicon-ok"></span>&nbsp;
                        <span class="head"><fmt:message key="content.room.info.bathroom" bundle="${content}" /></span>
                        <span class="tail"><fmt:message key="content.room.info.shared" bundle="${content}" /></span>
                    </div>
                    <div class="room-info-service">
                        <span class="glyphicon glyphicon-ok"></span>&nbsp;
                        <span class="head"><fmt:message key="content.room.info.max.guests" bundle="${content}" /></span>
                        <span class="tail">${roomtype.capacity}</span>
                    </div>
                    <div class="room-info-service">
                        <span class="glyphicon glyphicon-ok"></span>&nbsp;
                        <span class="head"><fmt:message key="content.room.info.common.room" bundle="${content}" /></span>
                        <span class="tail"><fmt:message key="content.room.info.yes" bundle="${content}" /></span>
                    </div>
                    <div class="room-info-service">
                        <span class="glyphicon glyphicon-ok"></span>&nbsp;
                        <span class="head"><fmt:message key="content.room.info.wifi" bundle="${content}" /></span>
                        <span class="tail"><fmt:message key="content.room.info.free" bundle="${content}" /></span>
                    </div>
                    <div class="room-info-service">
                        <span class="glyphicon glyphicon-ok"></span>&nbsp;
                        <span class="head"><fmt:message key="content.room.info.city.map" bundle="${content}" /></span>
                        <span class="tail"><fmt:message key="content.room.info.free" bundle="${content}" /></span>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="room-info-service">
                        <span class="glyphicon glyphicon-ok"></span>&nbsp;
                        <span class="head"><fmt:message key="content.room.info.bed.linnen" bundle="${content}" /></span>
                        <span class="tail"><fmt:message key="content.room.info.free" bundle="${content}" /></span>
                    </div>
                    <div class="room-info-service">
                        <span class="glyphicon glyphicon-ok"></span>&nbsp;
                        <span class="head"><fmt:message key="content.room.info.mini.supermarket" bundle="${content}" /></span>
                        <span class="tail"><fmt:message key="content.room.info.available" bundle="${content}" /></span>
                    </div>
                    <div class="room-info-service">
                        <span class="glyphicon glyphicon-ok"></span>&nbsp;
                        <span class="head"><fmt:message key="content.room.info.reception" bundle="${content}" /></span>
                        <span class="tail"><fmt:message key="content.room.info.24.hours" bundle="${content}" /></span>
                    </div>
                    <div class="room-info-service">
                        <span class="glyphicon glyphicon-ok"></span>&nbsp;
                        <span class="head"><fmt:message key="content.room.info.bicycle.hire" bundle="${content}" /></span>
                        <span class="tail"><fmt:message key="content.room.info.charge" bundle="${content}" /></span>
                    </div>
                    <div class="room-info-service">
                        <span class="glyphicon glyphicon-ok"></span>&nbsp;
                        <span class="head"><fmt:message key="content.room.info.lockers" bundle="${content}" /></span>
                        <span class="tail"><fmt:message key="content.room.info.charge" bundle="${content}" /></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <img class="room-info-image" src="images/rooms/${roomtype.capacity}.jpg" >
        </div>
    </div>
</div>
<br><br><br><br><br>
<br><br><br><br><br>
<jsp:include page="common/footer.jsp" />
</body>
</html>
