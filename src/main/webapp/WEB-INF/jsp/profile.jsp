<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${lang ne null ? lang : pageContext.request.locale}" />
<fmt:setBundle basename="properties.content" var="content"/>
<fmt:setBundle basename="properties.hostel" var="info" />
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <title><fmt:message key="content.title.profile" bundle="${content}"/></title>
</head>
<body>
<div id="lang-modal-ru" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.ru" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="ru_RU">
                    <input type="hidden" name="requestPage" value="controller?action=profile">
                </div>
            </form>
        </div>
    </div>
</div>

<div id="lang-modal-en" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.en" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="en_EN">
                    <input type="hidden" name="requestPage" value="controller?action=profile">
                </div>
            </form>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <jsp:include page="common/header.jsp"/>
        </div>
    </div>
</div>
<br><br><br>
<div class="container">
    <div class="jumbotron">
        <div class="row">
            <div class="col-md-8">
                <h1><fmt:message key="content.profile" bundle="${content}" /> ${user.login}</h1>
                <form action="controller" method="POST" accept-charset="UTF-8">
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-pencil"></span>
                        &nbsp; <fmt:message key="content.button.edit.personal.information" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="go_to_change_user_info">
                    <input type="hidden" name="user" value="${user.id}">
                </form>
            </div>
            <div class="col-md-4">
                <img src="images/photo_not_found.png" alt="Photo" class="img-responsive" height="200px" width="200px">
            </div>
        </div>
    </div>
    <hr>
</div>
<div class="container-fluid">
    <h2><fmt:message key="content.profile.orders" bundle="${content}" />:</h2>
    <div class="row">
        <div class="col-md-12">
            <c:choose>
                <c:when test="${orders eq null}">
                    <h4 class="bg-warning">
                        <fmt:message key="content.profile.null.orders" bundle="${content}" />
                    </h4>
                </c:when>
                <c:when test="${empty orders}">
                    <h4 class="bg-info">
                        <fmt:message key="content.profile.no.orders" bundle="${content}" />
                    </h4>
                </c:when>
                <c:otherwise>
                    <table class="table text-center">
                        <tr>
                            <th class="info text-center"><fmt:message key="content.table.room" bundle="${content}" /></th>
                            <th class="info text-center"><fmt:message key="content.table.checkin.date" bundle="${content}" /></th>
                            <th class="info text-center"><fmt:message key="content.table.checkout.date" bundle="${content}" /></th>
                            <th class="info text-center"><fmt:message key="content.table.guests" bundle="${content}" /></th>
                            <th class="info text-center"><fmt:message key="content.table.status" bundle="${content}" /></th>
                            <th class="info text-center"><fmt:message key="content.table.type" bundle="${content}" /></th>
                            <th class="info text-center"><fmt:message key="content.table.discount" bundle="${content}" /></th>
                            <th class="info text-center"><fmt:message key="content.table.price" bundle="${content}" /></th>
                            <th class="info text-center"></th>
                        </tr>
                        <c:forEach items="${orders}" var="elem" >
                            <tr <c:if test="${elem.orderStatus.status eq 'Cancelled'}">class="danger"</c:if>
                                <c:if test="${elem.orderStatus.status eq 'Notconfirmed'}">class="warning"</c:if>
                                <c:if test="${elem.orderStatus.status eq 'Confirmed'}">class="success"</c:if>
                                <c:if test="${elem.orderStatus.status eq 'Paid'}">class="info"</c:if>>
                                <td><a href="#room-modal-${elem.id}" data-toggle="modal">
                                    <fmt:message key="room.${fn:toLowerCase(elem.room.roomType.description)}" bundle="${info}" /></a></td>
                                <td><fmt:formatDate pattern="dd.MM.yyyy" value="${elem.checkIn}"/></td>
                                <td><fmt:formatDate pattern="dd.MM.yyyy" value="${elem.checkOut}"/></td>
                                <td>${elem.places}</td>
                                <td><fmt:message key="order.status.${fn:toLowerCase(elem.orderStatus.status)}" bundle="${info}" /></td>
                                <td><fmt:message key="order.type.${fn:toLowerCase(elem.orderType.type)}" bundle="${info}" /></td>
                                <td>${elem.discount.percentage}</td>
                                <td>${elem.totalAmount}</td>
                                <td>
                                    <c:if test="${elem.orderStatus.status eq 'Pending'}">
                                        <form action="controller" method="POST" accept-charset="UTF-8">
                                            <button type="submit" class="btn form-btn form-btn-red">
                                                <span class="glyphicon glyphicon-remove"></span>
                                                <fmt:message key="content.button.cancel" bundle="${content}" />
                                            </button>
                                            <input type="hidden" name="action" value="user_change_order_status">
                                            <input type="hidden" name="status" value="Cancelled">
                                            <input type="hidden" name="order" value="${elem.id}">
                                        </form>
                                    </c:if>
                                    <c:if test="${elem.orderStatus.status eq 'Confirmed' && elem.orderType.type eq 'Payment'}">
                                        <form action="controller" method="POST" accept-charset="UTF-8">
                                            <button type="submit" class="btn form-btn form-btn-info">
                                                <span class="glyphicon glyphicon-usd"></span>
                                                <fmt:message key="content.button.pay" bundle="${content}" />
                                            </button>
                                            <input type="hidden" name="action" value="user_change_order_status">
                                            <input type="hidden" name="status" value="Paid">
                                            <input type="hidden" name="order" value="${elem.id}">
                                        </form>
                                    </c:if>
                                </td>
                            </tr>
                            <div id="room-modal-${elem.id}" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h3 class="modal-title">
                                            <fmt:message key="content.modal.info.about.room" bundle="${content}" /></h3>
                                        </div>
                                        <div class="modal-body">
                                            <img src="images/rooms/${fn:toLowerCase(elem.room.roomType.capacity)}.jpg"
                                                 alt="Room image"
                                                 class="img img-responsive" width="100%">
                                            <br>
                                            <span> <fmt:message key="room.${fn:toLowerCase(elem.room.roomType.description)}" bundle="${info}"/></span>
                                        </div>
                                        <div class="modal-footer">
                                            <a class="btn form-btn"
                                               href="controller?action=roomtype_details&roomtype=${elem.room.roomType.id}">
                                                <fmt:message key="content.button.view.more" bundle="${content}" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </table>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>
<br><br><br><br><br>
<jsp:include page="common/footer.jsp"/>
</body>
</html>
