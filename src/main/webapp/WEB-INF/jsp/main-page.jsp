<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="user-tag" uri="/WEB-INF/tld/taglib.tld" %>
<fmt:setLocale value="${lang ne null ? lang : pageContext.request.locale}" />
<fmt:setBundle basename="properties.content" var="content"/>
<fmt:setBundle basename="properties.hostel" var="info" />
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <title><fmt:message key="content.title.main.page" bundle="${content}"/></title>
</head>
<body>
<div id="lang-modal-ru" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.ru" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="ru_RU">
                    <input type="hidden" name="requestPage" value="controller?action=main_page">
                </div>
            </form>
        </div>
    </div>
</div>

<div id="lang-modal-en" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.en" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="en_EN">
                    <input type="hidden" name="requestPage" value="controller?action=main_page">
                </div>
            </form>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <jsp:include page="common/header.jsp"/>
        </div>
    </div>
</div>
<jsp:include page="common/carousel.jsp" />
<div class="contents">
    <div class="row">
        <h3 class="text-center"><fmt:message key="content.room.best.rooms" bundle="${content}" /></h3>
        <div class="col-sm-4">
            <div class="room">
                <a href="controller?action=roomtype_details&roomtype=${roomtype1.id}"><img src="images/rooms/${roomtype1.capacity}.jpg" ></a>
                <h4 class="main-room text-center"><a class="room-title" href="controller?action=roomtype_details&roomtype=${roomtype1.id}">
                    <fmt:message key="room.${fn:toLowerCase(roomtype1.description)}" bundle="${info}" /></a></h4>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="room">
                <a href="controller?action=roomtype_details&roomtype=${roomtype2.id}"><img src="images/rooms/${roomtype2.capacity}.jpg" ></a>
                <h4 class="main-room text-center"><a class="room-title" href="controller?action=roomtype_details&roomtype=${roomtype2.id}">
                    <fmt:message key="room.${fn:toLowerCase(roomtype2.description)}" bundle="${info}" /></a></h4>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="room">
                <a href="controller?action=roomtype_details&roomtype=${roomtype3.id}"><img src="images/rooms/${roomtype3.capacity}.jpg"></a>
                <h4 class="main-room text-center"><a class="room-title" href="controller?action=roomtype_details&roomtype=${roomtype3.id}">
                    <fmt:message key="room.${fn:toLowerCase(roomtype3.description)}" bundle="${info}" /></a></h4>
            </div>
        </div>    
    </div>
</div>
<div class="container fluid full">
    <div class="row">
        <div class="container">
            <div class="col-sm-4 service-block">
                <img class="center-block" src="images/hostel-service-1.png">
                    <h4 class="text-center service-title"><fmt:message key="content.main.service.first" bundle="${content}" /></h4>
                    <p class="text-justify service"><fmt:message key="content.main.service.first.title" bundle="${content}" /></p>
            </div>
            <div class="col-sm-4 service-block">
                <img class="center-block" src="images/hostel-service-2.png">
                    <h4 class="text-center service-title"><fmt:message key="content.main.service.second" bundle="${content}" /></h4>
                    <p class="text-justify service"><fmt:message key="content.main.service.second.title" bundle="${content}" /></p>
            </div>
            <div class="col-sm-4 service-block">
                <img class="center-block" src="images/hostel-service-3.png">
                    <h4 class="text-center service-title"><fmt:message key="content.main.service.third" bundle="${content}" /></h4>
                    <p class="text-justify service"><fmt:message key="content.main.service.third.title" bundle="${content}" /></p>
            </div>
        </div>
    </div>
</div>
<div class="container fluid stat">
    <div class="row">
        <div class="col-sm-3">
            <h1 class="text-center"><fmt:message key="content.main.stat.guests.number" bundle="${content}"/></h1>
            <h2 class="text-center"><fmt:message key="content.main.stat.guests" bundle="${content}" /></h2>
        </div>
        <div class="col-sm-3">
            <h1 class="text-center"><fmt:message key="content.main.stat.opening.hours.number" bundle="${content}"/></h1>
            <h2 class="text-center"><fmt:message key="content.main.stat.opening.hours" bundle="${content}" /></h2>
        </div>
        <div class="col-sm-3">
            <h1 class="text-center"><fmt:message key="content.main.stat.satisfaction.number" bundle="${content}"/></h1>
            <h2 class="text-center"><fmt:message key="content.main.stat.satisfaction" bundle="${content}"/></h2>
        </div>
        <div class="col-sm-3">
            <h1 class="text-center"><fmt:message key="content.main.stat.come.back.number" bundle="${content}"/></h1>
            <h2 class="text-center"><fmt:message key="content.main.stat.come.back" bundle="${content}"/></h2>
        </div>
    </div>
</div>
<!-- <br><br><br><br><br> -->
<jsp:include page="common/footer.jsp" />
</body>
</html>