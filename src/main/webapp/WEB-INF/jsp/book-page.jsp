<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${lang ne null ? lang : pageContext.request.locale}" />
<fmt:setBundle basename="properties.content" var="content"/>
<fmt:setBundle basename="properties.hostel" var="info" />
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-datepicker3.min.css">
    <link rel="stylesheet" href="css/bootstrap-datepicker3.standalone.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="js/bootstrap-datepicker.min.js"></script>
    <title><fmt:message key="content.title.booking" bundle="${content}"/></title>
</head>
<body>
<div id="lang-modal-ru" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.ru" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="ru_RU">
                    <input type="hidden" name="requestPage" value="controller?action=go_to_book_page">
                </div>
            </form>
        </div>
    </div>
</div>
<div id="lang-modal-en" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.en" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="en_EN">
                    <input type="hidden" name="requestPage" value="controller?action=go_to_book_page">
                </div>
            </form>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <jsp:include page="common/header.jsp"/>
        </div>
    </div>
</div>
<div class="container text-center" id="bookcontainer">
    <form>
        <div class="col-sm-offset-4 col-sm-4">
                <h3><fmt:message key="content.book.hostel.booking.form" bundle="${content}"/></h3><br>
            <div class="form-group">
            <label><fmt:message key="content.book.room.type" bundle="${content}" /></label>
                <select name="roomtype" class="form-control text-center" id="roomtype" >
                    <c:forEach items="${roomtypes}" var="elem" >
                        <option value="${elem.id}"><fmt:message key="room.${fn:toLowerCase(elem.description)}" bundle="${info}" />        
                            <span id="amounts">/ ${elem.amount}</span> </option>
                    </c:forEach>
                </select>
            </div>
            <div class="form-group">
                <label><fmt:message key="content.book.guest.number" bundle="${content}" /></label>
                <input name="guests" type='number' id="guests" class="form-control" min="1" max="12" required />
            </div>
            <div class="form-group">
                <label><fmt:message key="content.book.date.checkin" bundle="${content}" /></label>
                <div class='input-group input-daterange'>
                    <input name="checkin" required type='text' class="form-control" id="checkin"/>
                    <span class="input-group-addon add-on">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label><fmt:message key="content.book.date.checkout" bundle="${content}" /></label>
                <div class='input-group input-daterange'>
                    <input name="checkout" required type='text' class="form-control" id="checkout"/>
                    <span class="input-group-addon add-on">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="form-group">
            <label><fmt:message key="content.book.order.type" bundle="${content}" /></label>
                <select name="ordertype" class="form-control text-center" id="ordertype" >
                    <c:forEach items="${ordertypes}" var="elem" >
                        <option value="${elem.id}"><fmt:message key="order.type.${fn:toLowerCase(elem.type)}" bundle="${info}" />        
                            </option>
                    </c:forEach>
                </select>
            </div>
            <div class="form-group">
                <button type="button" class="btn form-btn form-btn-blue btn-block" onclick="calculate()">
                    <span class="glyphicon glyphicon-scale"></span>
                    &nbsp; <fmt:message key="content.button.calculate" bundle="${content}" />
                </button>
            </div>
            <div class="form-group">
                <button type="submit" class="btn form-btn btn-block" formaction="controller?action=about_page">
                    <span class="glyphicon glyphicon-ok"></span>
                    &nbsp; <fmt:message key="content.button.book" bundle="${content}" />
                </button>
                <input type="hidden" name="action" value="book">
            </div>
            <div class="form-group" id="cost">
                <label id="calcform"><fmt:message key="content.book.total.cost" bundle="${content}" /></label>
                <span id="result">0</span>
            </div>
        </div>  
    </form>
</div>
<script type="text/javascript">
    $('.input-daterange').datepicker({
    format: 'yyyy-mm-dd',
    todayBtn: true,
    autoclose: true,
    todayHighlight: true
});
</script>



<script>
    function calculate(){
        var FILL_FIELD = "Fill all fields, please.";
        var WRONG_DATE = "Fill right date, please."; 
        var cost = document.getElementById("cost");
        var result = document.getElementById("result");
        var calcform = document.getElementById("calcform");
        var res = document.getElementById("calcform");
        var roomtype = document.getElementById("roomtype");
        var mass = roomtype.options[roomtype.selectedIndex].text.split(" ");
        var amount = parseInt(mass[mass.length-1]);
        var guests = document.getElementById("guests").value;
        var ordertype = document.getElementById("ordertype");
        var checkin = new Date(document.getElementById("checkin").value);
        var checkout = new Date(document.getElementById("checkout").value);
        var utc1 = Date.UTC(checkin.getFullYear(), checkin.getMonth(), checkin.getDate());
        var utc2 = Date.UTC(checkout.getFullYear(), checkout.getMonth(), checkout.getDate());  
        var today = new Date();  
        var days = Math.floor((utc2-utc1)/(1000*3600*24));
        if(!guests||!checkin||!checkout||!roomtype||!ordertype){
            cost.style.visibility="visible";
            cost.style.backgroundColor = "red";
            calcform.innerHTML = "";
            result.innerHTML = FILL_FIELD;
        }
        else{
            if(days<0||utc1<today){
            cost.style.visibility="visible";
            cost.style.backgroundColor = "red";
            calcform.innerHTML = "";
            result.innerHTML = WRONG_DATE;
            }
            else{
                calcform = res;
                result.innerHTML = amount*days*parseInt(guests);
                cost.style.visibility="visible";
                cost.style.backgroundColor = "#3C82E7";
            }
        }
    }
</script>
<br><br><br><br><br>
<br><br><br><br><br>
<jsp:include page="common/footer.jsp"/>
</body>
</html>
