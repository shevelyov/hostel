<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="user-tag" uri="/WEB-INF/tld/taglib.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${lang ne null ? lang : pageContext.request.locale}" />
<fmt:setBundle basename="properties.content" var="content"/>
<fmt:setBundle basename="properties.hostel" var="info" />
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <title><fmt:message key="content.title.service" bundle="${content}"/> </title>
</head>
<body>

<div id="lang-modal-ru" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.ru" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="ru_RU">
                    <input type="hidden" name="requestPage" value="controller?action=service_page">
                </div>
            </form>
        </div>
    </div>
</div>

<div id="lang-modal-en" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.en" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="en_EN">
                    <input type="hidden" name="requestPage" value="controller?action=service_page">
                </div>
            </form>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <jsp:include page="common/header.jsp"/>
        </div>
    </div>
</div>
<br><br><br>
<div class="container-fluid">
    <div class="jumbotron">
        <h2 class="text-center"><fmt:message key="content.header.services" bundle="${content}" /></h2>
    </div>
</div>
<hr>
<div class="container">
    <div class="row">
        <div class="col-sm-4">
            <div class="icon"><i class="fa fa-wifi fa-3x"></i></div>
            <h4 class="text-center"><fmt:message key="content.service.free.wifi" bundle="${content}" /></h4>
        </div>
        <div class="col-sm-4">
            <div class="icon"><i class="fa fa-star fa-3x"></i></div>
            <h4 class="text-center"><fmt:message key="content.service.free.linnen" bundle="${content}" /></h4>
        </div>
        <div class="col-sm-4">
            <div class="icon"><i class="fa fa-tint fa-3x"></i></div>
            <h4 class="text-center"><fmt:message key="content.service.free.shower" bundle="${content}" /></h4>
        </div>
        <div class="col-sm-4">
            <div class="icon"><i class="fa fa-map-marker fa-3x"></i></div>
            <h4 class="text-center"><fmt:message key="content.service.free.city.map" bundle="${content}" /></h4>
        </div>
        <div class="col-sm-4">
            <div class="icon"><i class="fa fa-shopping-cart fa-3x"></i></div>
            <h4 class="text-center"><fmt:message key="content.service.mini.supermarket" bundle="${content}" /></h4>
        </div>
        <div class="col-sm-4">
            <div class="icon"><i class="fa fa-video-camera fa-3x"></i></div>
            <h4 class="text-center"><fmt:message key="content.service.security" bundle="${content}" /></h4>
        </div>
        <div class="col-sm-4">
            <div class="icon"><i class="fa fa-key fa-3x"></i></div>
            <h4 class="text-center"><fmt:message key="content.service.lockers" bundle="${content}" /></h4>
        </div>
        <div class="col-sm-4">
            <div class="icon"><i class="fa fa-users fa-3x"></i></div>
            <h4 class="text-center"><fmt:message key="content.service.free.walking.tour" bundle="${content}" /></h4>
        </div>
        <div class="col-sm-4">
            <div class="icon"><i class="fa fa-bicycle fa-3x"></i></div>
            <h4 class="text-center"><fmt:message key="content.service.bicycle.hire" bundle="${content}" /></h4>
        </div>
    </div>
</div>
<hr>
<br><br><br><br><br>
<jsp:include page="common/footer.jsp" />
</body>
</html>