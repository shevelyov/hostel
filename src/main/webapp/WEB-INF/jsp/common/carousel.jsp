<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${lang ne null ? lang : pageContext.request.locale}" />
<fmt:setBundle basename="properties.content" var="content"/>
<fmt:setBundle basename="properties.agency" var="info" />
<jsp:useBean id="user" class="by.epam.shevelyov.hostel.entity.impl.User" scope="session"/>
<!-- Carousel -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <a href=""><img id="carousel" class="img-responsive center-block" src="images/slider1.jpg" alt="First slide"></a>
            <div class="container">
                <div class="carousel-caption">
                    <h1 class="carousel-h1"><fmt:message key="content.carousel.welcome" bundle="${content}" /></h1>
                    <br>
                    <c:if test="${user.role.value eq 'User'}">
                        <p><a class="btn btn-lg btn-primary btn-carousel" href="controller?action=go_to_book_page" role="button">
                            <fmt:message key="content.carousel.book.now" bundle="${content}" />
                        </a></p>
                    </c:if>
                </div>
            </div>
        </div>
        <div class="item">
            <a href=""><img id="carousel" class="img-responsive center-block" src="images/slider2.jpg" alt="Second slide"></a>
            <div class="container">
                <div class="carousel-caption">
                    <h1 class="carousel-h1"><fmt:message key="content.carousel.welcome" bundle="${content}" /></h1>
                    <br>
                    <c:if test="${user.role.value eq 'User'}">
                        <p><a class="btn btn-lg btn-primary btn-carousel" href="controller?action=go_to_book_page" role="button">
                            <fmt:message key="content.carousel.book.now" bundle="${content}" />
                        </a></p>
                    </c:if>
                </div>
            </div>
        </div>
        <div class="item">
            <a href=""><img id="carousel" class="img-responsive center-block" src="images/slider3.jpg" alt="Third slide"></a>
            <div class="container">
                <div class="carousel-caption">
                    <h1 class="carousel-h1"><fmt:message key="content.carousel.welcome" bundle="${content}" /></h1>
                    <br>
                    <c:if test="${user.role.value eq 'User'}">
                        <p><a class="btn btn-lg btn-primary btn-carousel" href="controller?action=go_to_book_page" role="button">
                            <fmt:message key="content.carousel.book.now" bundle="${content}" />
                        </a></p>
                    </c:if>
                </div>
            </div>
        </div>
    </div>
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>