<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="user-tag" uri="/WEB-INF/tld/taglib.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${lang ne null ? lang : pageContext.request.locale}" />
<fmt:setBundle basename="properties.content" var="content"/>
<footer id="footer" class="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <p><strong><span class="glyphicon glyphicon-road"></span>&nbsp;
                    <fmt:message key="content.footer.addresses" bundle="${content}" />
                </strong></p>
                <p><fmt:message key="content.footer.address.first" bundle="${content}" /></p>
                <p><fmt:message key="content.footer.address.second" bundle="${content}" /></p>
            </div>
            <div class="col-sm-4">
                <p><strong><span class="glyphicon glyphicon-earphone"></span>&nbsp;
                    <fmt:message key="content.footer.phones" bundle="${content}" />
                </strong></p>
                <p>+375 (29) 397-81-53</p>
                <p>+375 (29) 397-81-53</p>
                <p>+375 (17) 201-08-10</p>
            </div>
            <div class="col-sm-4">
                <p><strong><span class="glyphicon glyphicon-thumbs-up"></span>&nbsp;
                    <fmt:message key="content.footer.social.networks" bundle="${content}" />
                </strong></p>
                <a href="http://vk.com"><fmt:message key="content.footer.vkontakte" bundle="${content}" /></a><br>
                <a href="http://twitter.com/"><fmt:message key="content.footer.twitter" bundle="${content}" /></a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-offset-4 col-sm-4">
                <p class="text-center copyright"><user-tag:copyright email="ilya.shevelyov@gmail.com"/></p>
            </div>
        </div>
    </div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>


