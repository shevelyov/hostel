<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="user-tag" uri="/WEB-INF/tld/taglib.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${lang ne null ? lang : pageContext.request.locale}" />
<fmt:setBundle basename="properties.content" var="content"/>
<fmt:setBundle basename="properties.hostel" var="info" />
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <title><fmt:message key="content.title.rooms" bundle="${content}"/> </title>
</head>
<body>

<div id="lang-modal-ru" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.ru" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="ru_RU">
                    <c:choose>
                        <c:when test="${action eq 'cheapest_room'}">
                            <input type="hidden" name="requestPage"
                                   value="controller?action=cheapest_room&page=${page}">
                        </c:when>
                        <c:when test="${action eq 'most_expensive_room'}">
                            <input type="hidden" name="requestPage"
                                   value="controller?action=most_expensive_room&page=${page}">
                        </c:when>
                        <c:when test="${action eq 'smallest_room'}">
                            <input type="hidden" name="requestPage"
                                   value="controller?action=smallest_room&page=${page}">
                        </c:when>
                        <c:when test="${action eq 'biggest_room'}">
                            <input type="hidden" name="requestPage"
                                   value="controller?action=biggest_room&page=${page}">
                        </c:when>
                        <c:otherwise>
                            <input type="hidden" name="requestPage"
                                   value="controller?action=cheapest_room&page=${page}">
                        </c:otherwise>
                    </c:choose>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="lang-modal-en" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.en" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="en_EN">
                    <c:choose>
                        <c:when test="${action eq 'cheapest_room'}">
                            <input type="hidden" name="requestPage"
                                   value="controller?action=cheapest_room&page=${page}">
                        </c:when>
                        <c:when test="${action eq 'most_expensive_room'}">
                            <input type="hidden" name="requestPage"
                                   value="controller?action=most_expensive_room&page=${page}">
                        </c:when>
                        <c:when test="${action eq 'smallest_room'}">
                            <input type="hidden" name="requestPage"
                                   value="controller?action=smallest_room&page=${page}">
                        </c:when>
                        <c:when test="${action eq 'biggest_room'}">
                            <input type="hidden" name="requestPage"
                                   value="controller?action=biggest_room&page=${page}">
                        </c:when>
                        <c:otherwise>
                            <input type="hidden" name="requestPage"
                                   value="controller?action=cheapest_room&page=${page}">
                        </c:otherwise>    
                    </c:choose>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <jsp:include page="common/header.jsp"/>
        </div>
    </div>
</div>
<br><br><br>
<div class="container-fluid">
    <div class="jumbotron">
        <h2 class="text-center"><fmt:message key="content.header.rooms" bundle="${content}" /></h2>
    </div>
    <hr>
</div>
<div class="container-fluid">    
    <div class="row">
        <div class="col-md-12 text-center">
            <div id="sort-item-title" class="sort-item ">
                <fmt:message key="content.room.sort.by" bundle="${content}" />
            </div>
            <div id="sort-item-lowest" class = "sort-item menu-title <c:if test="${action eq 'cheapest_room'}"> menu-title-active</c:if>">
                <a href="controller?action=cheapest_room&page=1">
                    <fmt:message key="content.room.lowest.price" bundle="${content}" />
                </a>
            </div>
            <div id="sort-item-highest" class = "sort-item menu-title <c:if test="${action eq 'most_expensive_room'}"> menu-title-active</c:if>">
                <a href="controller?action=most_expensive_room&page=1">
                    <fmt:message key="content.room.highest.price" bundle="${content}" />
                </a>
            </div>
            <div id="sort-item-biggest" class = "sort-item menu-title <c:if test="${action eq 'biggest_room'}"> menu-title-active</c:if>">
                <a href="controller?action=biggest_room&page=1">
                    <fmt:message key="content.room.biggest.room" bundle="${content}" />
                </a>
            </div>
            <div id="sort-item-smallest" class = "sort-item menu-title <c:if test="${action eq 'smallest_room'}"> menu-title-active</c:if>">
                <a href="controller?action=smallest_room&page=1">
                    <fmt:message key="content.room.smallest.room" bundle="${content}" />
                </a>
            </div>
        </div>
    </div>
    <hr class="menu-hr">
    <br><br><br>
</div>
<div class="container-fluid">
    <div class="row row-catalog">
        <c:choose>
            <c:when test="${roomtypes eq null or empty roomtypes}">
                <div class="col-md-4 col-md-offset-4">
                    <div class="alert alert-info fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <fmt:message key="content.no.rooms" bundle="${content}" />
                    </div>
                </div>
            </c:when>
            <c:otherwise>
                <c:forEach items="${roomtypes}" var="elem">
                    <div class="col-sm-4">
                        <div class="room">
                            <a href="controller?action=roomtype_details&roomtype=${elem.id}"><img src="images/rooms/${elem.capacity}.jpg" ></a>
                            <h4 class="text-center"><a class="room-title" href="controller?action=roomtype_details&roomtype=${elem.id}"><fmt:message key="room.${fn:toLowerCase(elem.description)}" bundle="${info}" /></a></h4>
                        </div>
                    </div>
                </c:forEach>
            </c:otherwise>
        </c:choose>
    </div>
    <c:if test="${not empty pagesCount and pagesCount gt 1}">
        <br>
        <div class="row">
            <div class="col-md-12 text-center">
                <ul class="pagination pagination-lg ">
                    <c:forEach begin="1" end="${pagesCount}" var="num">
                        <li <c:if test="${page eq num}">class="active"</c:if>>
                            <c:choose>
                                <c:when test="${action eq 'cheapest_room'}">
                                    <a href="controller?action=cheapest_room&page=${num}">${num}</a></li>
                                </c:when>
                                <c:when test="${action eq 'most_expensive_room'}">
                                    <a href="controller?action=most_expensive_room&page=${num}">${num}</a></li>
                                </c:when>
                                <c:when test="${action eq 'smallest_room'}">
                                    <a href="controller?action=smallest_room&page=${num}">${num}</a></li>
                                </c:when>
                                <c:when test="${action eq 'biggest_room'}">
                                    <a href="controller?action=biggest_room&page=${num}">${num}</a></li>
                                </c:when>
                                <c:otherwise>
                                    <a href="controller?action=cheapest_room&page=${num}">${num}</a></li>
                                </c:otherwise>
                            </c:choose>
                    </c:forEach>
                </ul>
            </div>
        </div>
    </c:if>
</div>
<hr>
<br><br><br><br><br>
<jsp:include page="common/footer.jsp" />
</body>
</html>