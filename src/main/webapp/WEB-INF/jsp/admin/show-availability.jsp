<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<fmt:setLocale value="${lang ne null ? lang : pageContext.request.locale}" />
<fmt:setBundle basename="properties.content" var="content" />
<fmt:setBundle basename="properties.hostel" var="info" />
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <title><fmt:message key="content.title.room.availability" bundle="${content}"/></title>
</head>
<body>
<div id="lang-modal-ru" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.ru" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="ru_RU">
                    <input type="hidden" name="requestPage" value="controller?action=go_to_choose_period">
                </div>
            </form>
        </div>
    </div>
</div>

<div id="lang-modal-en" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.en" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="en_EN">
                    <input type="hidden" name="requestPage" value="controller?action=go_to_choose_period">
                </div>
            </form>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <jsp:include page="../common/header.jsp"/>
        </div>
    </div>
</div>
<br><br><br><br><br>
<div class="container-fluid">
    <h2><fmt:message key="content.title.room.availability" bundle="${content}" />:</h2>
    <a href="controller?action=go_to_choose_period" class="btn form-btn">
        <span class="glyphicon glyphicon-chevron-left"></span>
        &nbsp; <fmt:message key="content.button.back" bundle="${content}" />
    </a>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-hover text-center">
                <tr>
                    <th class="info"><fmt:message key="content.table.date" bundle="${content}" /></th>
                    <c:forEach var="room" items="${map}">
                        <th class="info"><fmt:message key="room.${fn:toLowerCase(room.key)}" bundle="${info}" /></th>
                    </c:forEach>
                </tr>
                <c:forEach var="date" items="${dates}" varStatus="status">
                    <tr>
                        <td class="info">${date}</td>
                        <c:forEach var="entry" items="${map}">
                            <td <c:if test="${entry.value[status.index] le 0}">class="danger"</c:if>
                                <c:if test="${entry.value[status.index] eq 1}">class="warning"</c:if>
                                <c:if test="${entry.value[status.index] gt 1}">class="success"</c:if>
                                >${entry.value[status.index]}</td>
                        </c:forEach>
                     </tr>
                </c:forEach>    
            </table>
        </div>
    </div>
</div>
<br><br><br><br><br>
<br><br><br><br><br>
<jsp:include page="../common/footer.jsp" />
</body>
</html>
