<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<fmt:setLocale value="${lang ne null ? lang : pageContext.request.locale}" />
<fmt:setBundle basename="properties.content" var="content" />
<fmt:setBundle basename="properties.hostel" var="info" />
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <title><fmt:message key="content.title.all.users" bundle="${content}"/></title>
</head>
<body>
<div id="lang-modal-ru" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.ru" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="ru_RU">
                    <input type="hidden" name="requestPage" value="controller?action=all_users&page=1">
                </div>
            </form>
        </div>
    </div>
</div>

<div id="lang-modal-en" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.en" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="en_EN">
                    <input type="hidden" name="requestPage" value="controller?action=all_users&page=1">
                </div>
            </form>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <jsp:include page="../common/header.jsp"/>
        </div>
    </div>
</div>
<br><br><br><br><br>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 text-center">
            <div id="menu-item-rooms" class="col-md-2 menu-title">
                <a href="controller?action=all_rooms&page=1">
                    <fmt:message key="content.menu.rooms" bundle="${content}" />
                </a>
            </div>
            <div id="menu-item-orders" class="col-md-2 menu-title">
                <a href="controller?action=all_orders&page=1">
                    <fmt:message key="content.menu.orders" bundle="${content}" />
                </a>
            </div>
            <div id="menu-item-users" class="col-md-2 menu-title menu-title-active">
                <a href="controller?action=all_users&page=1">
                    <fmt:message key="content.menu.users" bundle="${content}" />
                </a>
            </div>
            <div id="menu-item-administrators" class="col-md-2 menu-title">
                <a href="controller?action=all_administrators&page=1">
                    <fmt:message key="content.menu.administrators" bundle="${content}" />
                </a>
            </div>
            <div id="menu-item-discounts" class="col-md-2 menu-title">
                <a href="controller?action=all_discounts&page=1">
                    <fmt:message key="content.menu.discounts" bundle="${content}" />
                </a>
            </div>
            <c:if test="${user.role.value eq 'Admin'}">
                <div id="menu-item-bans" class="col-md-2 menu-title">
                    <a href="controller?action=all_bans&page=1">
                        <fmt:message key="content.menu.bans" bundle="${content}" />
                    </a>
                </div>
            </c:if>
            <c:if test="${user.role.value eq 'Super'}">
            <div id="menu-item-roomtypes" class="col-md-2 menu-title">
                <a href="controller?action=all_roomtypes&page=1">
                    <fmt:message key="content.menu.roomtypes" bundle="${content}" />
                </a>
            </div>
            </c:if>
        </div>
    </div>
    <hr class="menu-hr">
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h2><fmt:message key="content.menu.all.users" bundle="${content}" /></h2>
            <br>
            <c:choose>
                <c:when test="${users eq null}">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="alert alert-danger fade in">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <fmt:message key="content.null.users" bundle="${content}" />.
                        </div>
                    </div>
                </c:when>
                <c:when test="${empty users}">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="alert alert-info fade in">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <fmt:message key="content.no.users" bundle="${content}" />.
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <table class="table table-hover text-center">
                        <tr>
                            <th class="info"><fmt:message key="content.table.id" bundle="${content}" /></th>
                            <th class="info"><fmt:message key="content.table.first.name" bundle="${content}" /></th>
                            <th class="info"><fmt:message key="content.table.last.name" bundle="${content}" /></th>
                            <th class="info"><fmt:message key="content.table.login" bundle="${content}" /></th>
                            <th class="info"><fmt:message key="content.table.user.phone" bundle="${content}" /></th>
                            <th class="info"><fmt:message key="content.table.email" bundle="${content}" /></th>
                            <th class="info"></th>
                            <th class="info"></th>
                        </tr>
                        <c:forEach var="elem" items="${users}">
                            <tr>
                                <td>${elem.id}</td>
                                <td>${elem.firstName}</td>
                                <td>${elem.lastName}</td>
                                <td>${elem.login}</td>
                                <td>${elem.phoneNumber}</td>
                                <td>${elem.email}</td>
                                <td><a href="controller?action=user_details&user=${elem.id}" class="btn form-btn form-btn-info">
                                    <span class="glyphicon glyphicon-cog"></span>
                                    &nbsp; <fmt:message key="content.button.details" bundle="${content}" />
                                </a></td>
                                <td>
                                    <form action="controller" method="POST" accept-charset="UTF-8">
                                        <button type="submit" class="btn form-btn form-btn-red">
                                            <span class="glyphicon glyphicon-remove"></span>
                                            &nbsp; <fmt:message key="content.button.ban" bundle="${content}" />
                                        </button>
                                        <input type="hidden" name="action" value="go_to_add_ban">
                                        <input type="hidden" name="user" value="${elem.id}">
                                    </form>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
    <c:if test="${not empty pagesCount and pagesCount gt 1}">
        <br>
        <div class="row">
            <div class="col-md-12 text-center">
                <ul class="pagination pagination-lg ">
                    <c:forEach begin="1" end="${pagesCount}" var="num">
                        <li <c:if test="${page eq num}">class="active"</c:if>>
                            <a href="controller?action=all_users&page=${num}">${num}</a></li>
                    </c:forEach>
                </ul>
            </div>
        </div>
    </c:if>
</div>
<br><br><br><br><br>
<br><br><br><br><br>
<jsp:include page="../common/footer.jsp" />
</body>
</html>
