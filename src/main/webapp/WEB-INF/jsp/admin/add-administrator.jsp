<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${lang ne null ? lang : pageContext.request.locale}" />
<fmt:setBundle basename="properties.content" var="content" />
<fmt:setBundle basename="properties.hostel" var="info" />
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <title><fmt:message key="content.title.add.administrator" bundle="${content}"/></title>
</head>
<body>

<div id="lang-modal-ru" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.ru" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="ru_RU">
                    <input type="hidden" name="requestPage" value="controller?action=go_to_add_administrator">
                </div>
            </form>
        </div>
    </div>
</div>

<div id="lang-modal-en" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.en" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="en_EN">
                    <input type="hidden" name="requestPage" value="controller?action=go_to_add_administrator">
                </div>
            </form>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <jsp:include page="../common/header.jsp"/>
        </div>
    </div>
    <br><br><br><br><br>
    <div class="container">
        <div class="row">
            <h2><fmt:message key="content.title.add.administrator" bundle="${content}" />:</h2>
            <a href="controller?action=all_administrators&page=1" class="btn form-btn">
                <span class="glyphicon glyphicon-chevron-left"></span>
                &nbsp; <fmt:message key="content.menu.all.administrators" bundle="${content}" />
            </a>
            <hr>
            <div class="col-md-6">
                <form role="form" action="controller" method="POST">
                    <div class="form-group">
                        <label for="login"><fmt:message key="content.table.administrator.login" bundle="${content}" />:</label>
                        <c:set var="enterLogin" scope="page">
                            <fmt:message key="content.form.enter.login" bundle="${content}" />
                        </c:set>
                        <input type="text" name="login" class="form-control form-input" id="login"
                               placeholder="${enterLogin}" pattern="^[a-zA-Z](.[\w9_-]*){3,20}$" required>
                    </div>
                    <div class="form-group">
                        <label for="password"><fmt:message key="content.table.password" bundle="${content}" />:</label>
                        <c:set var="enterPassword" scope="page">
                            <fmt:message key="content.form.enter.password" bundle="${content}" />
                        </c:set>
                        <input type="password" name="password" class="form-control form-input" id="password"
                               placeholder="${enterPassword}" pattern="^[\w]{6,30}$" required>
                    </div>
                    <div class="form-group">
                        <label for="repeatPassword"><fmt:message key="content.table.repeat.password" bundle="${content}" />:</label>
                        <c:set var="repeatPassword" scope="page">
                            <fmt:message key="content.form.repeat.password" bundle="${content}" />
                        </c:set>
                        <input type="password" name="repeatPassword" class="form-control form-input" id="repeatPassword"
                               placeholder="${repeatPassword}" pattern="^[\w]{6,30}$" required>
                    </div>
                    <div class="form-group">
                        <label for="firstname"><fmt:message key="content.table.first.name" bundle="${content}" />:</label>
                        <c:set var="enterFirstName" scope="page">
                            <fmt:message key="content.form.enter.first.name" bundle="${content}" />
                        </c:set>
                        <input type="text" name="firstname" class="form-control form-input" id="firstname"
                               placeholder="${enterFirstName}" pattern="^[a-zA-Z ,.'-]+$" required>
                    </div>
                    <div class="form-group">
                        <label for="lastname"><fmt:message key="content.table.last.name" bundle="${content}" />:</label>
                        <c:set var="enterLastName" scope="page">
                            <fmt:message key="content.form.enter.last.name" bundle="${content}" />
                        </c:set>
                        <input type="text" name="lastname" class="form-control form-input" id="lastname"
                               placeholder="${enterLastName}" pattern="^[a-zA-Z ,.'-]+$" required>
                    </div>
                    <div class="form-group">
                        <label for="email"><fmt:message key="content.table.email" bundle="${content}" />:</label>
                        <c:set var="enterEmail" scope="page">
                            <fmt:message key="content.form.enter.email" bundle="${content}" />
                        </c:set>
                        <input type="text" name="email" class="form-control form-input" id="email"
                               placeholder="${enterEmail}" pattern="^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$" required>
                    </div>
                    <div class="form-group">
                        <label for="phone"><fmt:message key="content.table.user.phone" bundle="${content}" />:</label>
                        <c:set var="enterPhone" scope="page">
                            <fmt:message key="content.form.enter.phone" bundle="${content}" />
                        </c:set>
                        <input type="text" name="phone" class="form-control form-input" id="phone"
                               placeholder="${enterPhone}" pattern="^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{2,3})[-. )]*(\d{3})[-. ]*(\d{2})[-. ]*(\d{2,5})?\s*$" required>
                    </div>
                    <div class="form-group">
                        <label><fmt:message key="content.table.role" bundle="${content}" />:</label>
                        <br>
                        <c:forEach items="${roles}" var="elem">
                            <label class="radio-inline">
                                <input type="radio" name="role" value="${elem.value}">
                                    <fmt:message key="role.${fn:toLowerCase(elem.value)}" bundle="${info}" /></label>
                        </c:forEach>
                    </div>
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-plus"></span>
                        &nbsp; <fmt:message key="content.button.add.administrator" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="add_administrator">
                </form>
            </div>
        </div>
    </div>
</div>
<br><br><br><br><br>
<jsp:include page="../common/footer.jsp" />
</body>
</html>
