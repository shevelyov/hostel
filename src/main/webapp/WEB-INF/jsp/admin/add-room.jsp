<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${lang ne null ? lang : pageContext.request.locale}" />
<fmt:setBundle basename="properties.content" var="content" />
<fmt:setBundle basename="properties.hostel" var="info" />
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <title><fmt:message key="content.title.add.room" bundle="${content}"/></title>
</head>
<body>

<div id="lang-modal-ru" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.ru" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="ru_RU">
                    <input type="hidden" name="requestPage" value="controller?action=go_to_add_room">
                </div>
            </form>
        </div>
    </div>
</div>

<div id="lang-modal-en" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.en" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="en_EN">
                    <input type="hidden" name="requestPage" value="controller?action=go_to_add_room">
                </div>
            </form>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <jsp:include page="../common/header.jsp"/>
        </div>
    </div>
</div>
<br><br><br><br><br>
<div class="container">
    <div class="row">
        <h2><fmt:message key="content.title.add.room" bundle="${content}" />:</h2>
        <a href="controller?action=all_rooms&page=1" class="btn form-btn">
            <span class="glyphicon glyphicon-chevron-left"></span>
            &nbsp; <fmt:message key="content.menu.all.rooms" bundle="${content}" />
        </a>
        <hr>
        <div class="col-md-6">
            <form role="form" action="controller" method="POST">
                <div class="form-group">
                    <label for="roomtype"><fmt:message key="content.table.room.type" bundle="${content}" /></label>
                    <select name="roomtype" class="form-control" id="roomtype" >
                        <c:forEach items="${roomtypes}" var="elem" >
                            <option value="${elem.id}"><fmt:message key="room.${fn:toLowerCase(elem.description)}" bundle="${info}" />        
                            </option>
                        </c:forEach>
                    </select>
                </div>
                <div class="form-group">
                    <label for="roomnumber"><fmt:message key="content.table.room.number" bundle="${content}" />:</label>
                    <c:set var="enterRoomNumber" scope="page">
                        <fmt:message key="content.form.enter.room.number" bundle="${content}" />
                    </c:set>
                    <input type="text" name="roomnumber" class="form-control form-input" id="roomnumber"
                           placeholder="${enterRoomNumber}" pattern="^[a-zA-Z0-9 -#]{1,5}$" required>
                </div>
                <br>
                <button type="submit" class="btn form-btn">
                    <span class="glyphicon glyphicon-plus"></span>
                    &nbsp; <fmt:message key="content.button.add.room" bundle="${content}" />
                </button>
                <input type="hidden" name="action" value="add_room">
            </form>
        </div>
    </div>
</div>
<br><br><br><br><br>
<br><br><br><br><br>
<jsp:include page="../common/footer.jsp" />
</body>
</html>
