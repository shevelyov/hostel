<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<fmt:setLocale value="${lang ne null ? lang : pageContext.request.locale}" />
<fmt:setBundle basename="properties.content" var="content" />
<fmt:setBundle basename="properties.hostel" var="info" />
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <title><fmt:message key="content.title.change.administrator" bundle="${content}"/></title>
</head>
<body>
<jsp:useBean id="user" class="by.epam.shevelyov.hostel.entity.impl.User" scope="request" />

<div id="lang-modal-ru" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.ru" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="ru_RU">
                    <input type="hidden" name="requestPage" value="controller?action=go_to_change_administrator&user=${user.id}">
                </div>
            </form>
        </div>
    </div>
</div>

<div id="lang-modal-en" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.en" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="en_EN">
                    <input type="hidden" name="requestPage" value="controller?action=go_to_change_administrator&user=${user.id}">
                </div>
            </form>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <jsp:include page="../common/header.jsp"/>
        </div>
    </div>
</div>
<br><br><br><br><br>
<div class="container">
    <div class="row">
        <h2><fmt:message key="content.change.administrator" bundle="${content}" />:</h2>
        <a href="controller?action=all_administrators&page=1" class="btn form-btn">
            <span class="glyphicon glyphicon-chevron-left"></span>
            &nbsp; <fmt:message key="content.menu.all.administrators" bundle="${content}" />
        </a>
        <hr>
        <div class="col-md-6">
            <form action="controller" method="POST">
                <table class="table table-hover" border="0">
                    <tr>
                        <th><fmt:message key="content.table.id" bundle="${content}" />:</th>
                        <td>${user.id}</td>
                    </tr>
                    <tr>
                        <th><fmt:message key="content.table.administrator.login" bundle="${content}" />:</th>
                        <td><input type="text" name="login" class="form-control form-input" value="${user.login}"
                                   pattern="^[a-zA-Z](.[\w9_-]*){3,20}$" required></td>
                    </tr>
                    <tr>
                        <th><fmt:message key="content.table.password" bundle="${content}" />:</th>
                        <td><input type="password" name="password" class="form-control form-input" value="${password}"
                                   pattern="^[\w]{6,30}$" required></td>
                    </tr>
                    <tr>
                        <th><fmt:message key="content.table.repeat.password" bundle="${content}" />:</th>
                        <td><input type="password" name="repeatPassword" class="form-control form-input" value="${password}"
                                   pattern="^[\w]{6,30}$" required></td>
                    </tr>
                    <tr>
                        <th><fmt:message key="content.table.first.name" bundle="${content}" />:</th>
                        <td><input type="text" name="firstname" class="form-control form-input" value="${user.firstName}"
                                   pattern="^[a-zA-Z ,.'-]+$" required></td>
                    </tr>
                    <tr>
                        <th><fmt:message key="content.table.last.name" bundle="${content}" />:</th>
                        <td><input type="text" name="lastname" class="form-control form-input" value="${user.lastName}"
                                   pattern="^[a-zA-Z ,.'-]+$" required></td>
                    </tr>
                    <tr>
                        <th><fmt:message key="content.table.email" bundle="${content}" />:</th>
                        <td><input type="text" name="email" class="form-control form-input" value="${user.email}"
                                   pattern="^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$" required></td>
                    </tr>
                    <tr>
                        <th><fmt:message key="content.table.user.phone" bundle="${content}" />:</th>
                        <td><input type="text" name="phone" class="form-control form-input" value="${user.phoneNumber}"
                                   pattern="^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{2,3})[-. )]*(\d{3})[-. ]*(\d{2})[-. ]*(\d{2,5})?\s*$" required></td>
                    </tr>
                    <tr>
                        <th><fmt:message key="content.table.role" bundle="${content}" /></th>
                        <td>
                            <c:forEach items="${roles}" var="elem">
                                <label class="radio-inline">
                                    <input <c:if test="${elem.value eq user.role.value}">checked</c:if>
                                           type="radio" name="role" value="${elem.value}">
                                    <fmt:message key="role.${fn:toLowerCase(elem.value)}" bundle="${info}" /></label></label>
                            </c:forEach>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td><button type="submit" class="btn form-btn">
                            <span class="glyphicon glyphicon-ok"></span>
                            &nbsp; <fmt:message key="content.button.change" bundle="${content}" />
                        </button></td>
                    </tr>
                </table>
                <input type="hidden" name="action" value="change_administrator">
                <input type="hidden" name="user" value="${user.id}">
            </form>
        </div>
    </div>
</div>
<br><br><br><br><br>
<br><br><br><br><br>
<jsp:include page="../common/footer.jsp" />
</body>
</html>
