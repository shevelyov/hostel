<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<fmt:setLocale value="${lang ne null ? lang : pageContext.request.locale}" />
<fmt:setBundle basename="properties.content" var="content" />
<fmt:setBundle basename="properties.hostel" var="info" />
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <title><fmt:message key="content.title.user.details" bundle="${content}"/></title>
</head>
<body>
<jsp:useBean id="user" class="by.epam.shevelyov.hostel.entity.impl.User" scope="request" />

<div id="lang-modal-ru" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.ru" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="ru_RU">
                    <input type="hidden" name="requestPage" value="controller?action=user_details&user=${user.id}">
                </div>
            </form>
        </div>
    </div>
</div>

<div id="lang-modal-en" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.en" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="en_EN">
                    <input type="hidden" name="requestPage" value="controller?action=user_details&user=${user.id}">
                </div>
            </form>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <jsp:include page="../common/header.jsp"/>
        </div>
    </div>
</div>
<div class="container-fluid">
    <br><br><br><br><br>
    <h2><fmt:message key="content.user.details" bundle="${content}" />:</h2>
    <a href="controller?action=all_users&page=1" class="btn form-btn">
        <span class="glyphicon glyphicon-chevron-left"></span>
        &nbsp; <fmt:message key="content.menu.all.users" bundle="${content}" /></a>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <h2><fmt:message key="content.user.orders" bundle="${content}" />:</h2>
            <c:choose>
                <c:when test="${orders eq null}">
                    <h4 class="bg-warning">
                        <fmt:message key="content.user.null.orders" bundle="${content}" />
                    </h4>
                </c:when>
                <c:when test="${empty orders}">
                    <h4 class="bg-info">
                        <fmt:message key="content.user.no.orders" bundle="${content}" />
                    </h4>
                </c:when>
                <c:otherwise>
                    <table class="table text-center table-hover">
                        <tr>
                            <th class="info text-center"><fmt:message key="content.table.room" bundle="${content}" /></th>
                            <th class="info text-center"><fmt:message key="content.table.checkin.date" bundle="${content}" /></th>
                            <th class="info text-center"><fmt:message key="content.table.checkout.date" bundle="${content}" /></th>
                            <th class="info text-center"><fmt:message key="content.table.guests" bundle="${content}" /></th>
                            <th class="info text-center"><fmt:message key="content.table.status" bundle="${content}" /></th>
                            <th class="info text-center"><fmt:message key="content.table.type" bundle="${content}" /></th>
                            <th class="info text-center"><fmt:message key="content.table.discount" bundle="${content}" /></th>
                            <th class="info text-center"><fmt:message key="content.table.price" bundle="${content}" /></th>
                        </tr>
                        <c:forEach items="${orders}" var="elem" >
                            <tr class="success">
                                <td><fmt:message key="room.${fn:toLowerCase(elem.room.roomType.description)}" bundle="${info}" /></td>
                                <td><fmt:formatDate pattern="dd.MM.yyyy" value="${elem.checkIn}"/></td>
                                <td><fmt:formatDate pattern="dd.MM.yyyy" value="${elem.checkOut}"/></td>
                                <td>${elem.places}</td>
                                <td><fmt:message key="order.status.${fn:toLowerCase(elem.orderStatus.status)}" bundle="${info}" /></td>
                                <td><fmt:message key="order.type.${fn:toLowerCase(elem.orderType.type)}" bundle="${info}" /></td>
                                <td>${elem.discount.percentage}</td>
                                <td>${elem.totalAmount}</td>
                            </tr>
                        </c:forEach>
                    </table>
                </c:otherwise>
            </c:choose>
            <h2><fmt:message key="content.user.bans" bundle="${content}" />:</h2>
            <c:choose>
                <c:when test="${bans eq null}">
                    <h4 class="bg-warning">
                        <fmt:message key="content.user.null.bans" bundle="${content}" />
                    </h4>
                </c:when>
                <c:when test="${empty bans}">
                    <h4 class="bg-info">
                        <fmt:message key="content.user.no.bans" bundle="${content}" />
                    </h4>
                </c:when>
                <c:otherwise>
                    <table class="table text-center table-hover">
                        <tr>
                            <th class="info"><fmt:message key="content.table.administrator.login" bundle="${content}" /></th>
                            <th class="info"><fmt:message key="content.table.start.date" bundle="${content}" /></th>
                            <th class="info"><fmt:message key="content.table.end.date" bundle="${content}" /></th>
                            <th class="info"><fmt:message key="content.table.reason" bundle="${content}" /></th>
                        </tr>
                        <c:forEach items="${bans}" var="elem" >
                            <tr class="danger">
                                <td>${elem.admin.login}</td>
                                <td><fmt:formatDate pattern="dd.MM.yyyy" value="${elem.startDate}"/></td>
                                <td><fmt:formatDate pattern="dd.MM.yyyy" value="${elem.endDate}"/></td>
                                <td>${elem.reason}</td>
                            </tr>
                        </c:forEach>
                    </table>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>
<br><br><br><br><br>
<jsp:include page="../common/footer.jsp" />
</body>
</html>
