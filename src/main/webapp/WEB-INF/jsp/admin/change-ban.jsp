<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<fmt:setLocale value="${lang ne null ? lang : pageContext.request.locale}" />
<fmt:setBundle basename="properties.content" var="content" />
<fmt:setBundle basename="properties.hostel" var="info" />
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-datepicker3.min.css">
    <link rel="stylesheet" href="css/bootstrap-datepicker3.standalone.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="js/bootstrap-datepicker.min.js"></script>
    <title><fmt:message key="content.title.change.ban" bundle="${content}"/></title>
</head>
<body>
<jsp:useBean id="ban" class="by.epam.shevelyov.hostel.entity.impl.Ban" scope="request" />

<div id="lang-modal-ru" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.ru" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="ru_RU">
                    <input type="hidden" name="requestPage" value="controller?action=go_to_change_ban&ban=${ban.id}">
                </div>
            </form>
        </div>
    </div>
</div>

<div id="lang-modal-en" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title"><fmt:message key="content.modal.lang.en" bundle="${content}" /></h3>
            </div>
            <form role="form" action="controller" method="GET" accept-charset="UTF-8">
                <div class="modal-body">
                    <br>
                    <button type="submit" class="btn form-btn">
                        <span class="glyphicon glyphicon-ok"></span>
                        &nbsp; <fmt:message key="content.button.yes" bundle="${content}" />
                    </button>
                    <button type="button" class="btn form-btn form-btn-red" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>
                        &nbsp; <fmt:message key="content.button.no" bundle="${content}" />
                    </button>
                    <input type="hidden" name="action" value="change_language">
                    <input type="hidden" name="lang" value="en_EN">
                    <input type="hidden" name="requestPage" value="controller?action=go_to_change_ban&ban=${ban.id}">
                </div>
            </form>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <jsp:include page="../common/header.jsp"/>
        </div>
    </div>
</div>
<br><br><br><br><br>
<div class="container">
    <div class="row">
        <h2><fmt:message key="content.change.ban" bundle="${content}" />:</h2>
        <a href="controller?action=all_bans&page=1" class="btn form-btn">
            <span class="glyphicon glyphicon-chevron-left"></span>
            &nbsp; <fmt:message key="content.menu.all.bans" bundle="${content}" />
        </a>
        <hr>
        <div class="col-md-6">
            <form action="controller" method="POST">
                <table class="table table-hover" border="0">
                    <tr>
                        <th><fmt:message key="content.table.id" bundle="${content}" />:</th>
                        <td>${ban.id}</td>
                    </tr>
                    <tr>
                        <th><fmt:message key="content.table.user.login" bundle="${content}" />:</th>
                        <td>${ban.user.login}</td>
                    </tr>
                    <tr>
                        <th><fmt:message key="content.table.administrator.login" bundle="${content}" />:</th>
                        <td>${ban.admin.login}</td>
                    </tr>
                    <tr>
                        <th><fmt:message key="content.table.start.date" bundle="${content}" />:</th>
                        <td>${ban.startDate}</td>
                    </tr>
                    <tr>
                        <th><fmt:message key="content.table.end.date" bundle="${content}" />:</th>
                        <td><div class='input-group input-daterange'>
                            <input name="endDate" required type='text' class="form-control form-input" id="endDate" value="${ban.endDate}"/>
                            <span class="input-group-addon add-on">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div></td>
                    </tr>
                    <tr>
                        <th><fmt:message key="content.table.reason" bundle="${content}" />:</th>
                        <td><input type="text" name="reason" class="form-control form-input" value="${ban.reason}"
                                   pattern="^.+$" required></td>
                    </tr>
                    <tr>
                        <th></th>
                        <td><button type="submit" class="btn form-btn">
                            <span class="glyphicon glyphicon-ok"></span>
                            &nbsp; <fmt:message key="content.button.change" bundle="${content}" />
                        </button></td>
                    </tr>
                </table>
                <input type="hidden" name="action" value="change_ban">
                <input type="hidden" name="ban" value="${ban.id}">
            </form>
        </div>
    </div>
</div>
<br><br><br><br><br>
<script type="text/javascript">
    $('.input-daterange').datepicker({
    format: 'yyyy-mm-dd',
    todayBtn: true,
    autoclose: true,
    todayHighlight: true
});
</script>
<jsp:include page="../common/footer.jsp" />
</body>
</html>
