package by.epam.shevelyov.hostel.dao.impl;

import by.epam.shevelyov.hostel.dao.BaseDAO;
import by.epam.shevelyov.hostel.dao.DAOEnum;
import by.epam.shevelyov.hostel.dao.QueriesEnum;
import by.epam.shevelyov.hostel.dao.factory.BaseFactory;
import by.epam.shevelyov.hostel.dao.factory.DAOFactory;
import by.epam.shevelyov.hostel.entity.impl.OrderStatus;
import by.epam.shevelyov.hostel.manager.ConfigEnum;
import by.epam.shevelyov.hostel.manager.ConfigManager;
import by.epam.shevelyov.hostel.pool.ConnectionsPool;
import by.epam.shevelyov.hostel.service.OrderStatusService;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class MySqlOrderStatusDAOTest {
    private DAOFactory factory;
    BaseDAO<OrderStatus> orderStatusDAO;
    ConnectionsPool pool;

    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        factory = BaseFactory.defineFactory(DAOEnum.MYSQL);
        pool = ConnectionsPool.getInstance();
        orderStatusDAO = factory.getOrderStatusDAO();
        pool.init();
    }

    @After
    public void tearDown() {
        pool.destroy();
    }

    @Test
    public void testGetInstance() {
        BaseDAO<OrderStatus> tempDAO = factory.getOrderStatusDAO();
        assertEquals(orderStatusDAO, tempDAO);
    }

    @Test
    public void testFind() {
        OrderStatus orderStatus = orderStatusDAO.findEntityById(1);
        assertEquals(1, orderStatus.getId());
    }

    @Test
    public void testCreate() {
        OrderStatus orderStatus = new OrderStatus(6, "testStatus");
        boolean result = orderStatusDAO.create(orderStatus);
        assertEquals(true, result);
        OrderStatus dbOrderStatus = orderStatusDAO.findEntityById(6);
        assertEquals("testStatus", dbOrderStatus.getStatus());
    }

    @Test
    public void testFindAll() {
        List<OrderStatus> list = orderStatusDAO.findAll();
        assertEquals(5, list.size());
    }

    @Test
    public void testUpdate() {
        OrderStatus newOrderStatus = OrderStatusService.getOrderStatusById(5);
        newOrderStatus.setStatus("newStatus");
        boolean result = orderStatusDAO.update(newOrderStatus);
        OrderStatus dbOrderStatus = orderStatusDAO.findEntityById(5);
        assertEquals(true, result);
        assertEquals("newStatus", dbOrderStatus.getStatus());
    }

    @Test
    public void testDelete() {
        assertTrue(orderStatusDAO.delete(6));
    }

    @Test
    public void testAnotherExecute() {
        List<OrderStatus> list = orderStatusDAO.execute(QueriesEnum.ORDER_STATUS_BY_STATUS, new String[]{"Cancelled"});
        assertEquals(1, list.size());
    }
}