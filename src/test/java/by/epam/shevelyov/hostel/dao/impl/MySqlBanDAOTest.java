package by.epam.shevelyov.hostel.dao.impl;

import by.epam.shevelyov.hostel.dao.BaseDAO;
import by.epam.shevelyov.hostel.dao.DAOEnum;
import by.epam.shevelyov.hostel.dao.QueriesEnum;
import by.epam.shevelyov.hostel.dao.factory.BaseFactory;
import by.epam.shevelyov.hostel.dao.factory.DAOFactory;
import by.epam.shevelyov.hostel.entity.impl.Ban;
import by.epam.shevelyov.hostel.manager.ConfigEnum;
import by.epam.shevelyov.hostel.manager.ConfigManager;
import by.epam.shevelyov.hostel.pool.ConnectionsPool;
import by.epam.shevelyov.hostel.service.BanService;
import by.epam.shevelyov.hostel.service.UserService;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Date;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class MySqlBanDAOTest {
    private DAOFactory factory;
    BaseDAO<Ban> banDAO;
    ConnectionsPool pool;

    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        factory = BaseFactory.defineFactory(DAOEnum.MYSQL);
        pool = ConnectionsPool.getInstance();
        banDAO = factory.getBanDAO();
        pool.init();
    }
    @After
    public void tearDown() {
        pool.destroy();
    }

    @Test
    public void testGetInstance() {
        BaseDAO<Ban> tempDAO = factory.getBanDAO();
        assertEquals(banDAO, tempDAO);
    }

    @Test
    public void testFind() {
        Ban ban = banDAO.findEntityById(1);
        assertEquals(1, ban.getId());
    }

    @Test
    public void testCreate() {
        Ban ban = new Ban(4, UserService.getUserById(1), UserService.getUserById(2), Date.valueOf("2017-01-01"), Date.valueOf("2017-02-02"),"spam");
        boolean result = banDAO.create(ban);
        assertEquals(true, result);
        Ban dbBan = banDAO.findEntityById(4);
        assertEquals("spam", dbBan.getReason());
    }

    @Test
    public void testFindAll() {
        List<Ban> list = banDAO.findAll();
        assertEquals(3, list.size());
    }

    @Test
    public void testUpdate() {
        Ban newBan = BanService.getBanById(4);
        newBan.setReason("bot");
        boolean result = banDAO.update(newBan);
        Ban dbBan = banDAO.findEntityById(4);
        assertEquals(true, result);
        assertEquals("bot", dbBan.getReason());
    }

    @Test
    public void testDelete() {
        assertTrue(banDAO.delete(4));
    }

    @Test
    public void testExecute() {
        List<Ban> list = banDAO.execute(QueriesEnum.BAN_BY_REASON, new String[]{ "spam" });
        assertEquals(2, list.get(0).getId());
    }
    @Test
    public void testAnotherExecute() {
        List<Ban> list = banDAO.execute(QueriesEnum.BAN_BY_USER_ID, 2);
        assertEquals(1, list.size());
    }
}
