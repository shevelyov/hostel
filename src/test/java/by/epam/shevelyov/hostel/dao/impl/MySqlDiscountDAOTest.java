package by.epam.shevelyov.hostel.dao.impl;

import by.epam.shevelyov.hostel.dao.BaseDAO;
import by.epam.shevelyov.hostel.dao.DAOEnum;
import by.epam.shevelyov.hostel.dao.factory.BaseFactory;
import by.epam.shevelyov.hostel.dao.factory.DAOFactory;
import by.epam.shevelyov.hostel.entity.impl.Discount;
import by.epam.shevelyov.hostel.manager.ConfigEnum;
import by.epam.shevelyov.hostel.manager.ConfigManager;
import by.epam.shevelyov.hostel.pool.ConnectionsPool;
import by.epam.shevelyov.hostel.service.DiscountService;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class MySqlDiscountDAOTest {
    private DAOFactory factory;
    BaseDAO<Discount> discountDAO;
    ConnectionsPool pool;

    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        factory = BaseFactory.defineFactory(DAOEnum.MYSQL);
        pool = ConnectionsPool.getInstance();
        discountDAO = factory.getDiscountDAO();
        pool.init();
    }
    @After
    public void tearDown() {
        pool.destroy();
    }

    @Test
    public void testGetInstance() {
        BaseDAO<Discount> tempDAO = factory.getDiscountDAO();
        assertEquals(discountDAO, tempDAO);
    }

    @Test
    public void testFind() {
        Discount discount = discountDAO.findEntityById(1);
        assertEquals(1, discount.getId());
    }

    @Test
    public void testCreate() {
        Discount discount = new Discount(5, "testTitle", 5, "testDescription");
        boolean result = discountDAO.create(discount);
        assertEquals(true, result);
        Discount dbDiscount = discountDAO.findEntityById(5);
        assertEquals(5, dbDiscount.getPercentage());
    }

    @Test
    public void testFindAll() {
        List<Discount> list = discountDAO.findAll();
        assertEquals(5, list.size());
    }

    @Test
    public void testUpdate() {
        Discount newDiscount = DiscountService.getDiscountById(5);
        newDiscount.setPercentage(15);
        boolean result = discountDAO.update(newDiscount);
        Discount dbDiscount = discountDAO.findEntityById(5);
        assertEquals(true, result);
        assertEquals(15, dbDiscount.getPercentage());
    }

    @Test
    public void testDelete() {
        assertTrue(discountDAO.delete(5));
    }

}
