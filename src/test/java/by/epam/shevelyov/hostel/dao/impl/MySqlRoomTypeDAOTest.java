package by.epam.shevelyov.hostel.dao.impl;

import by.epam.shevelyov.hostel.dao.BaseDAO;
import by.epam.shevelyov.hostel.dao.DAOEnum;
import by.epam.shevelyov.hostel.dao.QueriesEnum;
import by.epam.shevelyov.hostel.dao.factory.BaseFactory;
import by.epam.shevelyov.hostel.dao.factory.DAOFactory;
import by.epam.shevelyov.hostel.entity.impl.RoomType;
import by.epam.shevelyov.hostel.manager.ConfigEnum;
import by.epam.shevelyov.hostel.manager.ConfigManager;
import by.epam.shevelyov.hostel.pool.ConnectionsPool;
import by.epam.shevelyov.hostel.service.RoomTypeService;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class MySqlRoomTypeDAOTest {
    private DAOFactory factory;
    BaseDAO<RoomType> roomTypeDAO;
    ConnectionsPool pool;

    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        factory = BaseFactory.defineFactory(DAOEnum.MYSQL);
        pool = ConnectionsPool.getInstance();
        roomTypeDAO = factory.getRoomTypeDAO();
        pool.init();
    }
    @After
    public void tearDown() {
        pool.destroy();
    }

    @Test
    public void testGetInstance() {
        BaseDAO<RoomType> tempDAO = factory.getRoomTypeDAO();
        assertEquals(roomTypeDAO, tempDAO);
    }

    @Test
    public void testFind() {
        RoomType roomType = roomTypeDAO.findEntityById(1);
        assertEquals(1, roomType.getId());
    }

    @Test
    public void testCreate() {
        RoomType roomType = new RoomType(0,"getFreePlacesByDatesAndRoomId",4, 50);
        boolean result = roomTypeDAO.create(roomType);
        assertEquals(true, result);
        RoomType dbRoomType = roomTypeDAO.findEntityById(10);
        assertEquals(4, dbRoomType.getCapacity());
    }

    @Test
    public void testFindAll() {
        List<RoomType> list = roomTypeDAO.findAll();
        assertEquals(10, list.size());
    }

    @Test
    public void testUpdate() {
        RoomType newRoomType = RoomTypeService.getRoomTypeById(10);
        newRoomType.setCapacity(5);
        boolean result = roomTypeDAO.update(newRoomType);
        RoomType dbRoomType = roomTypeDAO.findEntityById(10);
        assertEquals(true, result);
        assertEquals(5, dbRoomType.getCapacity());
    }

    @Test
    public void testDelete() {
        assertTrue(roomTypeDAO.delete(10));
    }

    @Test
    public void testExecute() {
        List<RoomType> list = roomTypeDAO.execute(QueriesEnum.ROOM_TYPE_BY_CAPACITY, 4);
        assertEquals(3, list.size());
    }

    @Test
    public void testAnotherExecute() {
        List<RoomType> list = roomTypeDAO.execute(QueriesEnum.ROOM_TYPE_BY_DESCRIPTION, new String[]{"six.bed.male.dorm"});
        assertEquals(1, list.size());
    }
}
