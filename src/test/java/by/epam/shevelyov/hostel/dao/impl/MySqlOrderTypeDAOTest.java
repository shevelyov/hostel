package by.epam.shevelyov.hostel.dao.impl;

import by.epam.shevelyov.hostel.dao.BaseDAO;
import by.epam.shevelyov.hostel.dao.DAOEnum;
import by.epam.shevelyov.hostel.dao.factory.BaseFactory;
import by.epam.shevelyov.hostel.dao.factory.DAOFactory;
import by.epam.shevelyov.hostel.entity.impl.OrderType;
import by.epam.shevelyov.hostel.manager.ConfigEnum;
import by.epam.shevelyov.hostel.manager.ConfigManager;
import by.epam.shevelyov.hostel.pool.ConnectionsPool;
import by.epam.shevelyov.hostel.service.OrderTypeService;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class MySqlOrderTypeDAOTest {
    private DAOFactory factory;
    BaseDAO<OrderType> orderTypeDAO;
    ConnectionsPool pool;

    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        factory = BaseFactory.defineFactory(DAOEnum.MYSQL);
        pool = ConnectionsPool.getInstance();
        orderTypeDAO = factory.getOrderTypeDAO();
        pool.init();
    }

    @After
    public void tearDown() {
        pool.destroy();
    }

    @Test
    public void testGetInstance() {
        BaseDAO<OrderType> tempDAO = factory.getOrderTypeDAO();
        assertEquals(orderTypeDAO, tempDAO);
    }

    @Test
    public void testFind() {
        OrderType orderType = orderTypeDAO.findEntityById(1);
        assertEquals(1, orderType.getId());
    }

    @Test
    public void testCreate() {
        OrderType orderType = new OrderType(0, "testType");
        boolean result = orderTypeDAO.create(orderType);
        assertEquals(true, result);
        OrderType dbOrderType = orderTypeDAO.findEntityById(3);
        assertEquals("testType", dbOrderType.getType());
    }

    @Test
    public void testFindAll() {
        List<OrderType> list = orderTypeDAO.findAll();
        assertEquals(3, list.size());
    }

    @Test
    public void testUpdate() {
        OrderType newOrderType = OrderTypeService.getOrderTypeById(3);
        newOrderType.setType("newType");
        boolean result = orderTypeDAO.update(newOrderType);
        OrderType dbOrderType = orderTypeDAO.findEntityById(3);
        assertEquals(true, result);
        assertEquals("newType", dbOrderType.getType());
    }

    @Test
    public void testDelete() {
        assertTrue(orderTypeDAO.delete(5));
    }
}