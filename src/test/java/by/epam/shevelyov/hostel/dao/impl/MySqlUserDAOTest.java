package by.epam.shevelyov.hostel.dao.impl;

import by.epam.shevelyov.hostel.dao.BaseDAO;
import by.epam.shevelyov.hostel.dao.DAOEnum;
import by.epam.shevelyov.hostel.dao.QueriesEnum;
import by.epam.shevelyov.hostel.dao.factory.BaseFactory;
import by.epam.shevelyov.hostel.dao.factory.DAOFactory;
import by.epam.shevelyov.hostel.entity.impl.User;
import by.epam.shevelyov.hostel.entity.util.RoleEnum;
import by.epam.shevelyov.hostel.manager.ConfigEnum;
import by.epam.shevelyov.hostel.manager.ConfigManager;
import by.epam.shevelyov.hostel.pool.ConnectionsPool;
import by.epam.shevelyov.hostel.service.UserService;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MySqlUserDAOTest {
    private DAOFactory factory;
    BaseDAO<User> userDAO;
    ConnectionsPool pool;

    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        factory = BaseFactory.defineFactory(DAOEnum.MYSQL);
        pool = ConnectionsPool.getInstance();
        userDAO = factory.getUserDAO();
        pool.init();
    }

    @After
    public void tearDown() {
        pool.destroy();
    }

    @Test
    public void testGetInstance() {
        BaseDAO<User> tempDAO = factory.getUserDAO();
        assertEquals(userDAO, tempDAO);
    }

    @Test
    public void testFind() {
        User user = userDAO.findEntityById(1);
        assertEquals(1, user.getId());
    }

    @Test
    public void testCreate() {
        User user = new User(0, "testLogin", "1234", "name", "surname", "getFreePlacesByDatesAndRoomId@email.com", "123123", RoleEnum.valueOf("User".toUpperCase()));
        boolean result = userDAO.create(user);
        assertEquals(true, result);
        User dbUser = userDAO.findEntityById(9);
        assertEquals("testLogin", dbUser.getLogin());
    }

    @Test
    public void testFindAll() {
        List<User> list = userDAO.findAll();
        assertEquals(9, list.size());
    }

    @Test
    public void testUpdate() {
        User newUser = UserService.getUserById(9);
        newUser.setLogin("NEW");
        boolean result = userDAO.update(newUser);
        User dbUser = userDAO.findEntityById(9);
        assertEquals(true, result);
        assertEquals("NEW", dbUser.getLogin());
    }

    @Test
    public void testDelete() {
        assertTrue(userDAO.delete(9));
    }

    @Test
    public void testExecute() {
        List<User> list = userDAO.execute(QueriesEnum.USER_BY_LOGIN, new String[]{"NEW"});
        assertEquals(9, list.get(0).getId());
    }
}
