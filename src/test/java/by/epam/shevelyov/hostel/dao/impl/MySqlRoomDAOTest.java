package by.epam.shevelyov.hostel.dao.impl;

import by.epam.shevelyov.hostel.dao.BaseDAO;
import by.epam.shevelyov.hostel.dao.DAOEnum;
import by.epam.shevelyov.hostel.dao.QueriesEnum;
import by.epam.shevelyov.hostel.dao.factory.BaseFactory;
import by.epam.shevelyov.hostel.dao.factory.DAOFactory;
import by.epam.shevelyov.hostel.entity.impl.Room;
import by.epam.shevelyov.hostel.manager.ConfigEnum;
import by.epam.shevelyov.hostel.manager.ConfigManager;
import by.epam.shevelyov.hostel.pool.ConnectionsPool;
import by.epam.shevelyov.hostel.service.RoomService;
import by.epam.shevelyov.hostel.service.RoomTypeService;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class MySqlRoomDAOTest {
    private DAOFactory factory;
    BaseDAO<Room> roomDAO;
    ConnectionsPool pool;

    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        factory = BaseFactory.defineFactory(DAOEnum.MYSQL);
        pool = ConnectionsPool.getInstance();
        roomDAO = factory.getRoomDAO();
        pool.init();
    }
    @After
    public void tearDown() {
        pool.destroy();
    }

    @Test
    public void testGetInstance() {
        BaseDAO<Room> tempDAO = factory.getRoomDAO();
        assertEquals(roomDAO, tempDAO);
    }

    @Test
    public void testFind() {
        Room room = roomDAO.findEntityById(1);
        assertEquals(1, room.getId());
    }

    @Test
    public void testCreate() {
        Room room = new Room(0,RoomTypeService.getRoomTypeById(1),"111");
        boolean result = roomDAO.create(room);
        assertEquals(true, result);
        Room dbRoom = roomDAO.findEntityById(13);
        assertEquals("111", dbRoom.getRoomNumber());
    }

    @Test
    public void testFindAll() {
        List<Room> list = roomDAO.findAll();
        assertEquals(10, list.size());
    }

    @Test
    public void testUpdate() {
        Room newRoom = RoomService.getRoomById(13);
        newRoom.setRoomNumber("122");
        boolean result = roomDAO.update(newRoom);
        Room dbRoom = roomDAO.findEntityById(13);
        assertEquals(true, result);
        assertEquals("122", dbRoom.getRoomNumber());
    }

    @Test
    public void testDelete() {
        assertTrue(roomDAO.delete(14));
    }

    @Test
    public void testExecute() {
        List<Room> list = roomDAO.execute(QueriesEnum.ROOM_BY_ROOM_TYPE_ID, 1);
        assertEquals(1, list.size());
    }
}
