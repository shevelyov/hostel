package by.epam.shevelyov.hostel.dao.impl;

import by.epam.shevelyov.hostel.dao.BaseDAO;
import by.epam.shevelyov.hostel.dao.DAOEnum;
import by.epam.shevelyov.hostel.dao.QueriesEnum;
import by.epam.shevelyov.hostel.dao.factory.BaseFactory;
import by.epam.shevelyov.hostel.dao.factory.DAOFactory;
import by.epam.shevelyov.hostel.entity.impl.*;
import by.epam.shevelyov.hostel.manager.ConfigEnum;
import by.epam.shevelyov.hostel.manager.ConfigManager;
import by.epam.shevelyov.hostel.pool.ConnectionsPool;
import by.epam.shevelyov.hostel.service.*;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Date;
import java.util.List;


public class MySqlOrderDAOTest extends Assert {
    private DAOFactory factory;
    BaseDAO<Order> orderDAO;
    ConnectionsPool pool;

    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        factory = BaseFactory.defineFactory(DAOEnum.MYSQL);
        pool = ConnectionsPool.getInstance();
        orderDAO = factory.getOrderDAO();
        pool.init();
    }

    @After
    public void tearDown() {
        pool.destroy();
    }

    @Test
    public void testGetInstance() {
        BaseDAO<Order> tempDAO = factory.getOrderDAO();
        assertEquals(orderDAO, tempDAO);
    }

    @Test
    public void testFind() {
        Order order = orderDAO.findEntityById(1);
        assertEquals(1, order.getId());
    }

    @Test
    public void testCreate() {
        User user = UserService.getUserById(1);
        Room room = RoomService.getRoomById(1);
        OrderStatus orderStatus = OrderStatusService.getOrderStatusById(1);
        OrderType orderType = OrderTypeService.getOrderTypeById(1);
        Discount discount  = DiscountService.getDiscountById(1);
        Order order = new Order(0, user, 3, Date.valueOf("2017-06-06"), Date.valueOf("2017-06-09"), Date.valueOf("2017-06-19"),
                room, orderStatus, orderType, discount, 200.0);
        boolean result = orderDAO.create(order);
        assertEquals(true, result);
        Order dbOrder = orderDAO.findEntityById(10);
        assertEquals(3, dbOrder.getPlaces());
    }

    @Test
    public void testFindAll() {
        List<Order> list = orderDAO.findAll();
        assertEquals(7, list.size());
    }

    @Test
    public void testUpdate() {
        Order newOrder = OrderService.getOrderById(5);
        newOrder.setPlaces(2);
        boolean result = orderDAO.update(newOrder);
        Order dbOrder = orderDAO.findEntityById(5);
        assertEquals(true, result);
        assertEquals(2, dbOrder.getPlaces());
    }

    @Test
    public void testDelete() {
        assertTrue(orderDAO.delete(10));
    }

    @Test
    public void testExecute() {
        List<Order> list = orderDAO.execute(QueriesEnum.ORDER_CANCELLED, 1);
        assertEquals(1, list.size());
    }
    @Test
    public void testAnotherExecute() {
        List<Order> list = orderDAO.execute(QueriesEnum.ORDER_BY_DATE_CHECK_IN, new Date[]{Date.valueOf("2017-03-24")});
        assertEquals(1, list.size());
    }
}
