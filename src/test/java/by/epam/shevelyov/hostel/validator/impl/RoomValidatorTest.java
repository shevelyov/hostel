package by.epam.shevelyov.hostel.validator.impl;

import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.manager.ConfigEnum;
import by.epam.shevelyov.hostel.manager.ConfigManager;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class RoomValidatorTest extends Assert {
    private RoomValidator validator = RoomValidator.getInstance();
    private RequestContext context = new RequestContext();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        pool.init();
        context.addRequestParameter("room", new String[]{ null });
    }

    @After
    public void tearDown() throws Exception {
        pool.destroy();
        context.clear();
    }

    @Test
    public void testGetInstance() {
        assertEquals(RoomValidator.getInstance(), validator);
    }

    @Test
    public void testEmptyRoom() {
        assertEquals(MessagesEnum.ERROR_VALIDATE_EMPTY_ROOM, validator.validate(context));
    }

    @Test
    public void testWrongRoom() {
        context.addRequestParameter("room", new String[]{ "wrong room" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_ROOM, validator.validate(context));
    }

    @Test
    public void testValidate() {
        context.addRequestParameter("room", new String[]{ "1" });
        assertEquals(MessagesEnum.SUCCESS, validator.validate(context));
    }
}