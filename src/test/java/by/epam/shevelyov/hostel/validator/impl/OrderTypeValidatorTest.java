package by.epam.shevelyov.hostel.validator.impl;

import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.manager.ConfigEnum;
import by.epam.shevelyov.hostel.manager.ConfigManager;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class OrderTypeValidatorTest extends Assert {
    private OrderTypeValidator validator = OrderTypeValidator.getInstance();
    private RequestContext context = new RequestContext();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        pool.init();
        context.addRequestParameter("ordertype", new String[]{ null });
    }

    @After
    public void tearDown() throws Exception {
        pool.destroy();
        context.clear();
    }

    @Test
    public void testGetInstance() {
        assertEquals(OrderTypeValidator.getInstance(), validator);
    }

    @Test
    public void testEmptyRoomType() {
        assertEquals(MessagesEnum.ERROR_VALIDATE_EMPTY_ORDERTYPE, validator.validate(context));
    }

    @Test
    public void testWrongRoomType() {
        context.addRequestParameter("ordertype", new String[]{ "wrong ordertype" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_ORDERTYPE, validator.validate(context));
    }

    @Test
    public void testValidate() {
        context.addRequestParameter("ordertype", new String[]{ "1" });
        assertEquals(MessagesEnum.SUCCESS, validator.validate(context));
    }
}
