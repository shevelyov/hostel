package by.epam.shevelyov.hostel.validator.impl.admin;

import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.manager.ConfigEnum;
import by.epam.shevelyov.hostel.manager.ConfigManager;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.*;

public class AddBanValidatorTest extends Assert {
    private AddBanValidator validator = AddBanValidator.getInstance();
    private RequestContext context = new RequestContext();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        pool.init();
        context.addRequestParameter("login", new String[]{ null });
        context.addRequestParameter("admin", new String[]{ "wrong_admin" });
        context.addRequestParameter("startDate", new String[]{ "2017-01-01" });
        context.addRequestParameter("endDate", new String[]{ "2017-01-01" });
        context.addRequestParameter("reason", new String[]{ "reason" });
    }

    @After
    public void tearDown() throws Exception {
        pool.destroy();
        context.clear();
    }

    @Test
    public void testGetInstance() {
        assertEquals(AddBanValidator.getInstance(), validator);
    }

    @Test
    public void testWrongAdmin() {
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_ADMINISTRATOR, validator.validate(context));
    }

    @Test
    public void testEmptyFields() {
        context.addRequestParameter("admin", new String[]{ "1" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_EMPTY_FIELDS, validator.validate(context));
    }

    @Test
    public void testWrongDates() {
        context.addRequestParameter("admin", new String[]{ "1" });
        context.addRequestParameter("login", new String[]{ "login" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_DATES, validator.validate(context));
    }

    @Test
    public void testWrongLogin() {
        context.addRequestParameter("admin", new String[]{ "1" });
        context.addRequestParameter("login", new String[]{ "login" });
        context.addRequestParameter("startDate", new String[]{ "2017-10-11" });
        context.addRequestParameter("endDate", new String[]{ "2017-11-11" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_LOGIN, validator.validate(context));
    }

    @Test
    public void testValidate() {
        context.addRequestParameter("admin", new String[]{ "1" });
        context.addRequestParameter("login", new String[]{ "ivanov123" });
        context.addRequestParameter("startDate", new String[]{ "2017-10-11" });
        context.addRequestParameter("endDate", new String[]{ "2017-11-11" });
        assertEquals(MessagesEnum.SUCCESS, validator.validate(context));
    }
}