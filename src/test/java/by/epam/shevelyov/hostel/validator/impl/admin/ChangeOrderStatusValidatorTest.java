package by.epam.shevelyov.hostel.validator.impl.admin;

import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.manager.ConfigEnum;
import by.epam.shevelyov.hostel.manager.ConfigManager;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ChangeOrderStatusValidatorTest extends Assert {
    private ChangeOrderStatusValidator validator = ChangeOrderStatusValidator.getInstance();
    private RequestContext context = new RequestContext();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        pool.init();
        context.addRequestParameter("order", new String[]{ "wrong_order" });
        context.addRequestParameter("status", new String[]{ null });
    }

    @After
    public void tearDown() throws Exception {
        pool.destroy();
        context.clear();
    }

    @Test
    public void testGetInstance() {
        assertEquals(ChangeOrderStatusValidator.getInstance(), validator);
    }

    @Test
    public void testWrongOrder() {
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_ORDER, validator.validate(context));
    }

    @Test
    public void testEmptyStatus() {
        context.addRequestParameter("order", new String[]{ "1" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_EMPTY_STATUS, validator.validate(context));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testWrongStatus() {
        context.addRequestParameter("order", new String[]{ "1" });
        context.addRequestParameter("status", new String[]{ "wrong_status" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_STATUS, validator.validate(context));
    }

    @Test
    public void testValidate() {
        context.addRequestParameter("order", new String[]{ "1" });
        context.addRequestParameter("status", new String[]{ "Pending" });
        assertEquals(MessagesEnum.SUCCESS, validator.validate(context));
    }
}
