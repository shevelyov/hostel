package by.epam.shevelyov.hostel.validator.impl.admin;

import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.manager.ConfigEnum;
import by.epam.shevelyov.hostel.manager.ConfigManager;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ShowAvailabilityValidatorTest extends Assert {
    private ShowAvailabilityValidator validator = ShowAvailabilityValidator.getInstance();
    private RequestContext context = new RequestContext();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        pool.init();
        context.addRequestParameter("start", new String[]{ null });
        context.addRequestParameter("end", new String[]{ "2017-01-01" });
    }

    @After
    public void tearDown() throws Exception {
        pool.destroy();
        context.clear();
    }

    @Test
    public void testGetInstance() {
        assertEquals(ShowAvailabilityValidator.getInstance(), validator);
    }

    @Test
    public void testWrongEmptyFields() {
        assertEquals(MessagesEnum.ERROR_VALIDATE_EMPTY_FIELDS, validator.validate(context));
    }

    @Test
    public void testWrongDates() {
        context.addRequestParameter("start", new String[]{ "2017-01-02" });
        context.addRequestParameter("end", new String[]{ "2017-01-01" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_DATES, validator.validate(context));
    }

    @Test
    public void testValidate() {
        context.addRequestParameter("start", new String[]{ "2017-01-01" });
        context.addRequestParameter("end", new String[]{ "2017-02-02" });
        assertEquals(MessagesEnum.SUCCESS, validator.validate(context));
    }
}
