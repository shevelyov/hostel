package by.epam.shevelyov.hostel.validator.impl;

import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.manager.ConfigEnum;
import by.epam.shevelyov.hostel.manager.ConfigManager;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ViewRoomsValidatorTest extends Assert {
    private ViewRoomsValidator validator = ViewRoomsValidator.getInstance();
    private RequestContext context = new RequestContext();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        pool.init();
        context.addRequestParameter("page", new String[]{ "wrong_page" });
        context.addRequestParameter("action", new String[]{ "wrong action" });
    }

    @After
    public void tearDown() throws Exception {
        pool.destroy();
        context.clear();
    }

    @Test
    public void testGetInstance() {
        assertEquals(ViewRoomsValidator.getInstance(), validator);
    }

    @Test
    public void testWrongAction() {
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_ACTION, validator.validate(context));
    }

    @Test
    public void testWrongPage() {
        context.addRequestParameter("action", new String[]{ "cheapest_room" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_PAGE, validator.validate(context));
    }

    @Test
    public void testValidate() {
        context.addRequestParameter("action", new String[]{ "cheapest_room" });
        context.addRequestParameter("page", new String[]{ "1" });
        assertEquals(MessagesEnum.SUCCESS, validator.validate(context));
    }

}
