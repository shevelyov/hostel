package by.epam.shevelyov.hostel.validator.impl;

import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.manager.ConfigEnum;
import by.epam.shevelyov.hostel.manager.ConfigManager;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BanValidatorTest extends Assert {
    private BanValidator validator = BanValidator.getInstance();
    private RequestContext context = new RequestContext();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        pool.init();
        context.addRequestParameter("ban", new String[]{ null });
    }

    @After
    public void tearDown() throws Exception {
        pool.destroy();
        context.clear();
    }

    @Test
    public void testGetInstance() {
        assertEquals(BanValidator.getInstance(), validator);
    }

    @Test
    public void testEmptyBan() {
        assertEquals(MessagesEnum.ERROR_VALIDATE_EMPTY_BAN, validator.validate(context));
    }

    @Test
    public void testWrongBan() {
        context.addRequestParameter("ban", new String[]{ "wrong ban" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_BAN, validator.validate(context));
    }

    @Test
    public void testValidate() {
        context.addRequestParameter("ban", new String[]{ "1" });
        assertEquals(MessagesEnum.SUCCESS, validator.validate(context));
    }
}