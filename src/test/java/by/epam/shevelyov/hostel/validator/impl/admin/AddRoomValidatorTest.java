package by.epam.shevelyov.hostel.validator.impl.admin;

import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.manager.ConfigEnum;
import by.epam.shevelyov.hostel.manager.ConfigManager;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AddRoomValidatorTest extends Assert {
    private AddRoomValidator validator = AddRoomValidator.getInstance();
    private RequestContext context = new RequestContext();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        pool.init();
        context.addRequestParameter("roomtype", new String[]{ "wrong_roomtype" });
        context.addRequestParameter("roomnumber", new String[]{ null });
    }

    @After
    public void tearDown() throws Exception {
        pool.destroy();
        context.clear();
    }

    @Test
    public void testGetInstance() {
        assertEquals(AddRoomValidator.getInstance(), validator);
    }

    @Test
    public void testWrongRoomType() {
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_ROOMTYPE, validator.validate(context));
    }

    @Test
    public void testEmptyFields() {
        context.addRequestParameter("roomtype", new String[]{ "1" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_EMPTY_FIELDS, validator.validate(context));
    }

    @Test
    public void testWrongParams() {
        context.addRequestParameter("roomtype", new String[]{ "1" });
        context.addRequestParameter("roomnumber", new String[]{ "wrong_number" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_PARAMS, validator.validate(context));
    }

    @Test
    public void testValidate() {
        context.addRequestParameter("roomtype", new String[]{ "1" });
        context.addRequestParameter("roomnumber", new String[]{ "122" });
        assertEquals(MessagesEnum.SUCCESS, validator.validate(context));
    }
}
