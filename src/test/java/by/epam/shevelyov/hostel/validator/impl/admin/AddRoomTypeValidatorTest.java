package by.epam.shevelyov.hostel.validator.impl.admin;

import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.manager.ConfigEnum;
import by.epam.shevelyov.hostel.manager.ConfigManager;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AddRoomTypeValidatorTest extends Assert {
    private AddRoomTypeValidator validator = AddRoomTypeValidator.getInstance();
    private RequestContext context = new RequestContext();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        pool.init();
        context.addRequestParameter("description", new String[]{ null });
        context.addRequestParameter("amount", new String[]{ "25" });
        context.addRequestParameter("capacity", new String[]{ "wrong_capacity" });
    }

    @After
    public void tearDown() throws Exception {
        pool.destroy();
        context.clear();
    }

    @Test
    public void testGetInstance() {
        assertEquals(AddRoomTypeValidator.getInstance(), validator);
    }

    @Test
    public void testEmptyFields() {
        assertEquals(MessagesEnum.ERROR_VALIDATE_EMPTY_FIELDS, validator.validate(context));
    }

    @Test
    public void testWrongParams() {
        context.addRequestParameter("description", new String[]{ "description" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_PARAMS, validator.validate(context));
    }

    @Test
    public void testValidate() {
        context.addRequestParameter("description", new String[]{ "description" });
        context.addRequestParameter("capacity", new String[]{ "6" });
        assertEquals(MessagesEnum.SUCCESS, validator.validate(context));
    }
}
