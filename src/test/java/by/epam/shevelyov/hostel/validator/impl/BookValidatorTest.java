package by.epam.shevelyov.hostel.validator.impl;

import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.manager.ConfigEnum;
import by.epam.shevelyov.hostel.manager.ConfigManager;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BookValidatorTest extends Assert {
    private BookValidator validator = BookValidator.getInstance();
    private RequestContext context = new RequestContext();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        pool.init();
        context.addRequestParameter("roomtype", new String[]{null});
        context.addRequestParameter("ordertype", new String[]{"3"});
        context.addRequestParameter("guests", new String[]{"15"});
        context.addRequestParameter("checkin", new String[]{"2017-01-01"});
        context.addRequestParameter("checkout", new String[]{"2017-02-02"});
    }

    @After
    public void tearDown() throws Exception {
        pool.destroy();
        context.clear();
    }

    @Test
    public void testGetInstance() {
        assertEquals(BookValidator.getInstance(), validator);
    }

    @Test
    public void testEmptyBook() {
        assertEquals(MessagesEnum.ERROR_VALIDATE_EMPTY_FIELDS, validator.validate(context));
    }

    @Test
    public void testWrongRoomType() {
        context.addRequestParameter("roomtype", new String[]{"15"});
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_ROOMTYPE, validator.validate(context));
    }

    @Test
    public void testWrongOrderType() {
        context.addRequestParameter("roomtype", new String[]{"1"});
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_ORDERTYPE, validator.validate(context));
    }

    @Test
    public void testWrongDates() {
        context.addRequestParameter("roomtype", new String[]{"1"});
        context.addRequestParameter("ordertype", new String[]{"1"});
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_DATE, validator.validate(context));
    }

    @Test
    public void testWrongGuests() {
        context.addRequestParameter("roomtype", new String[]{"1"});
        context.addRequestParameter("ordertype", new String[]{"1"});
        context.addRequestParameter("checkin", new String[]{"2017-08-10"});
        context.addRequestParameter("checkout", new String[]{"2017-08-15"});
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_GUESTS, validator.validate(context));
    }

    @Test
    public void testNoPlaces() {
        context.addRequestParameter("roomtype", new String[]{"1"});
        context.addRequestParameter("ordertype", new String[]{"1"});
        context.addRequestParameter("checkin", new String[]{"2017-07-31"});
        context.addRequestParameter("checkout", new String[]{"2017-08-15"});
        context.addRequestParameter("guests", new String[]{"4"});
        assertEquals(MessagesEnum.ERROR_VALIDATE_NO_PLACES, validator.validate(context));
    }

    @Test
    public void testValidate() {
        context.addRequestParameter("roomtype", new String[]{"1"});
        context.addRequestParameter("ordertype", new String[]{"1"});
        context.addRequestParameter("checkin", new String[]{"2017-08-10"});
        context.addRequestParameter("checkout", new String[]{"2017-08-15"});
        context.addRequestParameter("guests", new String[]{"4"});
        assertEquals(MessagesEnum.SUCCESS, validator.validate(context));
    }
}
