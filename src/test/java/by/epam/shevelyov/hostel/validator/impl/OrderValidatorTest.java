package by.epam.shevelyov.hostel.validator.impl;

import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.manager.ConfigEnum;
import by.epam.shevelyov.hostel.manager.ConfigManager;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class OrderValidatorTest extends Assert {
    private OrderValidator validator = OrderValidator.getInstance();
    private RequestContext context = new RequestContext();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        pool.init();
        context.addRequestParameter("order", new String[]{ null });
    }

    @After
    public void tearDown() throws Exception {
        pool.destroy();
        context.clear();
    }

    @Test
    public void testGetInstance() {
        assertEquals(OrderValidator.getInstance(), validator);
    }

    @Test
    public void testEmptyOrder() {
        assertEquals(MessagesEnum.ERROR_VALIDATE_EMPTY_ORDER, validator.validate(context));
    }

    @Test
    public void testWrongOrder() {
        context.addRequestParameter("order", new String[]{ "wrong order" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_ORDER, validator.validate(context));
    }

    @Test
    public void testValidate() {
        context.addRequestParameter("order", new String[]{ "1" });
        assertEquals(MessagesEnum.SUCCESS, validator.validate(context));
    }
}