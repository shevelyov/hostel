package by.epam.shevelyov.hostel.validator.impl.admin;

import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.manager.ConfigEnum;
import by.epam.shevelyov.hostel.manager.ConfigManager;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.*;

public class ChangeAdministratorValidatorTest extends Assert {
    private ChangeAdministratorValidator validator = ChangeAdministratorValidator.getInstance();
    private RequestContext context = new RequestContext();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        pool.init();
        context.addRequestParameter("login", new String[]{ null });
        context.addRequestParameter("password", new String[]{ "password" });
        context.addRequestParameter("repeatPassword", new String[]{ "Password" });
        context.addRequestParameter("email", new String[]{ "email" });
        context.addRequestParameter("firstname", new String[]{ "firstname" });
        context.addRequestParameter("lastname", new String[]{ "lastname" });
        context.addRequestParameter("phone", new String[]{ "wrong_phone" });
        context.addRequestParameter("role", new String[]{ "wrong_role" });
        context.addRequestParameter("admin", new String[]{ "wrong_admin" });
    }

    @After
    public void tearDown() throws Exception {
        pool.destroy();
        context.clear();
    }

    @Test
    public void testGetInstance() {
        assertEquals(ChangeAdministratorValidator.getInstance(), validator);
    }

    @Test
    public void testWrongAdministrator() {
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_ADMINISTRATOR, validator.validate(context));
    }

    @Test
    public void testEmptyFields() {
        context.addRequestParameter("admin", new String[]{ "1" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_EMPTY_FIELDS, validator.validate(context));
    }

    @Test
    public void testWrongParams() {
        context.addRequestParameter("admin", new String[]{ "1" });
        context.addRequestParameter("login", new String[]{ "login" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_PARAMS, validator.validate(context));
    }

    @Test
    public void testRepeatPassword() {
        context.addRequestParameter("admin", new String[]{ "1" });
        context.addRequestParameter("login", new String[] { "login" });
        context.addRequestParameter("email", new String[] { "email@mail.ru" });
        context.addRequestParameter("phone", new String[] { "123456789" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_PASSWORD, validator.validate(context));
    }

    @Ignore
    @Test
    public void testExistLogin() {
        context.addRequestParameter("admin", new String[]{ "1" });
        context.addRequestParameter("login", new String[]{ "login" });
        context.addRequestParameter("email", new String[] { "email@mail.ru" });
        context.addRequestParameter("phone", new String[] { "123456789" });
        context.addRequestParameter("repeatPassword", new String[] { "password" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_EXIST_LOGIN, validator.validate(context));
    }

    @Ignore
    @Test
    public void testExistEmail() {
        context.addRequestParameter("admin", new String[]{ "1" });
        context.addRequestParameter("login", new String[]{ "newlogin" });
        context.addRequestParameter("email", new String[] { "kirill.kot@gmail.com" });
        context.addRequestParameter("phone", new String[] { "123456789" });
        context.addRequestParameter("repeatPassword", new String[] { "password" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_EXIST_EMAIL, validator.validate(context));
    }

    @Ignore
    @Test(expected = IllegalArgumentException.class)
    public void testWrongRole() {
        context.addRequestParameter("admin", new String[]{ "1" });
        context.addRequestParameter("login", new String[]{ "newlogin" });
        context.addRequestParameter("email", new String[] { "email@mail.ru" });
        context.addRequestParameter("phone", new String[] { "123456789" });
        context.addRequestParameter("repeatPassword", new String[] { "password" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_ROLE, validator.validate(context));
    }

    @Ignore
    @Test
    public void testValidate() {
        context.addRequestParameter("admin", new String[]{ "1" });
        context.addRequestParameter("login", new String[]{ "newlogin" });
        context.addRequestParameter("email", new String[] { "email@mail.ru" });
        context.addRequestParameter("phone", new String[] { "123456789" });
        context.addRequestParameter("repeatPassword", new String[] { "password" });
        context.addRequestParameter("role", new String[]{ "admin" });
        assertEquals(MessagesEnum.SUCCESS, validator.validate(context));
    }

}
