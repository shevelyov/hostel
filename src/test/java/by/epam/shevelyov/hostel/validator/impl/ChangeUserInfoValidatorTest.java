package by.epam.shevelyov.hostel.validator.impl;

import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.User;
import by.epam.shevelyov.hostel.manager.ConfigEnum;
import by.epam.shevelyov.hostel.manager.ConfigManager;
import by.epam.shevelyov.hostel.manager.MessagesEnum;
import by.epam.shevelyov.hostel.pool.ConnectionsPool;
import by.epam.shevelyov.hostel.service.UserService;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.*;

public class ChangeUserInfoValidatorTest extends Assert {
    private ChangeUserInfoValidator validator = ChangeUserInfoValidator.getInstance();
    private RequestContext context = new RequestContext();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        pool.init();
        context.addRequestParameter("password", new String[] { "password" });
        context.addRequestParameter("repeatPassword", new String[] { "Password" });
        context.addRequestParameter("email", new String[] { null });
        context.addRequestParameter("firstName", new String[] { "firstName" });
        context.addRequestParameter("lastName", new String[] { "lastName" });
        context.addRequestParameter("phone", new String[] { "123456789" });
    }

    @After
    public void tearDown() throws Exception {
        pool.destroy();
        context.clear();
    }

    @Test
    public void testGetInstance() {
        assertEquals(RegistrationValidator.getInstance(), validator);
    }

    @Test
    public void testEmptyUser() {
        assertEquals(MessagesEnum.ERROR_VALIDATE_EMPTY_USER, validator.validate(context));
    }

    @Test
    public void testEmptyFields() {
        context.addRequestParameter("user", new String[] { "8" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_EMPTY_FIELDS, validator.validate(context));
    }

    @Test
    public void testWrongFields() {
        context.addRequestParameter("user", new String[] { "8" });
        context.addRequestParameter("email", new String[] { "wrong_email" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_PARAMS, validator.validate(context));
    }

    @Test
    public void testRepeatPassword() {
        context.addRequestParameter("user", new String[] { "8" });
        context.addRequestParameter("email", new String[] { "login@gmail.com" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_WRONG_PASSWORD, validator.validate(context));
    }

    @Ignore
    @Test
    public void testExistEmail() {
        context.addRequestParameter("user", new String[] { "8" });
        context.addRequestParameter("email", new String[] { "login@gmail.com" });
        context.addRequestParameter("repeatPassword", new String[] { "password" });
        assertEquals(MessagesEnum.ERROR_VALIDATE_EXIST_EMAIL, validator.validate(context));
    }

    @Ignore
    @Test
    public void testValidate() {
        context.addRequestParameter("user", new String[] { "8" });
        context.addRequestParameter("email", new String[] { "newlogin@gmail.com" });
        context.addRequestParameter("repeatPassword", new String[] { "password" });
        assertEquals(MessagesEnum.SUCCESS, validator.validate(context));
    }
}
