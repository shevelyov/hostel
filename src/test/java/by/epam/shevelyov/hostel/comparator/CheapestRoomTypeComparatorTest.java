package by.epam.shevelyov.hostel.comparator;

import by.epam.shevelyov.hostel.entity.impl.RoomType;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;

public class CheapestRoomTypeComparatorTest extends Assert {
    private RoomType roomType1 = new RoomType();
    private RoomType roomType2 = new RoomType();
    private ArrayList<RoomType> list = new ArrayList<>();

    @Before
    public void setUp() {
        roomType1.setAmount(50);
        roomType2.setAmount(100);
        list.add(roomType1);
        list.add(roomType2);
    }

    @After
    public void tearDown() throws Exception {
        list.clear();
    }

    @Test
    public void testCompare() {
        Collections.sort(list, new CheapestRoomTypeComparator());
        assertEquals(50, list.get(0).getAmount(), 0.001);
    }
}