package by.epam.shevelyov.hostel.encoder;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class EncoderTest extends Assert {
    private String text;

    @Before
    public void setUp() {
        text = "testText";
    }

    @Test
    public void testEncode() {
        String encoded = Encoder.encode(text);
        String decoded = Encoder.decode(encoded);
        System.out.println(encoded);
        assertEquals(text, decoded);
    }

}
