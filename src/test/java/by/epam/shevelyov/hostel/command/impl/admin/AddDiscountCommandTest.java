package by.epam.shevelyov.hostel.command.impl.admin;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.command.factory.ActionFactory;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.entity.impl.Discount;
import by.epam.shevelyov.hostel.manager.ConfigEnum;
import by.epam.shevelyov.hostel.manager.ConfigManager;
import by.epam.shevelyov.hostel.pool.ConnectionsPool;
import by.epam.shevelyov.hostel.service.DiscountService;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.*;

public class AddDiscountCommandTest extends Assert {
    private RequestContext context = new RequestContext();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        String[] arrTitle = {"testtitle"};
        String[] arrPercentage = {"5"};
        String[] arrDescription = {"testdescription"};
        context.addRequestParameter("title", arrTitle);
        context.addRequestParameter("percentage", arrPercentage);
        context.addRequestParameter("description", arrDescription);
        pool.init();
    }

    @After
    public void tearDown() throws Exception {
        context.clear();
        pool.destroy();
    }

    @Ignore
    @Test
    public void testExecute() {
        ActionCommand command = ActionFactory.defineCommand("add_discount");
        String page = command.execute(context);
        assertEquals("controller?action=all_discounts&page=1", page);
    }
}