package by.epam.shevelyov.hostel.command.impl;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.command.factory.ActionFactory;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.manager.ConfigEnum;
import by.epam.shevelyov.hostel.manager.ConfigManager;
import by.epam.shevelyov.hostel.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class RoomTypeDetailsCommandTest extends Assert {

    private ConnectionsPool pool = ConnectionsPool.getInstance();
    private RequestContext context = new RequestContext();

    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        pool.init();
        context.addRequestParameter("roomtype", new String[] { "3" });
    }

    @After
    public void tearDown() throws Exception {
        pool.destroy();
    }

    @Test

    public void testExecute() {
        ActionCommand command = ActionFactory.defineCommand("roomtype_details");
        String page = command.execute(context);
        assertEquals("WEB-INF/jsp/roomtype-details.jsp", page);
    }
}