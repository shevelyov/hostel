package by.epam.shevelyov.hostel.command.impl;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.command.factory.ActionFactory;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.manager.ConfigEnum;
import by.epam.shevelyov.hostel.manager.ConfigManager;
import by.epam.shevelyov.hostel.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.*;

public class RegistrationCommandTest extends Assert {
    private RequestContext context = new RequestContext();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        String[] arrLogin = {"testlogin"};
        String[] arrPassword = {"testpass"};
        String[] arrRepeatPassword = {"testpass"};
        String[] arrEmail = {"testemail@mail.com"};
        String[] arrName = {"testname"};
        String[] arrPhone = {"123456789"};
        context.addRequestParameter("login", arrLogin);
        context.addRequestParameter("password", arrPassword);
        context.addRequestParameter("repeatPassword", arrRepeatPassword);
        context.addRequestParameter("email", arrEmail);
        context.addRequestParameter("name", arrName);
        context.addRequestParameter("phone", arrPhone);
        pool.init();
    }

    @After
    public void tearDown() throws Exception {
        context.clear();
        pool.destroy();
    }

    @Ignore
    @Test
    public void testExecute() {
        ActionCommand command = ActionFactory.defineCommand("registration");
        String page = command.execute(context);
        assertEquals("controller?action=main_page", page);
    }
}