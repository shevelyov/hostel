package by.epam.shevelyov.hostel.command;

import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.manager.PagesEnum;
import by.epam.shevelyov.hostel.manager.PagesManager;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class EmptyCommandTest {
    private EmptyCommand command;

    @Before
    public void setUp() {
        command = new EmptyCommand();
    }

    @Test
    public void testExecute() {
        String actual = command.execute(new RequestContext());
        Assert.assertEquals(PagesManager.getPage(PagesEnum.ERROR_404), actual);
    }
}
