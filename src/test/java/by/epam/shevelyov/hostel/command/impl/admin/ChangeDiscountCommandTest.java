package by.epam.shevelyov.hostel.command.impl.admin;

import by.epam.shevelyov.hostel.command.ActionCommand;
import by.epam.shevelyov.hostel.command.factory.ActionFactory;
import by.epam.shevelyov.hostel.controller.RequestContext;
import by.epam.shevelyov.hostel.manager.ConfigEnum;
import by.epam.shevelyov.hostel.manager.ConfigManager;
import by.epam.shevelyov.hostel.pool.ConnectionsPool;
import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.*;

public class ChangeDiscountCommandTest extends Assert {
    private RequestContext context = new RequestContext();
    private ConnectionsPool pool = ConnectionsPool.getInstance();

    @Before
    public void setUp() {
        new DOMConfigurator().doConfigure(ConfigManager.getParam(ConfigEnum.LOG_TEST_PATH),
                LogManager.getLoggerRepository());
        String[] arrTitle = {"newtitle"};
        String[] arrPercentage = {"3"};
        String[] arrDescription = {"newdescription"};
        String[] arrDiscount = {"7"};
        context.addRequestParameter("title", arrTitle);
        context.addRequestParameter("percentage", arrPercentage);
        context.addRequestParameter("description", arrDescription);
        context.addRequestParameter("discount", arrDiscount);
        pool.init();
    }

    @After
    public void tearDown() throws Exception {
        context.clear();
        pool.destroy();
    }

    @Ignore
    @Test
    public void testExecute() {
        ActionCommand command = ActionFactory.defineCommand("change_discount");
        String page = command.execute(context);
        assertEquals("controller?action=all_discounts&page=1", page);
    }
}